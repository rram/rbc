// Sriramajayam

#include <rbc_json.h>
#include <nlohmann/json.hpp>
#include <fstream>
#include <filesystem>

namespace rbc
{
  namespace json
  {
    void ReadSetup(const std::string jsonfile,
		   double &EI, int &nNodes, double& length,
		   std::vector<double> &block_corners, std::string& block_file,
		   std::vector<double> &tvec, std::vector<double> &xy, int& num_samples_per_interval, std::pair<double,double>& ttarget, double& teps,
		   NLopt_Params& nlopt_params, SolveParams& solve_params,
		   int& nElmTOL, double& sdTOL)
    {
      assert(std::filesystem::path(jsonfile).extension()==".json" && "Expected setup file to have .json extension");
      assert(std::filesystem::exists(jsonfile) && "setup file does not exist");

	
      std::fstream stream;
      stream.open(jsonfile.c_str(), std::ios::in);
      assert(stream.good() && stream.is_open());
      auto J = nlohmann::json::parse(stream);
      stream.close();
      
      
      // blade params
      {
	auto it = J.find("blade params");
	assert(it!=J.end() && "Could not find tag blade params");
	auto& j = *it;
	assert(j.contains("EI") && "blade params missing EI");
	assert(j.contains("length") && "blade params missing length");
	assert(j.contains("nodes") && "blade params missing nodes");
	j["EI"].get_to(EI);
	j["nodes"].get_to(nNodes);
	j["length"].get_to(length);
      }

      // initial block
      {
	auto it = J.find("block");
	assert(it!=J.end() && "Could not find block");
	auto& j = *it;
	assert(j.contains("initial block corners") && "Could not find corners of initial block");
	assert(j.contains("block file") && "Could not find block file in initial block");
	block_corners.clear();
	j["initial block corners"].get_to(block_corners);
	j["block file"].get_to(block_file);
	assert(static_cast<int>(block_corners.size())==8);
	//assert(std::filesystem::exists(block_file)==false && "Block file should not exist");
      }

      // target profile
      std::string spline_filename;
      {
	auto it = J.find("target profile");
	assert(it!=J.end() && "Could not find target profile");
	auto& j = *it;
	assert(j.contains("filename") && "Could not find filename in target profile");
	assert(j.contains("num samples per interval") && "Could not find num samples per interval in target profile");
	assert(j.contains("target parameter interval") && "Could not find target parameter interval");
	assert(j.contains("parameter tolerance") && "Could not find parameter tolerance");
	j["num samples per interval"].get_to(num_samples_per_interval);
	j["filename"].get_to(spline_filename);
	std::vector<double> tint{};
	j["target parameter interval"].get_to(tint);
	assert(static_cast<int>(tint.size())==2);
	ttarget.first  = tint[0];
	ttarget.second = tint[1];
	j["parameter tolerance"].get_to(teps);
      }

      // nlopt params
      {
	auto it = J.find("nlopt params");
	assert(it!=J.end() && "Could not find nlopt params");
	auto& j = *it;
	assert(j.contains("relative ftol") && "Could not find relative ftol in nlopt params");
	assert(j.contains("relative xtol") && "Could not find relative xtol in nlopt params");
	assert(j.contains("gtol") && "Could not find gtol in nlopt params");
	j["relative ftol"].get_to(nlopt_params.ftol_rel);
	j["relative xtol"].get_to(nlopt_params.xtol_rel);
	j["gtol"].get_to(nlopt_params.gtol);
      }

      // elastica solver
      {
	auto it = J.find("elastica solver params");
	assert(it!=J.end() && "Could not find elastica solver params");
	auto& j = *it;
	assert(j.contains("convergence tol") && "Could not find convergence tol in elastica solver params");
	assert(j.contains("max iterations") && "Could not find maximum iterations in elastica solver params");
	assert(j.contains("residual scaling") && "Could not find residual scaling in elastica solver params");
	assert(j.contains("dof scaling") && "Could not find dof scaling in elastica solver params");
	assert(j.contains("verbosity") && "Could not find verbosity in elastica solver params");
	j["convergence tol"].get_to(solve_params.EPS);
	j["max iterations"].get_to(solve_params.nMaxIters);
	j["residual scaling"].get_to(solve_params.resScale);
	j["dof scaling"].get_to(solve_params.dofScale);
	j["verbosity"].get_to(solve_params.verbose);
      }
      
      // cut tolerances
      {
	auto it = J.find("cut tolerances");
	assert(it!=J.end() && "Could not find cut tolerances");
	auto& j = *it;
	assert(j.contains("sdTOL") && "Could not find sdTOL in cut tolerances");
	assert(j.contains("min num elms") && "Could not find min num elms in cut tolerances");
	j["min num elms"].get_to(nElmTOL);
	j["sdTOL"].get_to(sdTOL);
      }

      // read the spline knot vector and control points
      {
	tvec.clear();
	xy.clear();
	stream.open(spline_filename, std::ios::in);
	assert(stream.good() && stream.is_open());
	double val;
	stream >> val;
	while(stream.good())
	  {
	    tvec.push_back(val);
	    stream >> val; xy.push_back(val);
	    stream >> val; xy.push_back(val);
	    stream >> val;
	  }
	stream.close();

	// check that ttarget is a subset of tvec
	assert(ttarget.first<ttarget.second && ttarget.first>tvec.front() && ttarget.second<tvec.back());
      }

      // done
      return;
    }
    
  } // rbc::json
} // rbc::
