// Sriramajayam

#include <rbc_json.h>
#include <nlohmann/json.hpp>
#include <fstream>

namespace rbc
{
  namespace json
  {
    void ReadRoughCut(const std::string jsonfile,
		      int &nProfiles,
		      std::vector<double>& init_values,
		      std::array<std::pair<double,double>,6>& bounds,
		      int& init_cut_index)
    {
      assert(std::filesystem::path(jsonfile).extension()==".json" && "Expected setup file to have .json extension");
      assert(std::filesystem::exists(jsonfile) && "rough cut file does not exist");
      
      std::fstream stream;
      stream.open(jsonfile.c_str(), std::ios::in);
      assert(stream.good() && stream.is_open());
      auto J = nlohmann::json::parse(stream);
      stream.close();


      // continuation
      {
	auto it = J.find("continuation");
	assert(it!=J.end() && "Could not find continuation");
	auto& j = *it;
	assert(j.contains("num profiles") && "Could not find num profiles in continuation");
	assert(j.contains("init cut index") && "Could not find cut index in continuation");
	j["num profiles"].get_to(nProfiles);
	j["init cut index"].get_to(init_cut_index);
      }

      // initial values for parameters
      {
	init_values.resize(6);
	auto it = J.find("initial values");
	assert(it!=J.end() && "Could not find initial values");
	auto& j = *it;
	assert( (j.contains("H") && j.contains("V")  && j.contains("M") &&
		 j.contains("BC") && j.contains("r") && j.contains("z") ) &&
		"Could not find one/more of initial values for H,V,M,BC,r,z" );
	j["H"].get_to(init_values[0]);
	j["V"].get_to(init_values[1]);
	j["M"].get_to(init_values[2]);
	j["BC"].get_to(init_values[3]);
	j["r"].get_to(init_values[4]);
	j["z"].get_to(init_values[5]);
      }

      // bounds for parameters
      {
	auto it = J.find("bounds");
	assert(it!=J.end() && "Could not find bounds");
	auto& j = *it;
	assert( (j.contains("H") && j.contains("V")  && j.contains("M") &&
		 j.contains("BC") && j.contains("r") && j.contains("z") ) &&
		"Could not find bounds for one/more of H,V,M,BC,r,z" );

	std::array<double,2> lu;
	j["H"].get_to(lu);    bounds[0] = {lu[0], lu[1]};
	j["V"].get_to(lu);    bounds[1] = {lu[0], lu[1]};
	j["M"].get_to(lu);    bounds[2] = {lu[0], lu[1]};
	j["BC"].get_to(lu);   bounds[3] = {lu[0], lu[1]};
	j["r"].get_to(lu);    bounds[4] = {lu[0], lu[1]};
	j["z"].get_to(lu);    bounds[5] = {lu[0], lu[1]};
      }
      
      // Sanity checks
      for(int i=0; i<6; ++i)
	assert(bounds[i].first < bounds[i].second && 
	       init_values[i]>=bounds[i].first &&
	       init_values[i]<=bounds[i].second);
      
      // done
      return;
    }
    
  } // rbc::json::
} // rbc::
