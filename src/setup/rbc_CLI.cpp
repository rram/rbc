// Sriramajayam

#include <CLI/CLI.hpp>
#include <rbc_CLI.h>

namespace rbc
{
  namespace cli
  {
    void ReadCommandLineOptions(const int argc, char** argv,
				std::string& file_setup,
				std::string& file_cut)
    {
      // Commandline options
      CLI::App app;

      app.add_option("-s,--setup", file_setup, "Setup options in json format")
	->required()
	->check(CLI::ExistingFile);

      app.add_option("-c,--cut", file_cut, "Options for rough/fine cut in json format")
	->required()
	->check(CLI::ExistingFile);
	
      // read options
      app.parse(argc, argv);

      std::cout<<"\nCommandline options: "
	       <<"\n----------------------"
	       <<"\nSetup       : "<<file_setup
	       <<"\nCut         : "<<file_cut
	       << std::endl;

      return;
    }
  }
}
