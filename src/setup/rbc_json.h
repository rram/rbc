// Sriramajayam

#pragma once

#include <rbc_ProfileCuttingManager.h>

namespace rbc
{
  namespace json
  {
    void ReadSetup(const std::string jsonfile,
		   double &EI, int &nNodes, double& length,
		   std::vector<double>& block_corners, std::string& block_file,
		   std::vector<double>& tvec, std::vector<double>&xy, int& num_samples_per_interval, std::pair<double,double>& ttarget, double& teps, 
		   NLopt_Params& nlopt_params, SolveParams& solve_params,
		   int& nElmTOL, double& sdTOL);

    void ReadRoughCut(const std::string jsonfile,
		      int &nProfiles,
		      std::vector<double>& init_param_values,
		      std::array<std::pair<double,double>,6>& bounds,
		      int& init_cut_index);

    void ReadFineCut(const std::string jsonfile,
		     double& obj_wt_func_k,
		     std::vector<std::string>& trial_blades,
		     std::vector<double>& init_values,
		     std::array<std::pair<double,double>,6>& bounds,
		     int& init_cut_index);

    void WriteBlockState(const std::string jsonfile, const std::string block_tag,
			 std::vector<double>& corners,
			 const std::vector<std::pair<double,double>>& cut_intervals);
	
    void ReadBlockState(const std::string jsonfile, const std::string block_tag,
			std::vector<double>& corners,
			std::vector<std::pair<double,double>>& cut_intervals);

  }
}

