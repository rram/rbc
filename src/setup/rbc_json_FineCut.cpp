// Sriramajayam

#include <rbc_json.h>
#include <nlohmann/json.hpp>
#include <fstream>
#include <filesystem>

namespace rbc
{
  namespace json
  {
    void ReadFineCut(const std::string jsonfile,
		     double& obj_wt_func_k,
		     std::string& shapeRepoName,
		     std::vector<std::string>& trial_blades,
		     std::vector<double>& init_values,
		     std::array<std::pair<double,double>,6>& bounds,
		     int& init_cut_index)
    {
      assert(std::filesystem::exists(jsonfile) && "Fine cut json file does not exist");
      assert(std::filesystem::path(jsonfile).extension()==".json" && "Expected json extension for file");

      // parse
      std::fstream stream;
      stream.open(jsonfile, std::ios::in);
      assert(stream.good() && stream.is_open());
      auto J = nlohmann::json::parse(stream);
      stream.close();

      // fine cut params
      {
	trial_blades.clear();
	auto it = J.find("fine cut params");
	assert(it!=J.end() && "Could not find fine cut params");
	auto& j = *it;
	assert(j.contains("wt function k") && "Could not find wt function k in fine cut params");
	assert(j.contains("trial blades") && "Could not find trial blades in fine cut params");
	assert(j.contains("init cut index") && "Could not find init cut index in fine cut params");
	j["wt function k"].get_to(obj_wt_func_k);
	j["trial blades"].get_to(trial_blades);
	j["init cut index"].get_to(init_cut_index);
      }

      // initial values for parameters
      {
	init_values.resize(6);
	auto it = J.find("initial values");
	assert(it!=J.end() && "Could not find initial values");
	auto& j = *it;
	assert( (j.contains("H") && j.contains("V")  && j.contains("M") &&
		 j.contains("BC") && j.contains("r") && j.contains("z") ) &&
		"Could not find one/more of initial values for H,V,M,BC,r,z" );
	j["H"].get_to(init_values[0]);
	j["V"].get_to(init_values[1]);
	j["M"].get_to(init_values[2]);
	j["BC"].get_to(init_values[3]);
	j["r"].get_to(init_values[4]);
	j["z"].get_to(init_values[5]);
      }

      // bounds for parameters
      {
	auto it = J.find("bounds");
	assert(it!=J.end() && "Could not find bounds");
	auto& j = *it;
	assert( (j.contains("H") && j.contains("V")  && j.contains("M") &&
		 j.contains("BC") && j.contains("r") && j.contains("z") ) &&
		"Could not find bounds for one/more of H,V,M,BC,r,z" );

	std::array<double,2> lu;
	j["H"].get_to(lu);    bounds[0] = {lu[0], lu[1]};
	j["V"].get_to(lu);    bounds[1] = {lu[0], lu[1]};
	j["M"].get_to(lu);    bounds[2] = {lu[0], lu[1]};
	j["BC"].get_to(lu);   bounds[3] = {lu[0], lu[1]};
	j["r"].get_to(lu);    bounds[4] = {lu[0], lu[1]};
	j["z"].get_to(lu);    bounds[5] = {lu[0], lu[1]};
      }
      
      // Sanity checks
      assert(obj_wt_func_k>0 && "Expected positive k value");
      assert(static_cast<int>(trial_blades.size())>0 && "Expected 1 or more trial blades");
      for(int i=0; i<6; ++i)
	assert(bounds[i].first < bounds[i].second && 
	       init_values[i]>=bounds[i].first &&
	       init_values[i]<=bounds[i].second);
      
      // done
      return;
    }

  } // rbc::json::
} // rbc::
