// Sriramajayam

#ifndef RBC_COMMAND_LINE_OPTIONS_H
#define RBC_COMMAND_LINE_OPTIONS_H

#include <string>

namespace rbc
{
  namespace cli
  {
    // Rough/fine cut
    void ReadCommandLineOptions(const int argc, char** argv,
				std::string& file_setup,
				std::string& file_cut);
  }
}

#endif
