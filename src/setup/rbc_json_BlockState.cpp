// Sriramajayam

#include <nlohmann/json.hpp>
#include <rbc_json.h>
#include <fstream>
#include <filesystem>

namespace rbc
{
  namespace json
  {
    void ReadBlockState(const std::string jsonfile,   const std::string block_tag, 
			std::vector<double>& corners, std::vector<std::pair<double,double>>& cut_intervals)
    {
      assert(std::filesystem::path(jsonfile).extension()==".json" && "Expected json extension for block file");
      assert(std::filesystem::exists(jsonfile) && "Could not find block json file to read");

      // parse
      std::fstream stream;
      stream.open(jsonfile, std::ios::in);
      assert(stream.good() && stream.is_open());
      auto J = nlohmann::json::parse(stream);
      stream.close();

      // find the tag
      auto it = J.find(block_tag);
      assert(it!=J.end() && "Could not find block tag in block state file");
      auto& j = *it;
      assert(j.contains("num corners") && "Could not find num corners in block state");
      assert(j.contains("corners") && "Could not find corners in block state");
      assert(j.contains("num cut intervals") && "Could not find num cut intervals in block state");
      assert(j.contains("cut intervals") && "Could not find cut intervals in block state");

      int num_corners, num_cut_intervals;
      std::vector<double> tcut{};
      corners.clear();
      cut_intervals.clear();
      j["num corners"].get_to(num_corners);
      j["num cut intervals"].get_to(num_cut_intervals);
      j["corners"].get_to(corners);
      j["cut intervals"].get_to(tcut);
      assert(static_cast<int>(corners.size())==2*num_corners && "Inconsistent number of block corners");
      assert(static_cast<int>(tcut.size())==2*num_cut_intervals && "Inconsistent number of cut intervals");
      cut_intervals.resize(num_cut_intervals);
      for(int i=0; i<num_cut_intervals; ++i)
	cut_intervals[i] = {tcut[2*i], tcut[2*i+1]};

      // done
      return;
    }



    void WriteBlockState(const std::string jsonfile, const std::string block_tag,
			 std::vector<double>& corners,
			 const std::vector<std::pair<double,double>>& cut_intervals)
    {
      assert(std::filesystem::path(jsonfile).extension()==".json" && "Expected json extension for block file");

      // data
      const int num_corners = static_cast<int>(corners.size())/2;
      const int num_cut_intervals = static_cast<int>(cut_intervals.size());
      std::vector<double> tcut(2*num_cut_intervals);
      for(int i=0; i<num_cut_intervals; ++i)
	{
	  tcut[2*i]   = cut_intervals[i].first;
	  tcut[2*i+1] = cut_intervals[i].second;
	}
	
      // new file?
      if(std::filesystem::exists(jsonfile)==false)
	{
	  nlohmann::json J;
	  auto& j = J[block_tag];
	  j["num corners"]       = num_corners;
	  j["num cut intervals"] = num_cut_intervals;
	  j["corners"]           = corners;
	  j["cut intervals"]     = tcut;
	    
	  std::fstream pfile;
	  pfile.open(jsonfile, std::ios::out);
	  assert(pfile.good());
	  pfile << J;
	  pfile.close();
	}
      else // existing file
	{
	  std::fstream pfile;
	  pfile.open(jsonfile, std::ios::in);
	  assert(pfile.good() && pfile.is_open());
	  auto J = nlohmann::json::parse(pfile);
	  pfile.close();
	  assert(J.find(block_tag)==J.end() && "Cannot overwrite existing block tag");
	  auto& j = J[block_tag];
	  j["num corners"]       = num_corners;
	  j["num cut intervals"] = num_cut_intervals;
	  j["corners"]           = corners;
	  j["cut intervals"]     = tcut;

	  pfile.open(jsonfile, std::ios::out);
	  assert(pfile.good());
	  pfile << J;
	  pfile.close();
	}

      // done
      return;
    }

  } // rbc::json::
} // rbc::
