// Sriramajayam

#include <rbc_SectionFunctionals.h>
#include <rbc_NLopt_Optimizer.h>
#include <rbc_CubicBezierCurve.h>
#include <rbc_WeightFunction.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create an elastica
  const double EI = 1.;
  rbc::Elastica* elastica = new rbc::Elastica(coordinates, EI);
  
  // Trivial loads
  rbc::LoadParams load_params({.HVal=0., .VVal=0., .MDof=nNodes-1, .MVal=0.});

  // Trivial Dirichlet bcs
  std::pair<int,double> bc_params(0, 0.);

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};

  // Optimization variables (H, V, M, BC, x, y)
  const std::array<bool,6> optimization_variables{true, true, true, true, false, false};

  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1., .dofScale = 1.,
	.nMaxIters = 25, .verbose = false});

  // Target shape
  rbc::NLopt_Params opt_params;
  const double tol = 1.e-8;
  opt_params.ftol_rel = tol;
  opt_params.xtol_rel = tol;
  opt_params.gtol     = tol;

  rbc::NLSolverParams nl_params;
  nl_params.digits = 5;
  nl_params.max_iter = 100;
  nl_params.normTol = 1e-4;
  
  rbc::CubicBezierCurve curve({0.,1.}, 40, nl_params); 
  std::vector<double> control_points(8);
  for(int i=0; i<4; ++i)
    {
      const double t = static_cast<double>(i)/3.;
      control_points[2*i] = t;
      control_points[2*i+1] = 0.2*std::sin(t);
    }
  curve.SetControlPoints(control_points);

  // weight function
  rbc::WeightFunction wtFunc({0.,1.}, 100.);
  
  // Objective functional
  rbc::SectionFunctionals functional(*elastica, load_params, bc_params, origin_params, optimization_variables, solve_params, curve, curve, wtFunc, wtFunc);
  
  // Create optimizer
  rbc::NLopt_Optimizer opt(functional.GetNumParameters(), functional.GetNumConstraints());

  // NLopt parameters
  
  // Initial guess
  std::vector<double> opt_vars{0.1,0.2,0.3,0.4};
  std::vector<std::pair<double,double>> var_bounds(4, {-1.e10,1.e10});
  
  // Initial value of functional
  double J0;
  functional.EvaluateObjective(opt_vars, J0, nullptr);

  // Optimize
  int result = opt.Optimize(functional, opt_params, opt_vars, var_bounds);
  
  // Plot the final solution
  const auto& xy = elastica->GetCartesianCoordinates();
  std::fstream pfile;
  pfile.open((char*)"opt-shape.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    pfile << xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  pfile.close();
  
  // Check optimized parameters
  assert(std::abs(load_params.HVal-opt_vars[0])<1.e-4);
  assert(std::abs(load_params.VVal-opt_vars[1])<1.e-4);
  assert(std::abs(load_params.MVal-opt_vars[2])<1.e-4);
  assert(std::abs(bc_params.second-opt_vars[3])<1.e-4);

  // Check functional
  double Jval, dJvals[4];
  functional.EvaluateObjective(opt_vars, Jval, dJvals);
  assert(std::abs(Jval/J0)<1.e-4);

  // Check constraints
  std::vector<double> gvals(nNodes);
  functional.EvaluateConstraints(opt_vars, gvals.data(), nullptr);
  for(int n=0; n<nNodes; ++n)
    assert(gvals[n]<1.e-4); // g(x) <= 0
    
  // Clean up
  delete elastica;
}
