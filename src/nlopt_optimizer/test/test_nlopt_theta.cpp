// Sriramajayam

#include <rbc_ThetaFunctional.h>
#include <rbc_NLopt_Optimizer.h>
#include <P11DElement.h>
#include <iostream>

// Integrand
void TargetTheta(const double& s, const double& theta,
		 double& integrand, double* dintegrand, void* usr);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create an elastica
  const double EI = 1.;
  rbc::Elastica* elastica = new rbc::Elastica(coordinates, EI);
  
  // Trivial loads
  rbc::LoadParams load_params({.HVal=0., .VVal=0., .MDof=nNodes-1, .MVal=0.});

  // Trivial Dirichlet bcs
  std::pair<int,double> bc_params(0, 0.);

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};

  // Optimization variables (H, V, M, BC, x, y)
  const std::array<bool,6> optimization_variables{true, true, true, true, false, false};
  
  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1., .dofScale = 1.,
	.nMaxIters = 25, .verbose = false});

  // Function defining the target
  rbc::ThetaFunction tgtfunc(TargetTheta);
  
  // Objective functional
  rbc::ThetaFunctional functional(*elastica, load_params, bc_params, origin_params, optimization_variables, solve_params, tgtfunc, nullptr);

  // Create optimizer
  rbc::NLopt_Optimizer opt(functional.GetNumParameters(), functional.GetNumConstraints());

  // NLopt parameters
  rbc::NLopt_Params opt_params;
  const double tol = 1.e-8;
  opt_params.ftol_rel = tol;
  opt_params.xtol_rel = tol;
  opt_params.gtol     = tol;
  
  // Initial guess
  std::vector<double> opt_vars{0.,0.,0.,0.};
  std::vector<std::pair<double,double>> var_bounds(4,{-1.e-10,1.e10});
  
  // Initial value of functional
  double J0;
  functional.EvaluateObjective(opt_vars, J0, nullptr);
    
  // Optimize
  int result = opt.Optimize(functional, opt_params, opt_vars, var_bounds);
  
  // Check optimized parameters
  assert(std::abs(load_params.HVal-opt_vars[0])<tol);
  assert(std::abs(load_params.VVal-opt_vars[1])<tol);
  assert(std::abs(load_params.MVal-opt_vars[2])<tol);
  assert(std::abs(bc_params.second-opt_vars[3])<tol);
  std::cout << std::endl << "Solution: "
	    << opt_vars[0] << ", " << opt_vars[1] << ", " << opt_vars[2] << ", " << opt_vars[3] << std::endl;

  // Check sensitivities of functional
  double Jval, dJvals[4];
  functional.EvaluateObjective(opt_vars, Jval, dJvals);
  assert(std::abs(Jval/J0)<1.e-5);
  for(int i=0; i<4; ++i)
    assert(std::abs(dJvals[i])<1.e-4);
    
  // Clean up
  delete elastica;
}
  
  
// Integrand
void TargetTheta(const double& s, const double& theta,
		 double& integrand, double* dintegrand, void* usr)
{
  // Target theta
  double tgt = 0.5*s;
  integrand = 0.5*(theta-tgt)*(theta-tgt);
  (*dintegrand) = (theta-tgt);
  return;
}
