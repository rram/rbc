// Sriramajayam

#include <rbc_NLopt_Optimizer.h>
#include <iostream>

namespace rbc
{
  // Evaluate objective
  double NLopt_Objective_Function(const std::vector<double>& x,
				  std::vector<double>& grad,
				  void* params)
  {
    // Access
    assert(params!=nullptr);
    auto& funcs = *static_cast<Functionals*>(params);

    // evaluate objective/sensitivities
    double Jval = 0.;
    funcs.EvaluateObjective(x, Jval, grad.data());

    // done
    return Jval;
  }


  // Evaluate constraints
  void NLopt_Constraint_Function(unsigned int m, double* gvals,   // constraints
				 unsigned int n, const double *x, // parameters 
				 double *dgvals,
				 void* params)
  {
    assert(gvals!=nullptr);
    assert(params!=nullptr);
    auto& funcs = *static_cast<Functionals*>(params);

    // Access
    assert(static_cast<int>(n)==funcs.GetNumParameters());
    assert(static_cast<int>(m)==funcs.GetNumConstraints());
    
    // Evaluate constraints/sensitivities
    const std::vector<double> xvec(x, x+n);
    funcs.EvaluateConstraints(xvec, gvals, dgvals);
    
    // done
    return;
  }

  
  // Constructor
  NLopt_Optimizer::NLopt_Optimizer(const int nparams, const int nconstraints)
    :nParameters(nparams),
     nConstraints(nconstraints),
     opt_vars(nParameters),
     var_lbounds(nParameters),
     var_ubounds(nParameters),
     constraint_tols(nConstraints, -1.)
  {
    // Create the optimizer
    optimizer = new nlopt::opt(nlopt::LD_SLSQP, nParameters);
  }

  
  // Main functionality: optimize
  int NLopt_Optimizer::Optimize(Functionals& functionals,
				const NLopt_Params& opt_params,
				std::vector<double>& x, 
				const std::vector<std::pair<double,double>>& x_bounds)
  {
    assert(functionals.GetNumParameters()==nParameters);
    assert(functionals.GetNumConstraints()==nConstraints);
    assert(static_cast<int>(x.size())==nParameters);
    assert(static_cast<int>(x_bounds.size())==nParameters);
    
    // Set tolerances for parameters
    optimizer->set_ftol_rel(opt_params.ftol_rel);
    optimizer->set_xtol_rel(opt_params.xtol_rel);

    // Set tolerances for constraints
    std::fill(constraint_tols.begin(), constraint_tols.end(), opt_params.gtol);

    // Set the initial guess for optimization parameters
    std::cout << std::endl << "NLopt info:" << std::endl;
    for(int i=0; i<nParameters; ++i)
      {
	std::cout  << x_bounds[i].first << "\t" << x[i] << "\t" << x_bounds[i].second << std::endl;
	assert(x_bounds[i].first<=x_bounds[i].second && x[i]>=x_bounds[i].first && x[i]<=x_bounds[i].second);
	opt_vars[i] = x[i];
	var_lbounds[i] = x_bounds[i].first;
	var_ubounds[i] = x_bounds[i].second;
      }
    
    // Set lower and upper bounds for optimization parameters
    optimizer->set_lower_bounds(var_lbounds);
    optimizer->set_upper_bounds(var_ubounds);

    // Set the objective function evaluate
    optimizer->set_min_objective(NLopt_Objective_Function, &functionals);

    // set the constraint function to evaluate
    optimizer->remove_inequality_constraints();
    optimizer->add_inequality_mconstraint(NLopt_Constraint_Function, &functionals, constraint_tols);
    
    // Optimize
    double Jval = 0.;
    nlopt::result result = nlopt::FAILURE;
    try
      {	result = optimizer->optimize(opt_vars, Jval);
	assert(result>0);
	assert(result==nlopt::FTOL_REACHED || result==nlopt::XTOL_REACHED);
	if(result==nlopt::FTOL_REACHED)
	  std::cout << std::endl << "NLopt converged reason: FTOL_REACHED" << std::endl;
	else
	  std::cout << std::endl << "NLopt converged reason: XTOL_REACHED" << std::endl;
	for(int i=0; i<nParameters; ++i)
	  x[i] = opt_vars[i];
      }
    catch(std::exception &e)
      {
	std::cout << std::endl << "nlopt failed: " << e.what() << std::endl;
      }

    return result;
  }

}
