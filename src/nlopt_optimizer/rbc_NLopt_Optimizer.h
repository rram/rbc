// Sriramajayam

#ifndef RBC_NLOPT_OPTIMIZER_H
#define RBC_NLOPT_OPTIMIZER_H

#include <rbc_Functionals.h>
#include <nlopt.hpp>

namespace rbc
{
  //! Wrapper class for optimization with NLopt
 class  NLopt_Optimizer
 {
 public:

   //! Constructor
   NLopt_Optimizer(const int nparams, const int nconstraints);
   
   //! Disable copy and assignment
   NLopt_Optimizer(const NLopt_Optimizer&) = delete;
   NLopt_Optimizer operator=(const NLopt_Optimizer&) = delete;

   //! Destructor
   inline virtual ~NLopt_Optimizer()
   { delete optimizer; }

   //! Access the solver object
   inline nlopt::opt& GetSolver()
   { return *optimizer; }

   //! Main functionality: optimize
   int Optimize(Functionals& functionals, const NLopt_Params& op,
		std::vector<double>& x,   const std::vector<std::pair<double,double>>& x_bounds);

 private:
   const int nParameters;
   const int nConstraints;
   nlopt::opt* optimizer;
   std::vector<double> opt_vars;
   std::vector<double> var_lbounds, var_ubounds;
   std::vector<double> constraint_tols;

   // friends with objective evaluation function
   friend double NLopt_Objective_Function(const std::vector<double>& x,
					  std::vector<double>& grad,
					  void* params);

   // friends with constraint evaluation function
   friend void NLopt_Constraint_Function(unsigned int m, double* result, 
					 unsigned int n, const double *x,
					 double *gradient, void* params);
 };
}

#endif
