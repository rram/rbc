// Sriramajayam

#pragma once

#include <cassert>
#include <utility>
#include <vector>
#include <string>

namespace rbc
{
  class WeightFunction
  {
  public:

    //! Constructor
    inline WeightFunction(const std::pair<double,double> t, const double k)
      :t0(t.first),
      t1(t.second),
      kval(k)
      {
	assert(kval>0.);
      }

    //! Destructor
    inline virtual ~WeightFunction() {}

    //! Disable copy and assignment
    WeightFunction(const WeightFunction&) = delete;
    WeightFunction operator=(const WeightFunction&) = delete;

    //! Main functionality: evaluate weight function and derivatives
    void Evaluate(const double tval, double& val, double* dval=nullptr) const;

    //! Access the interval
    inline std::pair<double,double> GetInterval() const
    { return {t0,t1}; }
    
    //! Consistency test
    bool ConsistencyTest(const double tval, const double pertEPS, const double tolEPS) const;

    //! Plot the profile of the weight function
    void Plot(const std::string filename, const double t0, const double t1,  const int nPoints) const;
    
  private:
    const double kval;
    const double t0, t1;
  };
  
}
