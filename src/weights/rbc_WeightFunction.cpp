// Sriramajayam

#include <rbc_WeightFunction.h>
#include <fstream>
#include <cmath>

namespace rbc
{
  
  // Main functionality: evaluate functions and derivatives
  void WeightFunction::Evaluate(const double tval, double& val, double* dval) const
  {
    // value
    val = 0.5*(std::tanh(kval*(tval-t0)) + std::tanh(kval*(t1-tval)) );

    // derivative
    if(dval!=nullptr)
      (*dval) = 0.5*kval*(std::pow(std::cosh(kval*(tval-t0)),-2.)-std::pow(std::cosh(kval*(t1-tval)),-2.));
    
    // done
    return;
  }
  

  // Consistency test
  bool WeightFunction::ConsistencyTest(const double tval, const double pertEPS, const double tolEPS) const
  {
    // evaluate
    double fval, dfval;
    Evaluate(tval, fval, &dfval);

    // numerical derivatives
    double fplus, fminus;
    Evaluate(tval+pertEPS, fplus, nullptr);
    Evaluate(tval-pertEPS, fminus, nullptr);
    double dfnum = (fplus-fminus)/(2.*pertEPS);
    assert(std::abs(dfval-dfnum)<tolEPS);
    
    return true;
  }

  // Plot the profile of the weight function
  void WeightFunction::Plot(const std::string filename, const double t0, const double t1,  const int nPoints) const
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());

    const double dt = (t1-t0)/static_cast<double>(nPoints);
    double tval, wval, dwval;
    for(int i=0; i<=nPoints; ++i)
      {
	tval = t0 + static_cast<double>(i)*dt;
	Evaluate(tval, wval, &dwval);
	pfile << tval << " " << wval << " " << dwval << std::endl;
      }
    pfile.close();
    return;
  }
  
}
