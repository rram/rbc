// Sriramajayam

#include <rbc_WeightFunction.h>
#include <random>
#include <fstream>
#include <iostream>

using namespace rbc;

int main()
{
  const double kval = 50.;
  WeightFunction wfunc({0.25,0.5}, kval);

  auto tint = wfunc.GetInterval();
  assert(std::abs(tint.first-0.25)+std::abs(tint.second-0.5)<1.e-6);
  
  // plot
  wfunc.Plot("profile.dat", -0.25, 1.25, 200);

  // consistency tests
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,1.);
  for(int i=0; i<100; ++i)
    wfunc.ConsistencyTest(dis(gen), 1.e-6, 1.e-4);
}
  
