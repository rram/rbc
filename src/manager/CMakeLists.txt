# Sriramajayam

add_library(rbc_manager STATIC
  rbc_ProfileCuttingManager.cpp)

# headers
target_include_directories(rbc_manager PUBLIC 
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(rbc_manager PUBLIC rbc_monitor rbc_nlopt)

# Add required flags
target_compile_features(rbc_manager PUBLIC ${rbc_COMPILE_FEATURES})

install(FILES
  rbc_ProfileCuttingManager.h
  DESTINATION ${PROJECT_NAME}/include)
