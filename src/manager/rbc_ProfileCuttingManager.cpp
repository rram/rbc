// Sriramajayam

#include <rbc_ProfileCuttingManager.h>
#include <PlottingUtils.h>

namespace rbc
{
  // Constructor
  ProfileCuttingManager::ProfileCuttingManager(const ParametricCurve& pc,
					       const WeightFunction& cst_wt,
					       const std::vector<double>& corners,
					       const double length, const int nelms)
    :target_profile(pc),
     cst_wtFunc(cst_wt),
     profile_monitor(target_profile),
     area_monitor(corners),
     elastica(nullptr)
  {
    assert(static_cast<int>(corners.size())==8); // 4 corners

    // create the elastica
    CreateBlade(length, nelms);

    // create optimizers
    const int nnodes = elastica->GetNumNodes();
    for(int i=0; i<6; ++i)
      nlopt[i] = new NLopt_Optimizer(i+1, nnodes);
  }
  

  // Constructor
  ProfileCuttingManager::ProfileCuttingManager(const ParametricCurve& pc,
					       const WeightFunction& cst_wt,
					       const std::vector<double>& corners,
					       const std::vector<std::pair<double,double>>& cut_intervals,
					       const double length, const int nelms)
    :target_profile(pc),
     cst_wtFunc(cst_wt),
     profile_monitor(target_profile, cut_intervals),
     area_monitor(corners),
     elastica(nullptr)
  {
    // create the elastica
    CreateBlade(length, nelms);

    // create optimizers
    const int nnodes = elastica->GetNumNodes();
    for(int i=0; i<6; ++i)
      nlopt[i] = new NLopt_Optimizer(i+1, nnodes);
  }

  
  // Create the blade
  void ProfileCuttingManager::CreateBlade(const double length, const int nelms)
  {
    std::vector<double> coordinates(nelms+1);
    const double dh = length/static_cast<double>(nelms);
    for(int n=0; n<nelms+1; ++n)
      coordinates[n] = static_cast<double>(n)*dh;
    elastica = new Elastica(coordinates, 1.);
    return;
  }
					  
  
  // Pack optimization variable values: blade_params -> x
  void ProfileCuttingManager::PackVariables(const std::array<bool,6>& opt_vars,
					    const BladeParams& blade_params,
					    const std::array<std::pair<double,double>,6>& var_bounds,
					    std::vector<double>& x,
					    std::vector<std::pair<double,double>>& x_bounds)
  {
    // pack parameters -> x
    int count = 0;
    if(opt_vars[0]==true) x[count++] = blade_params.H;     // H
    if(opt_vars[1]==true) x[count++] = blade_params.V;     // V
    if(opt_vars[2]==true) x[count++] = blade_params.M;     // M
    if(opt_vars[3]==true) x[count++] = blade_params.BC;    // BC
    if(opt_vars[4]==true) x[count++] = blade_params.rz[0]; // r
    if(opt_vars[5]==true) x[count++] = blade_params.rz[1]; // z
    assert(static_cast<int>(x.size())==count);
    
    // pack bounds
    assert(static_cast<int>(x_bounds.size())==count);
    count = 0;
    for(int i=0; i<6; ++i)
      if(opt_vars[i]==true)
	x_bounds[count++] = var_bounds[i];
    
    // done
    return;
  }


  // Unpack optimization variable values: x -> blade_params
  void ProfileCuttingManager::UnpackVariables(const std::array<bool,6>& opt_vars,
					      const std::vector<double>& x, BladeParams& blade_params)
  {
    int count = 0;
    if(opt_vars[0]==true) blade_params.H     = x[count++];   // H
    if(opt_vars[1]==true) blade_params.V     = x[count++];   // V
    if(opt_vars[2]==true) blade_params.M     = x[count++];   // M
    if(opt_vars[3]==true) blade_params.BC    = x[count++];   // BC
    if(opt_vars[4]==true) blade_params.rz[0] = x[count++];   // r
    if(opt_vars[5]==true) blade_params.rz[1] = x[count++];   // z
    assert(static_cast<int>(x.size())==count);
    return;
  }
  
  // Main functionality: compute an equilibrium configuration for the blade
  void ProfileCuttingManager::EquilibrateBlade(const BladeParams& param_set,
					       const std::vector<double>& theta_guess, 
					       const SolveParams& solve_params,
					       BladeConfig& out_blade_config)
  {
    assert(elastica!=nullptr);
    
    // Push parameter set to loads, bcs, origin
    const int nNodes = elastica->GetNumNodes();
    load_params.HVal = param_set.H;
    load_params.VVal = param_set.V;
    load_params.MVal = param_set.M;
    load_params.MDof = nNodes-1;
    bc_params.first = 0;
    bc_params.second = param_set.BC;
    origin_params.node = param_set.node;
    origin_params.coords[0] = param_set.rz[0];
    origin_params.coords[1] = param_set.rz[1];

    // Set the initial guess for the solution
    assert(static_cast<int>(theta_guess.size())==nNodes);
    elastica->SetStateField(theta_guess.data());

    // Solve for state & sensitivities
    elastica->ComputeStateAndSensitivities(load_params, bc_params, origin_params, solve_params);

    // Update the blade state
    const auto& theta = elastica->GetStateField();
    const auto& rz = elastica->GetCartesianCoordinates();
    if(static_cast<int>(out_blade_config.theta.size())<nNodes) out_blade_config.theta.resize(nNodes);
    if(static_cast<int>(out_blade_config.rz.size())<2*nNodes)  out_blade_config.rz.resize(2*nNodes);
    std::copy(theta.begin(), theta.end(), out_blade_config.theta.begin());
    std::copy(rz.begin(), rz.end(), out_blade_config.rz.begin());

    // done
    return;
  }


  // Main functionality: continuation towards a specified profile
  void ProfileCuttingManager::ProfileContinuation(const FunctionalType ft,
						  const std::array<bool,6>& opt_vars,
						  const BladeParams& in_param_set,
						  const std::array<std::pair<double,double>,6>& var_bounds,
						  const std::vector<double>& theta_guess,
						  const ParametricCurve& curve,
						  const WeightFunction& obj_wtFunc,
						  const SolveParams& solve_params,
						  NLopt_Optimizer& optimizer,
						  const NLopt_Params& opt_params,
						  BladeParams& out_param_set,
						  BladeConfig& out_blade_config)
  {
    // Push parameter set and equilibrate
    EquilibrateBlade(in_param_set, theta_guess, solve_params, out_blade_config);

    // Create a section functional for this step
    Functionals* functionals;
    if(ft==FunctionalType::section_functional)
      functionals = new SectionFunctionals(*elastica, load_params, bc_params, origin_params,
					   opt_vars, solve_params, curve, target_profile, obj_wtFunc, cst_wtFunc);
    else if(ft==FunctionalType::weighted_section_functional)
      functionals = new WeightedSectionFunctionals(*elastica, load_params, bc_params, origin_params,
						   opt_vars, solve_params, curve, target_profile, obj_wtFunc, cst_wtFunc);
    else
      assert(false && "Unknown functional type specified");

    // #parameters: expect 1-6 parameters
    const int nParameters = functionals->GetNumParameters();
    assert(nParameters>0 && nParameters<=6);

    // Initial guess, bounds
    std::vector<double> x(nParameters);
    std::vector<std::pair<double,double>> x_bounds(nParameters);
    PackVariables(opt_vars, in_param_set, var_bounds, x, x_bounds);
    
    // optimize
    auto status = optimizer.Optimize(*functionals, opt_params, x, x_bounds);
    assert(status>=0);
    
    // Outcomes
    out_param_set = in_param_set;
    UnpackVariables(opt_vars, x, out_param_set);
    out_blade_config.theta = elastica->GetStateField();
    out_blade_config.rz = elastica->GetCartesianCoordinates();

    // clean up
    delete functionals;

    // done
    return;
  }

  
  // Main functionality: continuation towards a specified profile using nlopt
  void ProfileCuttingManager::OptimizeRoughCut(const FunctionalType ft,
					       const std::array<bool,6>& opt_vars,
					       const BladeParams& in_param_set,
					       const std::array<std::pair<double,double>,6>& var_bounds,
					       const std::vector<double>& theta_guess,
					       const ParametricCurve& curve,
					       const SolveParams& solve_params,
					       const NLopt_Params& nlopt_params,
					       BladeParams& out_param_set,
					       BladeConfig& out_blade_config)
  {
    const int nparams = std::count(opt_vars.begin(), opt_vars.end(), true);
    assert(nparams>=1 && nparams<=6);
    ProfileContinuation(ft, opt_vars, in_param_set, var_bounds, theta_guess, curve, cst_wtFunc, solve_params, *nlopt[nparams-1], nlopt_params, out_param_set, out_blade_config);
    return;
  }

  
  void ProfileCuttingManager::OptimizeFineCut(FunctionalType ft,
					      const std::array<bool,6>& opt_vars,
					      const BladeParams& in_param_set,
					      const std::array<std::pair<double,double>,6>& var_bounds,
					      const std::vector<double>& theta_guess,
					      const WeightFunction& obj_wtFunc,
					      const SolveParams& solve_params,
					      const NLopt_Params& nlopt_params,
					      BladeParams& out_param_set,
					      BladeConfig& out_blade_config)
  {
    const int nparams = std::count(opt_vars.begin(), opt_vars.end(), true);
    assert(nparams>=1 && nparams<=6);
    ProfileContinuation(ft, opt_vars, in_param_set, var_bounds, theta_guess, target_profile, obj_wtFunc, solve_params, *nlopt[nparams-1], nlopt_params, out_param_set, out_blade_config);
    return;
  }


  // Execute a cut
  void ProfileCuttingManager::Cut(const BladeParams& param_set,
				  BladeConfig& blade_config,
				  const SolveParams& solve_params,
				  const double sdTOL, const double tsizeTOL)
  {
    // Equilibrate the blade with the given set of parameters
    EquilibrateBlade(param_set, blade_config.theta, solve_params, blade_config);

    // update the cut profile
    profile_monitor.Update(*elastica, sdTOL, tsizeTOL);

    // polygon defining the cut area
    auto cut_box_corners = GetCutPolygonForBlade(blade_config);
    
    // execute cut
    area_monitor.Subtract(cut_box_corners);

    // done
    return;
  }

  
  // Helper function to check whether a given blade set lies in the feasible region
  bool ProfileCuttingManager::IsBladeFeasible(const BladeConfig& blade_config, const double sdTOL) const
  {
    bool flag = true;
    double sd;
    const int nNodes = elastica->GetNumNodes();
    for(int n=0; n<nNodes; ++n)
      {
	const double* X = &blade_config.rz[2*n];
	target_profile.GetSignedDistance(X, sd, nullptr);
	if(sd>sdTOL)
	  { flag = false;
	    break; }
      }
       
    return flag;
  }
		    


  // Evaluate functionals
  double ProfileCuttingManager::EvaluateRoughCutObjective(const FunctionalType ft,
							  const std::array<bool,6>& opt_vars,
							  const BladeParams& in_param_set,
							  const SolveParams& solve_params,
							  const BladeConfig& blade_config,
							  const ParametricCurve& curve)
  {
    // create functional
    Functionals* func;
    if(ft==FunctionalType::section_functional)
      func = new SectionFunctionals(*elastica, load_params, bc_params, origin_params,
				    opt_vars, solve_params, curve, target_profile, cst_wtFunc, cst_wtFunc);
    else if(ft==FunctionalType::weighted_section_functional)
      func = new WeightedSectionFunctionals(*elastica, load_params, bc_params, origin_params,
					    opt_vars, solve_params, curve, target_profile, cst_wtFunc, cst_wtFunc);
    else
      assert(false && "Unknown functional type specified");


    // #parameters: expect 4, 5 or 6
    const int nParameters = func->GetNumParameters();
    assert(nParameters>=1 && nParameters<=6);

     // Initial guess, dummy bounds
    std::vector<double> x(nParameters);
    std::array<std::pair<double,double>,6> dummy_bounds;
    std::vector<std::pair<double,double>>  dummy_x_bounds(nParameters);
    PackVariables(opt_vars, in_param_set, dummy_bounds, x, dummy_x_bounds);

    // compute
    double J = 0.;
    func->EvaluateObjective(x, J, nullptr);

    // clean up
    delete func;

    // done
    return J;
  }


  double ProfileCuttingManager::EvaluateFineCutObjective(const FunctionalType ft,
							 const std::array<bool,6>& opt_vars,
							 const BladeParams& in_param_set,
							 const SolveParams& solve_params,
							 const BladeConfig& blade_config,
							 const WeightFunction& obj_wtFunc)
  {
    // create functional
    Functionals* func;
    if(ft==FunctionalType::section_functional)
      func = new SectionFunctionals(*elastica, load_params, bc_params, origin_params,
				    opt_vars, solve_params, target_profile, target_profile, obj_wtFunc, cst_wtFunc);
    else if(ft==FunctionalType::weighted_section_functional)
      func = new WeightedSectionFunctionals(*elastica, load_params, bc_params, origin_params,
					    opt_vars, solve_params, target_profile, target_profile, obj_wtFunc, cst_wtFunc);
    else
      assert(false && "Unknown functional type specified");
    
    // #parameters
    const int nParameters = func->GetNumParameters();
    assert(nParameters>=1 && nParameters<=6);

    // Initial guess, dummy bounds
    std::vector<double> x(nParameters);
    std::array<std::pair<double,double>,6> dummy_bounds;
    std::vector<std::pair<double,double>>  dummy_x_bounds(nParameters);
    PackVariables(opt_vars, in_param_set, dummy_bounds, x, dummy_x_bounds);

    double J = 0.;
    func->EvaluateObjective(x, J, nullptr);

    // clean up
    delete func;

    // done
    return J;
  }

  // Evaluate the cut area
  double ProfileCuttingManager::EvaluateCutArea(const BladeConfig& blade_config) const
  {
    // cut polygon defined by the blade
    auto cut_box_corners = GetCutPolygonForBlade(blade_config); 
    const int nCorners = static_cast<int>(cut_box_corners.size()/2);
    boost_polygon2D cut_poly;
    for(int n=0; n<nCorners; ++n)
      boost::geometry::append(cut_poly.outer(), boost_point2D(cut_box_corners[2*n],cut_box_corners[2*n+1]));
    boost::geometry::correct(cut_poly);

    // existing block
    auto& uncut_poly = area_monitor.GetUncutPolygon();
    
    // intersection
    std::vector<boost_polygon2D> intersect_poly{};
    boost::geometry::intersection(uncut_poly, cut_poly, intersect_poly);
    
    // area of the intersection region
    double cut_area = 0.;
    for(auto& poly:intersect_poly)
      cut_area += boost::geometry::area(poly);

    // done
    return cut_area;
  }
  

  // Determine a polygon defining the region cut by a blade
  std::vector<double> ProfileCuttingManager::GetCutPolygonForBlade(const BladeConfig& blade_config) const
  {
    // Bounding box for region to be subtracted: find max r of blade, block
    const auto& blade_rz = blade_config.rz;
    const int nnodes   = static_cast<int>(blade_rz.size()/2);
    double rmax = blade_rz[0];
    for(int i=0; i<nnodes; ++i)
      if(blade_rz[2*i]>rmax)
	rmax = blade_rz[2*i];
    
    std::vector<double> block_corners = area_monitor.GetCorners();
    const int ncorners = static_cast<int>(block_corners.size()/2);
    for(int i=0; i<ncorners; ++i)
      if(block_corners[2*i]>rmax)
	rmax = block_corners[2*i];

    // provide a little tolerance in the radial direction
    rmax *= 1.1;

    // shape of the block to cut: HACK HACK HACK: ASSUMES TOP/BOT -> can break
    std::vector<double> cut_box = blade_rz;
    const double xtop[] = {cut_box[0], cut_box[1]};
    const double xbot[] = {cut_box[cut_box.size()-2], cut_box[cut_box.size()-1]};
    cut_box.push_back(rmax);
    cut_box.push_back(xbot[1]);
    cut_box.push_back(rmax);
    cut_box.push_back(xtop[1]);

    // done
    return std::move(cut_box);
  }

  
  // Print blade configuration to file
  void BladeConfig::PlotTec(const std::string filename)
  {
    const int nnodes = static_cast<int>(rz.size())/2;
    CoordConn MD;
    MD.nodes_element = 3;
    MD.spatial_dimension = 2;
    MD.nodes = static_cast<int>(rz.size())/2;
    MD.elements = MD.nodes-1;
    MD.coordinates = rz;
    MD.connectivity.resize(3*MD.elements);
    for(int e=0; e<MD.elements; ++e)
      {
	MD.connectivity[3*e+0] = e+1;
	MD.connectivity[3*e+1] = e+1;
	MD.connectivity[3*e+2] = e+2;
      }
    PlotTecCoordConn(filename.c_str(), MD);
  }


  
}
