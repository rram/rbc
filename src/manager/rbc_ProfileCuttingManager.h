// Sriramajayam

#pragma once

#include <rbc_Elastica.h>
#include <rbc_CutProfileMonitor.h>
#include <rbc_CutAreaMonitor.h>
#include <rbc_ParametricCurve.h>
#include <rbc_NLopt_Optimizer.h>
#include <rbc_SectionFunctionals.h>
#include <rbc_WeightedSectionFunctionals.h>

namespace rbc
{
  enum class FunctionalType
  {
    section_functional,
      weighted_section_functional
      };
  
  struct BladeParams
  {
    double H, V, M, BC;
    int node;
    double rz[2];
    inline BladeParams& operator=(const BladeParams& obj)
    {
      H = obj.H;
      V = obj.V;
      M = obj.M;
      BC = obj.BC;
      node = obj.node;
      rz[0] = obj.rz[0];
      rz[1] = obj.rz[1];
      return *this;
    }
  };

  
  struct BladeConfig
  {
    std::vector<double> theta;
    std::vector<double> rz;
    void PlotTec(const std::string filename);
  };

  
  class ProfileCuttingManager
  {
  public:
    
    //! Constructor
    ProfileCuttingManager(const ParametricCurve& pc,
			  const WeightFunction& cst_wt,
			  const std::vector<double>& corners,
			  const double length, const int nelms);


    //! Constructor
    ProfileCuttingManager(const ParametricCurve& pc,
			  const WeightFunction& cst_wt,
			  const std::vector<double>& corners,
			  const std::vector<std::pair<double,double>>& cut_intervals,
			  const double length, const int nelms);

    //! Destructor
    inline virtual ~ProfileCuttingManager()
    {
      delete elastica;
      for(auto& x:nlopt)
	delete x;
    }
    
    //! Disable copy and assignment
    ProfileCuttingManager(const ProfileCuttingManager&) = delete;
    ProfileCuttingManager operator=(const ProfileCuttingManager&) = delete;

    //! Access the blade
    inline const Elastica& GetBlade() const
    {
      assert(elastica!=nullptr);
      return *elastica;
    }
    
    //! Access the profile monitor
    inline const CutProfileMonitor& GetCutProfileMonitor() const
    { return profile_monitor; }

    //! Access the area monitor
    inline const CutAreaMonitor& GetCutAreaMonitor() const
    { return area_monitor; }

    //! Main functionality: compute an equilibrium configuration for the blade
    void EquilibrateBlade(const BladeParams& param_set,
			  const std::vector<double>& theta_guess, 
			  const SolveParams& solve_params,
			  BladeConfig& out_blade_config);

    //! Evaluate functionals
    double EvaluateRoughCutObjective(const FunctionalType ft,
				     const std::array<bool,6>& opt_vars,
				     const BladeParams& in_param_set,
				     const SolveParams& solve_params,
				     const BladeConfig& blade_config,
				     const ParametricCurve& curve);

    double EvaluateFineCutObjective(const FunctionalType ft,
				    const std::array<bool,6>& opt_vars,
				    const BladeParams& in_param_set,
				    const SolveParams& solve_params,
				    const BladeConfig& blade_config,
				    const WeightFunction& obj_wtfunc);
    
				     
    //! Try cutting: continuation towards a specified profile using NLopt
    void OptimizeRoughCut(const FunctionalType ft,
			  const std::array<bool,6>& opt_vars,
			  const BladeParams& in_param_set,
			  const std::array<std::pair<double,double>,6>& var_bounds,
			  const std::vector<double>& theta_guess, 
			  const ParametricCurve& curve,
			  const SolveParams& solve_params,
			  const NLopt_Params& opt_params,
			  BladeParams& out_param_set,
			  BladeConfig& out_blade_config);
    
    //! Try cutting: continuation towards a specified profile using NLopt
    void OptimizeFineCut(const FunctionalType ft,
			 const std::array<bool,6>& opt_vars,
			 const BladeParams& in_param_set,
			 const std::array<std::pair<double,double>,6>& var_bounds,
			 const std::vector<double>& theta_guess, 
			 const WeightFunction& obj_wtFunc,
			 const SolveParams& solve_params,
			 const NLopt_Params& opt_params,
			 BladeParams& out_param_set,
			 BladeConfig& out_blade_config);
    
    //! Evaluate the cut area
    double EvaluateCutArea(const BladeConfig& blade_config) const;
    
    //! Execute a cut
    void Cut(const BladeParams& param_set, BladeConfig& blade_config, const SolveParams& solve_params,
	     const double sdTOL, const double tsizeTOL);
	         
    //! Helper function to check whether a given blade set lies in the feasible region
    bool IsBladeFeasible(const BladeConfig& blade_config, const double sdTOL) const;
		    
  private:

    // Create the blade
    void CreateBlade(const double length, const int nelms);
    
    //! Pack optimization variable values: blade_params -> x
    static void PackVariables(const std::array<bool,6>& opt_vars,
			      const BladeParams& blade_params,
			      const std::array<std::pair<double,double>,6>& var_bounds,
			      std::vector<double>& x, std::vector<std::pair<double,double>>& x_bounds);
    
    //! Unpack optimization variable values: x -> blade_params
    static void UnpackVariables(const std::array<bool,6>& opt_vars,
				const std::vector<double>& x, BladeParams& blade_params);

    //! Advance cut: profile-based continuation
    void ProfileContinuation(const FunctionalType ft,
			     const std::array<bool,6>& opt_vars,
			     const BladeParams& in_param_set,
			     const std::array<std::pair<double,double>,6>& var_bounds,
			     const std::vector<double>& theta_guess,
			     const ParametricCurve& curve,
			     const WeightFunction& obj_wtFunc,
			     const SolveParams& solve_params,
			     NLopt_Optimizer& optimizer,
			     const NLopt_Params& opt_params,
			     BladeParams& out_param_set,
			     BladeConfig& out_blade_config);
    
    std::vector<double> GetCutPolygonForBlade(const BladeConfig& blade_config) const;

    const ParametricCurve          &target_profile;
    const WeightFunction           &cst_wtFunc;
    CutProfileMonitor              profile_monitor;
    CutAreaMonitor                 area_monitor;
    Elastica                       *elastica;
    LoadParams                     load_params;
    std::pair<int,double>          bc_params;
    OriginParams                   origin_params;
    std::array<NLopt_Optimizer*,6> nlopt;         //!< NLopt optimizer with different number of parameters
  };
}
