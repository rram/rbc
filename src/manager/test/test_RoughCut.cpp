// Sriramajayam

// How to use: ./test_RoughCut -s setup.json -c rough_cut.json

#include <rbc_ProfileCuttingManager.h>
#include <rbc_json.h>
#include <rbc_CLI.h>
#include <rbc_CubicSplineCurve.h>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Read command line options for filenames
  std::string file_setup, file_roughcut;
  rbc::cli::ReadCommandLineOptions(argc, argv, file_setup, file_roughcut);

  // Setup
  double EI;
  int nNodes;
  double length;
  std::vector<double> block_corners{};
  std::string block_file;
  std::vector<double> tvec{}, xy{};
  std::pair<double,double> ttarget;
  double teps;
  int nSamplesPerInterval;
  rbc::NLopt_Params nlopt_params;
  rbc::SolveParams  solve_params;
  int nElmTOL;
  double sdTOL;
  rbc::json::ReadSetup(file_setup, EI, nNodes, length,
		       block_corners, block_file,
		       tvec, xy, nSamplesPerInterval, ttarget, teps,
		       nlopt_params, solve_params,
		       nElmTOL, sdTOL);
  const int nElements = nNodes-1;
  std::vector<std::pair<double,double>> cut_intervals{};
  
  // Rough cut
  int nProfiles;
  std::vector<double> initial_param_values{};
  std::array<std::pair<double,double>,6> bounds{};
  int cut_index;
  rbc::json::ReadRoughCut(file_roughcut, nProfiles, initial_param_values, bounds, cut_index);
  const std::array<bool,6> optimization_params{true, true, true, true, true, false}; // H, V, M, BC, r, z
  const double tTOL = static_cast<double>(nElmTOL)*length/static_cast<double>(nElements);


  // ----------------------- all inputs read ------------------------------- //

  // signed distance solver params
  rbc::NLSolverParams nl_params;
  nl_params.digits = 5;
  nl_params.max_iter = 100;
  nl_params.normTol = 1e-4;

  // target curve
  const int nControlPoints = static_cast<int>(tvec.size());
  rbc::CubicSplineCurve target_curve(tvec, {ttarget.first+teps, ttarget.second-teps}, nSamplesPerInterval, nl_params);
  target_curve.SetControlPoints(xy);

  
  // Initial straight profile
  std::vector<double> init_control_points(2*nControlPoints);
  const double tmin  = tvec.front();
  const double tmax  = tvec.back();
  for(int p=0; p<nControlPoints; ++p)
    {
      double tval = tvec[p];
      double rinit = initial_param_values[4];
      double zinit = initial_param_values[5];
      double zval = zinit + (tval-tmin)/(tmax-tmin)*length;
      init_control_points[2*p+0] = rinit;
      init_control_points[2*p+1] = zval;
    }

  // Intermediate profiles for continuation
  rbc::CubicSplineCurve cont_curve(tvec, {ttarget.first+teps, ttarget.second-teps}, nSamplesPerInterval, nl_params);
  std::vector<double> cont_control_points(2*nControlPoints);

  // Initial wire parameters
  rbc::BladeParams in_blade_params;
  in_blade_params.H     = initial_param_values[0];
  in_blade_params.V     = initial_param_values[1];
  in_blade_params.M     = initial_param_values[2];
  in_blade_params.BC    = initial_param_values[3];
  in_blade_params.node  = nNodes/2;
  in_blade_params.rz[0] = initial_param_values[4];
  in_blade_params.rz[1] = initial_param_values[5];

  // Weight function
  rbc::WeightFunction wtFunc({ttarget.first+teps, ttarget.second-teps}, 100.);
  
  // Create manager
  rbc::ProfileCuttingManager manager(target_curve, wtFunc, block_corners, cut_intervals, length, nElements);

  // Intial guess for the elastica solution
  std::vector<double> theta_guess(nNodes);
  std::fill(theta_guess.begin(), theta_guess.end(), M_PI/2.);

  // record history
  std::fstream stream;
  stream.open(std::string("history-"+std::to_string(cut_index)+".dat").c_str(), std::ios::out);
  assert(stream.good());
  stream << "#cut index \t J \t block area \t tcut-length "<<std::endl;
  
  // Perform continuation
  rbc::FunctionalType ft = rbc::FunctionalType::section_functional;
  for(int p=0; p<=nProfiles; ++p)
    {
      // Control points of intermediate profile: (1-lambda)*straightwire + lambda*spline
      const double lambda = static_cast<double>(p)/static_cast<double>(nProfiles);
      for(int p=0; p<nControlPoints; ++p)
	{
	  cont_control_points[2*p+0] = (1.-lambda)*init_control_points[2*p+0] + lambda*xy[2*p+0];
	  cont_control_points[2*p+1] = (1.-lambda)*init_control_points[2*p+1] + lambda*xy[2*p+1];
	}
      cont_curve.SetControlPoints(cont_control_points);

      // Plot the intermediate profile
      // todo
      
      // Optimize blade for this profile
      rbc::BladeParams opt_blade_params;
      rbc::BladeConfig opt_blade_config;
      manager.OptimizeRoughCut(ft, optimization_params, in_blade_params, bounds, theta_guess,
			       cont_curve, solve_params, nlopt_params,
			       opt_blade_params, opt_blade_config);

      // Verify feasibility
      bool is_feasible = manager.IsBladeFeasible(opt_blade_config, sdTOL);
      assert(is_feasible==true);
      
      // Execute the cut at the optimized blade configuration
      manager.Cut(opt_blade_params, opt_blade_config, solve_params, sdTOL, tTOL);

      // write the block state
      auto block_corners = manager.GetCutAreaMonitor().GetCorners();
      auto cut_intervals = manager.GetCutProfileMonitor().GetCutIntervals();
      rbc::json::WriteBlockState(block_file, "block_state_"+std::to_string(cut_index), block_corners, cut_intervals);
      
      // record history
      const double Jval  = manager.EvaluateRoughCutObjective(ft, optimization_params, opt_blade_params, solve_params, opt_blade_config, cont_curve);
      const double bArea = manager.GetCutAreaMonitor().GetUncutPolygonArea();
      const double tLen  = manager.GetCutProfileMonitor().GetUncutProfileLength();
      stream << cut_index <<"\t" << Jval << "\t" << bArea << "\t" << tLen << std::endl;

      // visualization
      opt_blade_config.PlotTec("blade_config_"+std::to_string(cut_index)+".tec");
      manager.GetCutAreaMonitor().PlotTec("block_config_"+std::to_string(cut_index)+".tec");
      manager.GetCutProfileMonitor().PlotTecCutProfile("cut_profile_"+std::to_string(cut_index)+".tec", 100);
      manager.GetCutProfileMonitor().PlotTecUncutProfile("cut_profile_"+std::to_string(cut_index)+".tec", 100);
      
      // Update parameters and blade configuration for the next continuation iteration
      theta_guess     = opt_blade_config.theta;
      in_blade_params = opt_blade_params;
      ++cut_index;
    }
  stream.close();
  
  // done
  PetscFinalize();
}
