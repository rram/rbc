// Sriramajayam

#ifndef RBC_SENSITIVITY_CONSISTENCY_H
#define RBC_SENSITIVITY_CONSISTENCY_H

#include <rbc_Structs.h>
#include <cmath>

namespace rbc
{
  // Consistency test
  template<class OpType>
  bool Sensitivity_Consistency_Test(const void* arg,
				    OpType& Op,
				    const double pertEPS,
				    const double tolEPS)
  {
    assert(arg!=nullptr);
    const auto* config = static_cast<const SensitivityConfiguration*>(arg);
    const auto& state = *config->state;
    const auto& sensitivity = *config->sensitivity;
    const auto& L2GMap = *config->L2GMap;
    
    // Fields and dofs
    const int nDof = Op.GetFieldDof(0);

    // Size arrays
    std::vector<std::vector<double>> res(1), resplus(1), resminus(1);
    res[0].resize(nDof);
    resplus[0].resize(nDof);
    resminus[0].resize(nDof);

    std::vector<std::vector<std::vector<std::vector<double>>>> dres(1);
    dres[0].resize(nDof);
    for(int a=0; a<nDof; ++a)
      {
	dres[0][a].resize(1);
	dres[0][a][0].resize(nDof);
      }

    // Implemented residual and stiffness
    Op.GetDVal(arg, &res, &dres);

    // Perturbed configuration
    std::vector<double> pertAlpha = sensitivity;
    SensitivityConfiguration pertConfig;
    pertConfig.state = &state;
    pertConfig.sensitivity = &pertAlpha;
    pertConfig.L2GMap = &L2GMap;
    
    // Consistency tests
    for(int a=0; a<nDof; ++a)
      {
	// Positive perturbation
	pertAlpha[a] += pertEPS;
	double Eplus = Op.GetEnergy(&pertConfig);
	Op.GetVal(&pertConfig, &resplus);

	// Negative perturbation
	pertAlpha[a] -= 2.*pertEPS;
	double Eminus = Op.GetEnergy(&pertConfig);
	Op.GetVal(&pertConfig, &resminus);

	// Undo perturbations
	pertAlpha[a] += pertEPS;

	// Numerical derivatives
	double nres = (Eplus-Eminus)/(2.*pertEPS);
	//std::cout<<"\n"<<res[0][a]<<" should be "<<nres<<std::flush;
	assert(std::abs(nres-res[0][a])<tolEPS);
	
	for(int b=0; b<nDof; ++b)
	  {
	    double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	    //std::cout<<"\n"<<dres[0][a][0][b]<<" should be "<<ndres<<std::flush;
	    assert(std::abs(ndres-dres[0][a][0][b])<tolEPS);
	  }
      }
    return true;
  }

}

#endif
