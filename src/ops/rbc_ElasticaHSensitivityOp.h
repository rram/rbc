// Sriramajayam

#ifndef RBC_ELASTICA_H_SENSITIVITY_OP_H
#define RBC_ELASTICA_H_SENSITIVITY_OP_H

#include <ElementalOperation.h>
#include <utility>
#include <cassert>

namespace rbc
{

  //! Sensitivity of elastica operations with respect to the horizontal load
  class ElasticaHSensitivityOp: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element for this operation. Same interpolation for all fields
    //! \param[in] fieldnum Field number for dof angle
    //! \param[in] str Configuration access
    inline ElasticaHSensitivityOp(const int elmnum, const Element* elm, 
				  const double ei,
				  const double lambda_H, const double lambda_V)
      :DResidue(), ElmNum(elmnum), Elm(elm),  Fields({0}), EI(ei),
      LambdaH(lambda_H), LambdaV(lambda_V) {}
    
    //! Destructor. Nothing to do
    inline virtual ~ElasticaHSensitivityOp() {}
    
    // Disable copy and assignment
    ElasticaHSensitivityOp(const ElasticaHSensitivityOp&) = delete;
    ElasticaHSensitivityOp operator=(const ElasticaHSensitivityOp&) = delete;
    virtual ElasticaHSensitivityOp* Clone() const override
    { assert(false); return nullptr; }
    
    //! Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    //! Returns the fields used
    inline const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(Fields[fieldnumber]); }

    //! Returns the modulus
    inline double GetModulus() const
    { return EI; }

    //! Returns the load
    inline void GetLoadValues(double& H, double& V) const
    { H  = LambdaH;   V = LambdaV; }

    //! Set the load
    inline void SetLoadValues(const double H, const double V)
    { LambdaH = H;    LambdaV = V; }

    //! Computes the energy functional
    //! \param[in] Config Configuration at which to compute the energy
    double GetEnergy(const void* Config) const;
    
    //! Residual calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    inline void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const override
    { return GetDVal(Config, resval, nullptr); }

    //! Residual and stiffness calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    //! \param[in] dresval Computed local stiffness
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const override;

    //! Consistency test for this operation
    //! \param[in] Config Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to use for examining outputs
    bool ConsistencyTest(const void* Config,
				 const double pertEPS, const double tolEPS) const override;

  private:
    const int ElmNum;
    const Element* Elm; //!< Element for this operation
    std::vector<int> Fields; //!< Field number for this operation
    const double EI; //!< Bending modulus
    double LambdaH, LambdaV;  //!< Load value multiplying sin(theta), cos(theta) terms
  };

}

#endif
