// Sriramajayam

#include <LocalToGlobalMap.h>
#include <rbc_ElasticaHSensitivityOp.h>
#include <rbc_Structs.h>
#include <rbc_SensitivityConsistency.h>
#include <cassert>
#include <cmath>
#include <iostream>

namespace rbc
{
  // Compute the energy functional
  double ElasticaHSensitivityOp::GetEnergy(const void* arg) const
  {
    // Get this state
    assert(arg!=nullptr);
    const auto* config = static_cast<const SensitivityConfiguration*>(arg);
    const auto& state = *config->state;
    const auto& sensitivity = *config->sensitivity;
    const auto& L2GMap = *config->L2GMap;
    
    // Fields, ndofs, shape function derivatives
    const int field = Fields[0]; assert(field==0);
    const int nDof = GetFieldDof(0);
    const int nDeriv = Elm->GetNumDerivatives(field); assert(nDeriv==1);
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);

    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double Energy = 0.;
    double theta; 
    double alpha;
    double dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatives  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    const int node = L2GMap.Map(0,a,ElmNum);
	    theta += state[node]*qShapes[a];
	    alpha += sensitivity[node]*qShapes[a];
	    dalpha += sensitivity[node]*qDShapes[a];
	  }

	// Update energy
	Energy += Qwts[q]*( 0.5*EI*dalpha*dalpha +
			    0.5*alpha*alpha*(LambdaH*std::cos(theta) + LambdaV*std::sin(theta)) +
			    alpha*std::sin(theta) );
      }
    return Energy;
  }


  // Residual and stiffness calculation
  void ElasticaHSensitivityOp::GetDVal(const void *arg, 
				     std::vector<std::vector<double>>* funcval, 
				     std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // get this state
    assert(arg!=nullptr);
    const auto* config = static_cast<const SensitivityConfiguration*>(arg);
    const auto& state = *config->state;
    const auto& sensitivity = *config->sensitivity;
    const auto& L2GMap = *config->L2GMap;

    // Zero the outputs
    SetZero(funcval, dfuncval);
    
    // Fields, ndofs, num of derivatives
    const int field = Fields[0]; assert(field==0);
    const int nDof = GetFieldDof(field);
    const int nDeriv = Elm->GetNumDerivatives(field); assert(nDeriv==1);
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);
  
    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double theta;
    double alpha;
    double dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatices  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];

	// Compute theta, alpha & alpha' here
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    const int node = L2GMap.Map(0,a,ElmNum);
	    theta += state[node]*qShapes[a];
	    alpha += sensitivity[node]*qShapes[a];
	    dalpha += sensitivity[node]*qDShapes[a]; // 1D
	  }

	// Udpate the force vector
	if(funcval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    (*funcval)[0][a] += Qwts[q]*(EI*dalpha*qDShapes[a] +
					 LambdaH*std::cos(theta)*alpha*qShapes[a]+
					 LambdaV*std::sin(theta)*alpha*qShapes[a]+
					 1.*std::sin(theta)*qShapes[a]);
	
	// Update the stiffness
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    for(int b=0; b<nDof; ++b)
	      // This variation DELTA F = N_b
	      (*dfuncval)[0][a][0][b] += Qwts[q]*(EI*qDShapes[a]*qDShapes[b] +
						  LambdaH*std::cos(theta)*qShapes[a]*qShapes[b]+
						  LambdaV*std::sin(theta)*qShapes[a]*qShapes[b]);
      }
    return;
  }
  
  
  // Consistency test
  bool ElasticaHSensitivityOp::ConsistencyTest(const void* arg,
					     const double pertEPS,
					     const double tolEPS) const
  { return Sensitivity_Consistency_Test(arg, *this, pertEPS, tolEPS); }
  
}
