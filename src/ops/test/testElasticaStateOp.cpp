// Sriramajayam

#include <rbc_ElasticaStateOp.h>
#include <rbc_Structs.h>
#include <P11DElement.h>
#include <random>
#include <cassert>
#include <cmath>

using namespace rbc;

int main()
{
  // Create one segment
  std::vector<double> coordinates{std::sqrt(2.), std::sqrt(10.)};
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new P11DElement<1>({1,2});
  StandardP11DMap L2GMap(ElmArray);

  // Random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(-1.,1.);

  // State a random state
  const int nNodes = 2;
  std::vector<double> state(nNodes);
  for(int a=0; a<2; ++a)
    state[a] = dist(gen);
  
  // Tolerances
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-5;

  // Create a configuration
  StateConfiguration config;
  config.state = &state;
  config.L2GMap = &L2GMap;
  
  // Test class
  const double EI = std::sqrt(10.)+dist(gen);
  const double LambdaH = std::sqrt(5.)+dist(gen);
  const double LambdaV = std::sqrt(5.)+dist(gen);
  ElasticaStateOp Op(0, ElmArray[0], EI, LambdaH, LambdaV);
  assert(Op.GetField().size()==1 && "Unexpected number of fields");
  assert(Op.GetField()[0]==0 && "Unexpected field number");
  assert(Op.GetFieldDof(0)==2 && "Unexpected number of dofs");
  assert(Op.GetElement()==ElmArray[0] && "Unexpected element returned");
  assert(Op.ConsistencyTest(&config, pertEPS, tolEPS) && "Failed consistency test");
  assert(std::abs(Op.GetModulus()-EI)<1.e-10);
  double hload=0., vload=0.;
  Op.GetLoads(hload, vload);
  assert(std::abs(hload-LambdaH)+std::abs(vload-LambdaV)<1.e-10);
  delete ElmArray[0]; 
}

