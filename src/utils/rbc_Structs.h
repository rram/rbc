// Sriramajayam

#ifndef RBC_ELASTICA_STRUCTS_H
#define RBC_ELASTICA_STRUCTS_H

#include <utility>
#include <cassert>
#include <vector>

// Forward declarations
class LocalToGlobalMap;

namespace rbc
{
  // Configuration consisting of a sensitivity and state variable
  struct StateConfiguration
  {
    const std::vector<double>* state;
    const LocalToGlobalMap* L2GMap;
  };

  struct SensitivityConfiguration
  {
    const std::vector<double>* state;
    const std::vector<double>* sensitivity;
    const LocalToGlobalMap* L2GMap;
  };
  
  // Sensitivity type
  enum class SensitivityType {H, V, M, BC, Invalid};

  // Loads
  struct LoadParams
  {
    double HVal; // Horizontal component of load
    double VVal; // Vertical component of load
    int MDof; //! Dof to impose moment boundary condition
    double MVal; // Moment applied at the far end
    inline void Check() const
    { assert(MDof>=0); }
  };

  //! Helper struct to convey origin
  struct OriginParams
  {
    int node;
    double coords[2];
  };

  //! Helper struct for algorithmic parameters
  struct SolveParams
  {
    double EPS; //!< Absolute tolerance
    double resScale; //!< Scaling factor for convergence of residuals
    double dofScale; //!< Scaling factor for convergence of dofs
    int nMaxIters; //!< Max number of iterations
    bool verbose; //!< Print convergence details
    
    //! Sanity check on parameters
    inline void Check() const
    { assert(EPS>0. && resScale>0. && dofScale>0. && nMaxIters>0); }
  };
 
    
  struct NLopt_Params
  {
    double ftol_rel, xtol_rel, gtol;
    
    inline NLopt_Params(){}
    
    inline void Check()
    {
      assert(ftol_rel>0. && xtol_rel>0. && gtol>0.);
    }
  };

  struct NLSolverParams
  {
    int    digits; //!< Number of digits required to converge
    int    max_iter; //!< Maximum number of permitted iterations
    double normTol; //!< Tolerance for the norms of vectors. Vector norms will be checked to be larger than this.
  };
  
}

#endif
