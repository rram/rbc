// Sriramajayam

#ifndef RBC_STATE_SNES_FUNCS_H
#define RBC_STATE_SNES_FUNCS_H

#include <iostream>
#include <cassert>

namespace rbc
{
  // Assemble the residual for the state
  inline PetscErrorCode State_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    Elastica::SNESContext* ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "rbc::State_Residual_Func- context is null");

    // Unpackage
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    auto& theta = elastica.theta;
    auto& Asm = *elastica.state_Asm;
    const auto& L2GMap = *elastica.L2GMap;
    const auto& dirichlet_dof = bc_params.first;
    const auto& dirichlet_value = bc_params.second;
    const auto& mDof = load_params.MDof;

    // Set the configuration at which to evaluate the residual (PETSc guess)
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { double val;
	ierr = VecGetValues(solVec, 1, &n, &val); CHKERRQ(ierr);
	theta[n] = val; }
      
    // Update loading in state operations
    elastica.UpdateStateOperations(load_params);

    // Assemble the residual
    StateConfiguration config;
    config.state = &theta;
    config.L2GMap = &L2GMap;
    Asm.Assemble(&config, resVec);

    // Add moment boundary condition into the residual
    ierr = VecSetValues(resVec, 1, &mDof, &load_params.MVal, ADD_VALUES); CHKERRQ(ierr);
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // Set dirichlet BCs into the residual
    {
      double val = theta[dirichlet_dof]-dirichlet_value;
      ierr = VecSetValues(resVec, 1, &dirichlet_dof, &val, INSERT_VALUES); CHKERRQ(ierr);
    }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // -- done --
    return 0;
  }


  // Function for evaluating the Jacobian for the state
  inline PetscErrorCode State_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
  {
    // Get the context
    Elastica::SNESContext* ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "rbc::State_Jacobian_Func- context is null");

    // Unpackage
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    auto& theta = elastica.theta;
    auto& Asm = *elastica.state_Asm;
    const auto& L2GMap = *elastica.L2GMap;
    auto& tempVec = elastica.theta_solver.tempVec;
    const auto& dirichlet_dof = bc_params.first;

    // Set the configuration at which to evaluate the residual (PETSc guess)
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { double val;
	ierr = VecGetValues(solVec, 1, &n, &val); CHKERRQ(ierr);
	theta[n] = val; }
      
    // Update loads in state operations
    elastica.UpdateStateOperations(load_params);

    // Assemble
    StateConfiguration config;
    config.state = &theta;
    config.L2GMap = &L2GMap;
    Asm.Assemble(&config, tempVec, kMat);
      
    // Zero out Dirichlet dof
    ierr = MatZeroRows(kMat, 1, &dirichlet_dof, 1., PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    // -- done --
    return 0;
  }
}


#endif
