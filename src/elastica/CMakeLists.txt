# Sriramajayam

add_library(rbc_elastica STATIC
  rbc_Elastica.cpp
  rbc_Elastica_StateConsistency.cpp
  rbc_Elastica_SensitivityConsistency.cpp
  rbc_Elastica_Compute.cpp
  rbc_Elastica_Cartesian.cpp)

# headers
target_include_directories(rbc_elastica PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

target_link_libraries(rbc_elastica PUBLIC rbc_ops)

# Add required flags
target_compile_features(rbc_elastica PUBLIC ${rbc_COMPILE_FEATURES})

install(FILES
  "rbc_Elastica.h"
  "rbc_Elastica_State_SNES_Funcs.h"
  "rbc_Elastica_Sensitivity_SNES_Funcs.h"
  DESTINATION ${PROJECT_NAME}/include)
