// Sriramajayam

#ifndef RBC_ELASTICA_H
#define RBC_ELASTICA_H

#include <array>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <Assembler.h>
#include <SNES_Solver.h>
#include <rbc_ElasticaStateOp.h>
#include <rbc_ElasticaHSensitivityOp.h>
#include <rbc_ElasticaVSensitivityOp.h>
#include <rbc_ElasticaBCSensitivityOp.h>
#include <rbc_Structs.h>

namespace rbc
{
  //! Class for computing state and sensitivities of elastica solutions
  class Elastica
  {
  public:
    //! Constructor
    //! \param[in] coords Nodal coordinates (1D). Copied.
    //! \param[in] conn Element connectuivities. Referred to, not copied
    //! \param[in] ei Modulus. Copied
    //! \warning Assumes that the global coordinates have been set
    Elastica(const std::vector<double>& coords, const double ei);

    //! Destructor
    virtual ~Elastica();

    //! Disable copy and assignment
    Elastica(const Elastica&) = delete;
    Elastica& operator=(const Elastica&) = delete;

    //! Returns the element array
    inline const std::vector<Element*>& GetElementArray() const
    { return ElmArray; }

    //! Returns the local to global map
    inline const LocalToGlobalMap& GetLocalToGlobalMap() const
    { return *L2GMap; }
      
    //! Returns the coordinates array
    inline const std::vector<double>& GetCoordinates() const
    { return coordinates; }
    
    //! Returns the connectivity
    inline const std::vector<int>& GetConnectivity() const
    { return connectivity; }
  
    //! Returns the number of elements
    inline int GetNumElements() const
    { return nElements; }

    //! Returns the number of nodes
    inline int GetNumNodes() const
    { return nNodes; }

    //! Returns the length of the elastica
    inline double GetLength() const
    { return coordinates.back()-coordinates.front(); }

    //! Returns the mesh size, assumed to be uniform.
    inline double GetMeshSize() const
    { return (coordinates.back()-coordinates.front())/static_cast<double>(nElements); }
    
    //! Returns the state
    inline const std::vector<double>& GetStateField() const
    { return theta; }

    //! Returns the Cartesian coordinates of the nodes
    inline const std::vector<double>& GetCartesianCoordinates() const
    { return xy; }

    //! Set the state field
    //! Useful when setting the initial guess
    inline void SetStateField(const double* vals)
    {
      const int nDof = L2GMap->GetTotalNumDof();
      for(int a=0; a<nDof; ++a)
	theta[a] = vals[a];
      state_is_dirty = true;
    }

    //! Returns the requested sensitivity number
    inline const std::vector<double>& GetSensitivityField(const SensitivityType stype) const
    {
      assert(!sense_is_dirty &&  "rbc::Elastica::GetSensitivityField- Sensitivities are dirty");
      if(stype==SensitivityType::H) return alpha_H;
      else if(stype==SensitivityType::V) return alpha_V;
      else if(stype==SensitivityType::M) return alpha_M;
      else return alpha_BC;
    }

    //! Returns the sensitivities of nodal Cartesian coordinates
    inline const std::vector<double>& GetCartesianCoordinatesSensitivity(const SensitivityType stype) const
    {
      assert(!sense_is_dirty && "rbc::Elastica::GetCartesianCoordinatesSensitivity- Sensitivities are dirty");
      if(stype==SensitivityType::H) return dxy_H;
      else if(stype==SensitivityType::V) return dxy_V;
      else if(stype==SensitivityType::M) return dxy_M;
      else return dxy_BC;
    }
    
    //! Main functionality
    //! Computes the solution theta for a given set of loads
    //! \param[in] loads Set of loads
    //! \param[in] bc_params Dirichlet BCs
    //! \param[in] solve_params Parameters for the solution algorithm
    virtual void ComputeState(const LoadParams& load_params,
			      const std::pair<int,double>& bc_params,
			      const OriginParams& xy_params,
			      const SolveParams& solve_params);

    //! Computes  sensitivities with respect to loads and boundary conditions
    //! The state is recomputed.
    //! \param[in] loads Set of loads
    //! \param[in] bc_params Dirichlet BCs
    //! \param[in] solve_params Parameters for the solution algorithm
    virtual void ComputeStateAndSensitivities(const LoadParams& load_params,
					      const std::pair<int,double>& bc_params,
					      const OriginParams& xy_params,
					      const SolveParams& solve_params);

    //! Update loads in state operations
    void UpdateStateOperations(const LoadParams& load_params);

    //! Update loads in sensitivity operations
    void UpdateSensitivityOperations(const LoadParams& load_params);
    
    
    //! Consistency test for state calculations
    void StateConsistencyTest(const double* stateVals,
			      const LoadParams& load_params,
			      const std::pair<int,double>& bc_params);

    //! Consistency test for sensitivity calculations
    void SensitivityConsistencyTest(const SensitivityType stype,
				    const double* alphaVals,
				    const LoadParams& load_params,
				    const std::pair<int,double>& bc_params);

    //! Overload extraction operator
    friend std::ostream& operator<<(std::ostream& output, const Elastica& str);
    
  protected:
    //! Compute nodal cartesian coordinates
    void ComputeCartesianCoordinates(const OriginParams& xy_params);

    //! Compute sensitivities of cartesian coordinates
    void ComputeCartesianCoordinateSensitivities(const OriginParams& xy_params);
    
    // Members
    const std::vector<double> coordinates; //!< copy of nodal coordinates
    std::vector<int> connectivity; //!< Reference to element connectivities
    const double EI;
    const int nNodes; //!< Number of nodes
    const int nElements; //!< Number of elements
    std::vector<Element*> ElmArray; //!< Element array
    LocalToGlobalMap* L2GMap; //!< Local to global map

    // For state calculations
    std::vector<ElasticaStateOp*> state_OpsArray; //!< Operations to compute the state 
    StandardAssembler<ElasticaStateOp>* state_Asm; //!< Assembler for OpsArray calculations
    std::vector<double> theta; //!< Theta field
    std::vector<double> xy; //! Nodal cartesian coordinates
    
    // Sensitivities wrt horizontal loads
    std::vector<double> alpha_H;  
    std::vector<ElasticaHSensitivityOp*> HSense_OpsArray; 
    StandardAssembler<ElasticaHSensitivityOp>* HSense_Asm;
    std::vector<double> dxy_H; //! H-sensitivity of nodal Cartesian coordinates

    // Sensitivities wrt vertical loads
    std::vector<double> alpha_V;  
    std::vector<ElasticaVSensitivityOp*> VSense_OpsArray; 
    StandardAssembler<ElasticaVSensitivityOp>* VSense_Asm;
    std::vector<double> dxy_V; //! V-sensitivity of nodal Cartesian coordinates

    // Sensitivities wrt boundary conditions
    std::vector<double> alpha_BC;  
    std::vector<ElasticaBCSensitivityOp*> BCSense_OpsArray; 
    StandardAssembler<ElasticaBCSensitivityOp>* BCSense_Asm;
    std::vector<double> dxy_BC; //! BC-sensitivity of nodal Cartesian coordinates

    // Sensitivities wrt moment loads. Same operations and assembler as BC
    std::vector<double> alpha_M;
    std::vector<double> dxy_M; //! M-sensitivity of nodal Cartesian coordinates
    
    SNESSolver theta_solver; //!< Nonlinear solver for the state
    SNESSolver alpha_solver; //!< (Non)linear solver for sensitivities

    // Monitor whether state and sensitivities are clean/dirty
    mutable bool state_is_dirty;
    mutable bool sense_is_dirty;

    // Context for the solver
    struct SNESContext
    {
      Elastica* elastica;
      const LoadParams* load_params;
      const std::pair<int,double>* bc_params;
      const SolveParams* solve_params;
      SensitivityType stype;
    };
    
    // Friends
    friend PetscErrorCode State_Residual_Func(SNES, Vec, Vec, void*);
    friend PetscErrorCode State_Jacobian_Func(SNES, Vec, Mat, Mat, void*);
    friend PetscErrorCode Sensitivity_Residual_Func(SNES, Vec, Vec, void*);
    friend PetscErrorCode Sensitivity_Jacobian_Func(SNES, Vec, Mat, Mat, void*);
  };
}

#endif
