// Sriramajayam

#include <rbc_Elastica.h>
#include <P11DElement.h>
#include <iostream>
#include <cassert>
#include <rbc_Elastica_State_SNES_Funcs.h>
#include <rbc_Elastica_Sensitivity_SNES_Funcs.h>

namespace rbc
{
  // Constructor
  Elastica::Elastica(const std::vector<double>& coords, const double ei)
    :coordinates(coords),
     EI(ei),
     nNodes(static_cast<int>(coords.size())),
     nElements(nNodes-1),
     xy(2*coords.size()),
     dxy_H(2*coords.size()),
     dxy_V(2*coords.size()),
     dxy_BC(2*coords.size()),
     dxy_M(2*coords.size()),
     state_is_dirty(true),
     sense_is_dirty(true)
  {
    // Check that PETSc has been initialized
    PetscBool isInitialized;
    PetscInitialized(&isInitialized);
    assert(isInitialized==PETSC_TRUE);
    
    // Check that nodes are in sequence
    for(int n=1; n<nNodes; ++n)
      { assert(coordinates[n]>coordinates[n-1]); }

    // Create connectivity
    connectivity.resize(2*nElements);
    for(int e=0; e<nElements; ++e)
      { connectivity[2*e] = e+1;
	connectivity[2*e+1] = e+2; }
	
    // Create elements
    Segment<1>::SetGlobalCoordinatesArray(coordinates);
    ElmArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new P11DElement<1>(connectivity[2*e], connectivity[2*e+1]);

    // Local to global map
    L2GMap = new StandardP11DMap(ElmArray);

    // State variable calculations
    //------------------------------
    // Operations: set a trivial load
    state_OpsArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      state_OpsArray[e] = new ElasticaStateOp(e, ElmArray[e], EI, 0., 0.);
    
    // Assembler
    state_Asm = new StandardAssembler<ElasticaStateOp>(state_OpsArray, *L2GMap);
    // State
    theta.resize(nNodes);
    std::fill(theta.begin(), theta.end(), 0.);
    
    // Sensitivity variable calculations
    // -----------------------------------
    // Horizontal/Vertical/Moment/BCs
    alpha_H.resize(nNodes);
    alpha_V.resize(nNodes);
    alpha_BC.resize(nNodes);
    alpha_M.resize(nNodes);
    std::fill(alpha_H.begin(), alpha_H.end(), 0.);
    std::fill(alpha_V.begin(), alpha_V.end(), 0.);
    std::fill(alpha_BC.begin(), alpha_BC.end(), 0.);
    std::fill(alpha_M.begin(), alpha_M.end(), 0.);
    HSense_OpsArray.resize(nElements);
    VSense_OpsArray.resize(nElements);
    BCSense_OpsArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      {
	HSense_OpsArray[e]  = new ElasticaHSensitivityOp(e, ElmArray[e], EI, 0., 0.);
	VSense_OpsArray[e]  = new ElasticaVSensitivityOp(e, ElmArray[e], EI, 0., 0.);
	BCSense_OpsArray[e] = new ElasticaBCSensitivityOp(e, ElmArray[e], EI, 0., 0.);
      }
    HSense_Asm  = new StandardAssembler<ElasticaHSensitivityOp>(HSense_OpsArray, *L2GMap);
    VSense_Asm  = new StandardAssembler<ElasticaVSensitivityOp>(VSense_OpsArray, *L2GMap);
    BCSense_Asm = new StandardAssembler<ElasticaBCSensitivityOp>(BCSense_OpsArray, *L2GMap);
    
    // State sovler
    std::vector<int> nnz;
    state_Asm->CountNonzeros(nnz);
    theta_solver.Initialize(nnz, State_Residual_Func, State_Jacobian_Func);
    
    // Sensitivity solver
    nnz.clear();
    HSense_Asm->CountNonzeros(nnz); // Same non-zero patterns for all sensitivities
    alpha_solver.Initialize(nnz, Sensitivity_Residual_Func, Sensitivity_Jacobian_Func);
    
    // -- done --
  }


  // Destructor
  Elastica::~Elastica()
  {
    for(auto& x:ElmArray) delete x;
    delete L2GMap;
    for(auto& x:state_OpsArray) delete x;
    delete state_Asm;
    for(auto& x:HSense_OpsArray) delete x;
    for(auto& x:VSense_OpsArray) delete x;
    for(auto& x:BCSense_OpsArray) delete x;
    delete HSense_Asm;
    delete VSense_Asm;
    delete BCSense_Asm;
    theta_solver.Destroy();
    alpha_solver.Destroy();
  }

  // Update loads in state operations
  void Elastica::UpdateStateOperations(const LoadParams& load_params)
  {
    for(auto& op:state_OpsArray)
      op->SetLoad(load_params.HVal, load_params.VVal);
    return;
  }

  // Update loads in sensitivity operations
  void Elastica::UpdateSensitivityOperations(const LoadParams& load_params)
  {
    for(auto& op:HSense_OpsArray)
      op->SetLoadValues(load_params.HVal, load_params.VVal);
    for(auto& op:VSense_OpsArray)
      op->SetLoadValues(load_params.HVal, load_params.VVal);
    for(auto& op:BCSense_OpsArray)
      op->SetLoadValues(load_params.HVal, load_params.VVal);

    return;
  }

  // Overload extraction operator
  std::ostream& operator<<(std::ostream& output, const Elastica& str)
  {
    const auto& coords = str.GetCoordinates();
    const auto& theta = str.GetStateField();
    const auto& xy = str.GetCartesianCoordinates();
    const int nNodes = static_cast<int>(coords.size());
    for(int n=0; n<nNodes; ++n)
      output << coords[n] << "\t" << theta[n] << "\t" << xy[2*n] << "\t" <<xy[2*n+1] <<"\n";

    return output;
  }
  
}
