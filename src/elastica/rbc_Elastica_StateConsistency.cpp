// Sriramajayam

#include <rbc_Elastica.h>
#include <rbc_Elastica_State_SNES_Funcs.h>
#include <iostream>

namespace rbc
{
  // Consistency test for state calculations
  void Elastica::StateConsistencyTest(const double* stateVals,
				      const LoadParams& load_params,
				      const std::pair<int,double>& bc_params)
  {
    // Perturbation values
    const double pEPS = 1.e-6;
    const double mEPS = -1.e-6;
    
    // Access the solver internals
    auto& snes = theta_solver.snes;
    auto& solVec = theta_solver.solVec;
    auto& resVec = theta_solver.resVec;
    auto& kMat = theta_solver.kMat;

    // Set values for the state
    const int nDof = L2GMap->GetTotalNumDof();
    PetscErrorCode ierr;
    for(int i=0; i<nDof; ++i)
      {
	ierr = VecSetValues(solVec, 1, &i, &stateVals[i], INSERT_VALUES); CHKERRV(ierr);
	theta[i] = stateVals[i];
      }
    ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);
    
    // Update loads in state operations
    UpdateStateOperations(load_params);

    // Dummy solve parameters with consistent linearization on
    SolveParams solve_params({.EPS=1.e-10, .resScale=1., .dofScale=1., .nMaxIters=10, .verbose=true});
    
    // Context
    SNESContext ctx;
    ctx.elastica = this;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    ctx.stype = SensitivityType::Invalid;
    ierr = SNESSetApplicationContext(snes, &ctx); CHKERRV(ierr);

    // Assemble the stiffness matrix
    ierr = State_Jacobian_Func(snes, solVec, kMat, kMat, nullptr); CHKERRV(ierr);
    
    // Compute the residual at a pair of perturbed states
    Vec resPlus;
    ierr = VecDuplicate(resVec, &resPlus); CHKERRV(ierr);
    Vec resMinus;
    ierr = VecDuplicate(resVec, &resMinus); CHKERRV(ierr);
    Vec solPlus;
    ierr = VecDuplicate(solVec, &solPlus); CHKERRV(ierr);
    Vec solMinus;
    ierr = VecDuplicate(solVec, &solMinus); CHKERRV(ierr);

    // Perturb and compute residuals
    // Don't alter dirichlet dofs
    for(int a=0; a<nDof; ++a)
      if(a!=bc_params.first)
	{
	  // Positive perturbation
	  ierr = VecCopy(solVec, solPlus); CHKERRV(ierr);
	  ierr = VecSetValues(solPlus, 1, &a, &pEPS, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solPlus); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solPlus); CHKERRV(ierr);
	  for(int n=0; n<nDof; ++n)
	    { double val;
	      ierr = VecGetValues(solPlus, 1, &n, &val); CHKERRV(ierr);
	      theta[n] = val; }
	  
	  // Residual at positive perturbation
	  UpdateStateOperations(load_params);
	  ierr = State_Residual_Func(snes, solPlus, resPlus, nullptr); CHKERRV(ierr);

	  // Negative perturbation
	  ierr = VecCopy(solVec, solMinus); CHKERRV(ierr);
	  ierr = VecSetValues(solMinus, 1, &a, &mEPS, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solMinus); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solMinus); CHKERRV(ierr);
	  for(int n=0; n<nDof; ++n)
	    { double val;
	      ierr = VecGetValues(solMinus, 1, &n, &val); CHKERRV(ierr);
	      theta[n] = val; }
	  
	  // Residual at negative perturbation
	  UpdateStateOperations(load_params);
	  ierr = State_Residual_Func(snes, solMinus, resMinus, nullptr); CHKERRV(ierr);
	  
	  // Compare numerical and implemented stiffness
	  for(int b=0; b<nDof; ++b)
	    if(b!=bc_params.first)
	      {
		double kval;
		ierr = MatGetValues(kMat, 1, &b, 1, &a, &kval); CHKERRV(ierr);
		double rplus, rminus;
		ierr = VecGetValues(resPlus, 1, &b, &rplus); CHKERRV(ierr);
		ierr = VecGetValues(resMinus, 1, &b, &rminus); CHKERRV(ierr);
		double knum = (rplus-rminus)/(pEPS-mEPS);
		//std::cout<<"\nK("<<a<<","<<b<<"): "<<kval<<" should be "<<knum<<std::flush;
		if(std::abs(knum-kval)>1.e-5)
		  {
		    //std::cout<<"\nrbc::TendonElastica::StateConsistency failed: "
		    //	 <<kval<<" should be close to "<<knum<<std::flush;
		    //std::cout<<"  <---- "<<std::flush;
		    assert(std::abs(knum-kval)<1.e-5);
		  }
	      }
	}

    // Clean up
    ierr = VecDestroy(&resPlus); CHKERRV(ierr);
    ierr = VecDestroy(&resMinus); CHKERRV(ierr);
    ierr = VecDestroy(&solMinus); CHKERRV(ierr);
    ierr = VecDestroy(&solPlus); CHKERRV(ierr);
    return;
  }
    
}
