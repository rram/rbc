// Sriramajayam

#ifndef RBC_SENSITIVITY_SNES_FUNCS_H
#define RBC_SENSITIVITY_SNES_FUNCS_H

namespace rbc
{
  // Function for evaluating sensitivity residuals
  inline PetscErrorCode Sensitivity_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    Elastica::SNESContext* ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "el::Sensitivity_Residual_Func- context is null");

    // Unpackage
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    const auto& stype = ctx->stype;
    const auto& L2GMap = *elastica.L2GMap;
    const auto& theta = elastica.theta;
    const auto& mDof = load_params.MDof;
    std::vector<double>* alphas[] = {&elastica.alpha_H, &elastica.alpha_V, &elastica.alpha_M, &elastica.alpha_BC};
    const auto& dirichlet_dof = bc_params.first;
    const double dirichlet_vals[] = {0., 0., 0., 1.}; // H, V, M, BC
      
    // Pick out the requested sensitivity
    int snum = -1;
    if(stype==SensitivityType::H) snum = 0;
    else if(stype==SensitivityType::V) snum = 1;
    else if(stype==SensitivityType::M) snum = 2;
    else if(stype==SensitivityType::BC) snum = 3;
      
    else assert(false && "rbc::Sensitivity_SNES_Funcs- unexpected sensitivity type");
    auto& alpha = *alphas[snum];
    const auto& dirichlet_value = dirichlet_vals[snum];

    // Set the sensitivity values at which to evaluate the residual
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { double val;
	ierr = VecGetValues(solVec, 1, &n, &val); CHKERRQ(ierr);
	alpha[n] = val; }
      
    // Update sensitivity operations with given loads
    elastica.UpdateSensitivityOperations(load_params);

    // Assemble
    SensitivityConfiguration config;
    config.state = &theta;
    config.sensitivity = &alpha;
    config.L2GMap = &L2GMap;
    if(stype==SensitivityType::H)
      elastica.HSense_Asm->Assemble(&config, resVec);
    else if(stype==SensitivityType::V)
      elastica.VSense_Asm->Assemble(&config, resVec);
    else
      elastica.BCSense_Asm->Assemble(&config, resVec); // For both moment and BC

    // Add contribution from moment
    if(stype==SensitivityType::M)
      {
	double ONE = 1.;
	ierr = VecSetValues(resVec, 1, &mDof, &ONE, ADD_VALUES); CHKERRQ(ierr);
	ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
	ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      }
      
    // Set Dirichlet dof in the residual
    {
      double val = alpha[dirichlet_dof]-dirichlet_value;
      ierr = VecSetValues(resVec, 1, &dirichlet_dof, &val, INSERT_VALUES); CHKERRQ(ierr);
    }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // -- done--
    return 0;
  }
    
    
  // Function for evaluating sensitivity Jacobian
  inline PetscErrorCode Sensitivity_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
  {
    // Get the context
    Elastica::SNESContext *ctx;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &ctx);
    assert(ctx!=nullptr && "rbc::Sensitivity_Jacobian_Func- context is null");

    // Unpackage
    auto& elastica = *ctx->elastica;
    auto& load_params = *ctx->load_params;
    auto& bc_params = *ctx->bc_params;
    const auto& stype = ctx->stype;
    const auto& L2GMap = *elastica.L2GMap;
    const auto& theta = elastica.theta;
    std::vector<double>* alphas[] = {&elastica.alpha_H, &elastica.alpha_V,
				     &elastica.alpha_M, &elastica.alpha_BC};
    const auto& dirichlet_dof = bc_params.first;
    auto& tempVec = elastica.alpha_solver.tempVec;
      
    // Select the specified sensitivity type
    int snum = -1;
    if(stype==SensitivityType::H) snum = 0;
    else if(stype==SensitivityType::V) snum = 1;
    else if(stype==SensitivityType::M) snum = 2;
    else if(stype==SensitivityType::BC) snum = 3;
    else assert(false && "rbc::Sensitivity_SNES_Funcs- unexpected sensitivity type");
    auto& alpha = *alphas[snum];
      
    // Set the sensitivity values at which to evaluate the residual
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { double val;
	ierr = VecGetValues(solVec, 1, &n, &val); CHKERRQ(ierr);
	alpha[n] = val; }
      
    // Update loads in sensitivity operations
    elastica.UpdateSensitivityOperations(load_params);
      
    // Assemble
    SensitivityConfiguration config;
    config.state = &theta;
    config.sensitivity = &alpha;
    config.L2GMap = &L2GMap;
    if(stype==SensitivityType::H)
      elastica.HSense_Asm->Assemble(&config, tempVec, kMat);
    else if(stype==SensitivityType::V)
      elastica.VSense_Asm->Assemble(&config, tempVec, kMat);
    else
      elastica.BCSense_Asm->Assemble(&config, tempVec, kMat); // For both M and BC
      
    // Set Dirichlet bcs in the stiffness matrix
    ierr = MatZeroRows(kMat, 1, &dirichlet_dof, 1., PETSC_NULL, PETSC_NULL);
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // -- done --
    return 0;
  }
    
}

#endif
