// Sriramajayam

#include <rbc_Elastica.h>

namespace rbc
{
  // Compute nodal cartesian coordinates
  void Elastica::ComputeCartesianCoordinates(const OriginParams& xy_params)
  {
    assert(state_is_dirty==false);

    // Compute without setting the origin at the prescribed node
    std::fill(xy.begin(), xy.end(), 0.);
    const int nElements = static_cast<int>(ElmArray.size());
    const int nNodes = L2GMap->GetTotalNumDof();
    double delta_x, delta_y, thetaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto* Elm = ElmArray[e];
	const auto& Qwts = Elm->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = Elm->GetDof(0);
	delta_x = 0.; // integral over this element
	delta_y = 0.;
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta value here
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += theta[L2GMap->Map(0,a,e)]*Elm->GetShape(0,q,a);

	    // Integrate
	    delta_x += Qwts[q]*std::cos(thetaval);
	    delta_y += Qwts[q]*std::sin(thetaval);
	  }

	// Update Cartesian coordinates
	xy[2*(e+1)+0] = xy[2*e+0] + delta_x;
	xy[2*(e+1)+1] = xy[2*e+1] + delta_y;
      }

    // Set the prescribed coordinates at the specified node
    assert(xy_params.node>=0 && xy_params.node<nNodes);
    double offset[] = {xy_params.coords[0]-xy[2*xy_params.node],
		       xy_params.coords[1]-xy[2*xy_params.node+1]};
    for(int n=0; n<nNodes; ++n)
      {
	xy[2*n+0] += offset[0];
	xy[2*n+1] += offset[1];
      }
    assert(std::abs(xy[2*xy_params.node]-xy_params.coords[0]) +
	   std::abs(xy[2*xy_params.node+1]-xy_params.coords[1])<1.e-7);
    
    // done
    return;
  }



  // Compute sensitivities of cartesian coordinates
  void Elastica::ComputeCartesianCoordinateSensitivities(const OriginParams& xy_params)
  {
    assert(state_is_dirty==false && sense_is_dirty==false);
	
    // Initialize to zero
    std::fill(dxy_H.begin(), dxy_H.end(), 0.);
    std::fill(dxy_V.begin(), dxy_V.end(), 0.);
    std::fill(dxy_BC.begin(), dxy_BC.end(), 0.);
    std::fill(dxy_M.begin(), dxy_M.end(), 0.);

    // Access state and sensitivities
    const int nElements = static_cast<int>(ElmArray.size());
    const int nNodes = L2GMap->GetTotalNumDof();
    // H, V, M, BC
    const double* alpha_vals[4] = {alpha_H.data(), alpha_V.data(), alpha_M.data(), alpha_BC.data()};
    
    double thetaval;
    double alphaval[4];
    double delta_alpha_x[4], delta_alpha_y[4];
    for(int e=0; e<nElements; ++e)
      {
	const auto* Elm = ElmArray[e];
	const auto& Qwts = Elm->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = Elm->GetDof(0);
	for(int k=0; k<4; ++k)
	  { delta_alpha_x[k] = 0.;
	    delta_alpha_y[k] = 0.; }
	
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta and sensitivities
	    thetaval = 0.;
	    for(int k=0; k<4; ++k)
	      alphaval[k] = 0.;
	    for(int a=0; a<nDof; ++a)
	      {
		const double& Na = Elm->GetShape(0,q,a);
		const int dofnum = L2GMap->Map(0,a,e);
		thetaval += theta[dofnum]*Na;
		for(int k=0; k<4; ++k)
		  alphaval[k] += alpha_vals[k][dofnum]*Na;
	      }

	    // Integrate
	    for(int k=0; k<4; ++k)
	      {
		delta_alpha_x[k] -= Qwts[q]*std::sin(thetaval)*alphaval[k];
		delta_alpha_y[k] += Qwts[q]*std::cos(thetaval)*alphaval[k];
	      }
	  }

	// Update nodal sensitivities
	const int n0 = L2GMap->Map(0,0,e); assert(n0==e);
	const int n1 = L2GMap->Map(0,1,e); assert(n1==e+1);
	// H
	dxy_H[2*n1+0] += dxy_H[2*n0+0] + delta_alpha_x[0];
	dxy_H[2*n1+1] += dxy_H[2*n0+1] + delta_alpha_y[0];
	// V
	dxy_V[2*n1+0] += dxy_V[2*n0+0] + delta_alpha_x[1];
	dxy_V[2*n1+1] += dxy_V[2*n0+1] + delta_alpha_y[1];
	// M
	dxy_M[2*n1+0] += dxy_M[2*n0+0] + delta_alpha_x[2];
	dxy_M[2*n1+1] += dxy_M[2*n0+1] + delta_alpha_y[2];
	// BC
	dxy_BC[2*n1+0] += dxy_BC[2*n0+0] + delta_alpha_x[3];
	dxy_BC[2*n1+1] += dxy_BC[2*n0+1] + delta_alpha_y[3];
      }

    // Revise origin
    const int& node = xy_params.node;
    assert(node>=0 && node<nNodes);
    const double offset[] = {dxy_H[2*node],  dxy_H[2*node+1],
			     dxy_V[2*node],  dxy_V[2*node+1],
			     dxy_M[2*node],  dxy_M[2*node+1],
			     dxy_BC[2*node], dxy_BC[2*node+1]};
    for(int n=0; n<nNodes; ++n)
      for(int k=0; k<2; ++k)
	{
	  dxy_H[2*n+k]  -= offset[k];
	  dxy_V[2*n+k]  -= offset[2+k];
	  dxy_M[2*n+k]  -= offset[4+k];
	  dxy_BC[2*n+k] -= offset[6+k];
	}
    assert(std::abs(dxy_H[2*node])+std::abs(dxy_H[2*node+1])+
	   std::abs(dxy_V[2*node])+std::abs(dxy_V[2*node+1])+
	   std::abs(dxy_M[2*node])+std::abs(dxy_M[2*node+1])+
	   std::abs(dxy_BC[2*node])+std::abs(dxy_BC[2*node+1])<1.e-7);
    
    // done
    return;
  }
}
