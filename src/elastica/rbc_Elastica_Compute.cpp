// Sriramajayam

#include <rbc_Elastica.h>
#include <cassert>
#include <iostream>

namespace rbc
{    
  // Main functionality
  void Elastica::ComputeState(const LoadParams& load_params,
			      const std::pair<int,double>& bc_params,
			      const OriginParams& xy_params,
			      const SolveParams& solve_params)
  {
    if(solve_params.verbose)
      std::cout<<"\nrbc::Elastica::Computing state..."<<std::flush;
    
    // Set the current theta as the initial guess
    const double* init_guess = theta.data();

    // Package context
    SNESContext ctx;
    ctx.elastica = this;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    ctx.stype = SensitivityType::Invalid;

    // Solve
    theta_solver.Solve(init_guess, &ctx, solve_params.verbose);

    // Get the solution
    double* dofValues;
    PetscErrorCode ierr = VecGetArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);
    const int nDofs = L2GMap->GetTotalNumDof();
    for(int i=0; i<nDofs; ++i)
      theta[i] = dofValues[i];
    ierr = VecRestoreArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);

    // Mark state as not dirty and sensitivities as dirty
    state_is_dirty = false;
    sense_is_dirty = true;

    // Compute nodal cartesian coordinates
    ComputeCartesianCoordinates(xy_params);
    
    // -- done --
    return;
  }


  // Computes the sensitivity at a given set of loads
  void Elastica::ComputeStateAndSensitivities(const LoadParams& load_params,
					      const std::pair<int,double>& bc_params,
					      const OriginParams& xy_params,
					      const SolveParams& solve_params)
  {
    // Compute the state
    ComputeState(load_params, bc_params, xy_params, solve_params);
    assert(state_is_dirty==false && sense_is_dirty==true);

    if(solve_params.verbose)
      std::cout<<"\nrbc::Elastica Computing sensitivities..."<<std::flush;

    // Context
    SNESContext ctx;
    ctx.elastica = this;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    const int nDofs = L2GMap->GetTotalNumDof();
    
    // Compute all four sensitivities
    std::vector<double>* senses[] = {&alpha_H, &alpha_V, &alpha_M, &alpha_BC};
    SensitivityType stypes[] = {SensitivityType::H, SensitivityType::V,
				SensitivityType::M, SensitivityType::BC};
    for(int s=0; s<4; ++s)
      {
	auto& alpha = *senses[s];
	const double* init_guess = alpha.data();
	ctx.stype = stypes[s];
	alpha_solver.Solve(init_guess, &ctx, solve_params.verbose);
	double* dofValues;
	PetscErrorCode ierr = VecGetArray(alpha_solver.solVec, &dofValues); CHKERRV(ierr);
	for(int i=0; i<nDofs; ++i)
	  alpha[i] = dofValues[i];
	ierr = VecRestoreArray(alpha_solver.solVec, &dofValues); CHKERRV(ierr);
      }
    
    // Mark sensitivities as clean
    sense_is_dirty = false;

    // Compute sensitivities of Cartesian coordinates
    ComputeCartesianCoordinateSensitivities(xy_params);
      
    return;
  }

  
}
