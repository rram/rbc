// Sriramajayam

#include <rbc_Elastica.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  
  // Generate a random values for state/sensitivities/loads
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);

  // Loads
  rbc::LoadParams load_params({.HVal=dis(gen), .VVal=dis(gen), .MDof=nNodes-1, .MVal=dis(gen)});
  
  // Dirichlet BCs
  std::pair<int,double> bc_params(0,dis(gen));

  // Create strut
  const double EI = std::sqrt(5.);
  rbc::Elastica *str = new rbc::Elastica(coordinates, EI);

  // Set random values for the state
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  str->SetStateField(&theta[0]);

  // Do a consistency test for the L-th sensitivity
  rbc::SensitivityType stypes[] = {rbc::SensitivityType::H, rbc::SensitivityType::V,
				   rbc::SensitivityType::M, rbc::SensitivityType::BC};
  for(int L=0; L<4; ++L)
    {
      // random values for sensitivities
      std::vector<double> alpha(nNodes);
      for(int i=0; i<nNodes; ++i)
	alpha[i] = dis(gen);

      // Check consistency
      str->SensitivityConsistencyTest(stypes[L], &alpha[0], load_params, bc_params);
    }

  // Clean up
  delete str;
  PetscFinalize();
}
