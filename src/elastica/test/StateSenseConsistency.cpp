// Sriramajayam

#include <rbc_Elastica.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>
#include <random>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 20;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  
  // Generate a random values for state/loads
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  rbc::LoadParams load_params({.HVal=dis(gen), .VVal=dis(gen), .MDof=nNodes-1, .MVal=dis(gen)});
  
  // Dirichlet BCs
  std::pair<int,double> bc_params(0, dis(gen));

  // Origin
  rbc::OriginParams origin_params{.node=nNodes/2, .coords={0.,0.}};
  
  // Create strut
  const double EI = 1.;
  rbc::Elastica *str = new rbc::Elastica(coordinates, EI);

  // Solve parameters
  rbc::SolveParams solve_params({.EPS=1.e-10, .resScale=1., .dofScale=1., .nMaxIters=25, .verbose=true});

  // Compute the sensivities at the given loads
  str->ComputeStateAndSensitivities(load_params, bc_params, origin_params, solve_params);
  const auto& alphaField1 = str->GetSensitivityField(rbc::SensitivityType::H);
  const auto& alphaField2 = str->GetSensitivityField(rbc::SensitivityType::V);
  const auto& alphaField3 = str->GetSensitivityField(rbc::SensitivityType::M);
  const auto& alphaField4 = str->GetSensitivityField(rbc::SensitivityType::BC);
  std::vector<double> alpha1(nNodes), alpha2(nNodes), alpha3(nNodes), alpha4(nNodes);
  for(int i=0; i<nNodes; ++i)
    { alpha1[i] = alphaField1[i];
      alpha2[i] = alphaField2[i];
      alpha3[i] = alphaField3[i];
      alpha4[i] = alphaField4[i]; }
  const auto& xy= str->GetCartesianCoordinates();
  const std::vector<double>* dxy[] = { &str->GetCartesianCoordinatesSensitivity(rbc::SensitivityType::H),
				       &str->GetCartesianCoordinatesSensitivity(rbc::SensitivityType::V),
				       &str->GetCartesianCoordinatesSensitivity(rbc::SensitivityType::M),
				       &str->GetCartesianCoordinatesSensitivity(rbc::SensitivityType::BC)};


  // Compute the state at H +/- epsilon * numerical difference
  std::vector<double> alpha1Num(nNodes);
  std::vector<double> dxy_num_H(2*nNodes);
  const double P_EPS = 1.e-4;
  load_params.HVal += P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  const auto& thetaField = str->GetStateField();
  for(int i=0; i<nNodes; ++i)
    { alpha1Num[i] = thetaField[i];
      for(int k=0; k<2; ++k)
	dxy_num_H[2*i+k] = xy[2*i+k];
    }
  load_params.HVal -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha1Num[i] -= thetaField[i];
      alpha1Num[i] /= (2.*P_EPS);
      for(int k=0; k<2; ++k)
	{ dxy_num_H[2*i+k] -= xy[2*i+k];
	  dxy_num_H[2*i+k] /= (2.*P_EPS); }
    }
  
  // Restore the original load
  load_params.HVal += P_EPS;

  // Compute the state at V +/- epsilon & numerical difference
  std::vector<double> alpha2Num(nNodes);
  std::vector<double> dxy_num_V(2*nNodes);
  load_params.VVal += P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha2Num[i] = thetaField[i];
      for(int k=0; k<2; ++k)
	dxy_num_V[2*i+k] = xy[2*i+k];
    }
  load_params.VVal -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha2Num[i] -= thetaField[i];
      alpha2Num[i] /= (2.*P_EPS);
      for(int k=0; k<2; ++k)
	{ dxy_num_V[2*i+k] -= xy[2*i+k];
	  dxy_num_V[2*i+k] /= (2.*P_EPS); }
    }
  // Restore the original load
  load_params.VVal += P_EPS;

  // Compute the state at M +/- epsilon & numerical difference
  std::vector<double> alpha3Num(nNodes);
  std::vector<double> dxy_num_M(2*nNodes);
  load_params.MVal += P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  const auto& xy_M_plus = str->GetCartesianCoordinates();
  for(int i=0; i<nNodes; ++i)
    { alpha3Num[i] = thetaField[i];
      for(int k=0; k<2; ++k)
	dxy_num_M[2*i+k] = xy[2*i+k]; }
  load_params.MVal -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha3Num[i] -= thetaField[i];
      alpha3Num[i] /= (2.*P_EPS);
      for(int k=0; k<2; ++k)
	{ dxy_num_M[2*i+k] -= xy[2*i+k];
	  dxy_num_M[2*i+k] /= (2.*P_EPS); }
    }
  // Restore the original load
  load_params.MVal += P_EPS;
  
  // Compute the state bc +/- epsilon & numerical difference
  std::vector<double> alpha4Num(nNodes);
  std::vector<double> dxy_num_BC(2*nNodes);
  bc_params.second += P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    {
      alpha4Num[i] = thetaField[i];
      for(int k=0; k<2; ++k)
	dxy_num_BC[2*i+k] = xy[2*i+k];
    }
  bc_params.second -= 2.*P_EPS;
  str->ComputeState(load_params, bc_params, origin_params, solve_params);
  for(int i=0; i<nNodes; ++i)
    { alpha4Num[i] -= thetaField[i];
      alpha4Num[i] /= (2.*P_EPS);
    for(int k=0; k<2; ++k)
      { dxy_num_BC[2*i+k] -= xy[2*i+k];
	dxy_num_BC[2*i+k] /= (2.*P_EPS); }
    }
  // Restore the original bc
  bc_params.second += P_EPS;
  
  // Plot the computed and numerical sensitivities of theta
  std::fstream pfile;
  pfile.open((char*)"ss.dat", std::ios::out);
  for(int i=0; i<nNodes; ++i)
    pfile <<coordinates[i]<<" "
	  <<alpha1[i]<<" "<<alpha1Num[i]<<" "
	  <<alpha2[i]<<" "<<alpha2Num[i]<<" "
	  <<alpha3[i]<<" "<<alpha3Num[i]<<" "
	  <<alpha4[i]<<" "<<alpha4Num[i]<<"\n";
  pfile.flush(); pfile.close();

  // Plot the computed and numerical sensitivities of xy
  pfile.open((char*)"xy.dat", std::ios::out);
  for(int i=0; i<nNodes; ++i)
    pfile<<coordinates[i]<<" "
	 <<(*dxy[0])[2*i]<<" "<<dxy_num_H[2*i]<<" "
	 <<(*dxy[0])[2*i+1]<<" "<<dxy_num_H[2*i+1]<<" "
	 <<(*dxy[1])[2*i]<<" "<<dxy_num_V[2*i]<<" "
	 <<(*dxy[1])[2*i+1]<<" "<<dxy_num_V[2*i+1]<<" "
	 <<(*dxy[2])[2*i]<<" "<<dxy_num_M[2*i]<<" "
	 <<(*dxy[2])[2*i+1]<<" "<<dxy_num_M[2*i+1]<<" "
	 <<(*dxy[3])[2*i]<<" "<<dxy_num_BC[2*i]<<" "
	 <<(*dxy[3])[2*i+1]<<" "<<dxy_num_BC[2*i+1]<<"\n";
  pfile.flush();
  pfile.close();
  for(int i=0; i<nNodes; ++i)
    for(int k=0; k<2; ++k)
      {
	assert(std::abs((*dxy[0])[2*i+k]-dxy_num_H[2*i+k])<1.e-3);
	assert(std::abs((*dxy[1])[2*i+k]-dxy_num_V[2*i+k])<1.e-3);
	assert(std::abs((*dxy[2])[2*i+k]-dxy_num_M[2*i+k])<1.e-3);
	assert(std::abs((*dxy[3])[2*i+k]-dxy_num_BC[2*i+k])<1.e-3);
      }
      
  // Clean up
  delete str;
  PetscFinalize();
}
  


