// Sriramajayam

#include <rbc_Elastica.h>
#include <P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  
  // Set load
  // ----------
  rbc::LoadParams load_params({.HVal=0., .VVal=1., .MDof=nNodes-1, .MVal=0.});
  
  // Dirichlet BCs: clamp the left end
  // ----------------------------------
  std::pair<int,double> bc_params(0,0.);

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};
  
  // Solve parameters
  // ------------------
  rbc::SolveParams solve_params({.EPS = 1.e-10,	.resScale = 1.,	.dofScale = 1.,	.nMaxIters = 25, .verbose = true});

  // Create strut
  // --------------
  const double EI = 1.;
  rbc::Elastica* str = new rbc::Elastica(coordinates, EI);
  
  // Compute the state and sensitivities
  // -----------------------------------
  str->ComputeState(load_params, bc_params, origin_params, solve_params);

  // Print
  // -------
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  stream << *str;
  stream.close();

  // Clean up
  delete str;
  PetscFinalize();
}
