// Sriramajayam

#ifndef RBC_CUT_PROFILE_MONITOR_H
#define RBC_CUT_PROFILE_MONITOR_H

#include <boost/icl/interval_set.hpp>
#include <rbc_ParametricCurve.h>
#include <rbc_Elastica.h>

namespace rbc
{
  using Interval = boost::icl::continuous_interval<double>;
  using IntervalSet = boost::icl::interval_set<double>;
  
  class CutProfileMonitor
  {
  public:

    //! Constructor
    CutProfileMonitor(const ParametricCurve& crv);

    //! Constructor
    CutProfileMonitor(const ParametricCurve& crv,
		      const std::vector<std::pair<double,double>>& cut_interval_state);

    //! Destructor
    inline virtual ~CutProfileMonitor() {}

    //! Disable copy and assignment
    CutProfileMonitor(const CutProfileMonitor&) = delete;
    CutProfileMonitor operator=(const CutProfileMonitor&) = delete;

    //! Main functionality: update the set of cut and uncut intervals
    void Update(const Elastica& elastica, const double sdTOL, const double tsizeTOL);

    //! Main functionality: update the set of cut and uncut intervals
    void InsertCut(const double t0, const double t1, const double tsizeTOL); 
		
    //! Access the set of uncut intervals
    std::vector<std::pair<double,double>> GetUncutIntervals() const;
    
    //! Access the number of uncut intervals
    inline int GetNumUncutIntervals() const
    { return boost::icl::interval_count(uncut_intervals); }

    //! Length of the uncut profile
    inline double GetUncutProfileLength() const
    { return boost::icl::length(uncut_intervals); }

    //! Access cut/uncut intervals
    std::vector<std::pair<double,double>> GetCutIntervals() const;

    //! Plot the cut intervals in tec format
    void PlotTecCutProfile(const std::string filename, const int nsamples) const;

    //! Plot the uncut intervals in tec format
    void PlotTecUncutProfile(const std::string filename, const int nsamples) const;
    
  private:
    const ParametricCurve& curve;
    Interval tInterval;
    IntervalSet cut_intervals;
    IntervalSet uncut_intervals;

    void CleanseCutIntervals(const double tsizeTOL);
    void UpdateUncutIntervals();
     
  };

}

#endif
