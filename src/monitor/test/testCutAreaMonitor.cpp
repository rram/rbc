// Sriramajayam

#include <rbc_CutAreaMonitor.h>
#include <iostream>
#include <fstream>

using namespace rbc;

int main()
{
  std::vector<double> corners{0.,0., 1.,0., 1.,1., 0.,1.};
  CutAreaMonitor monitor(corners);
  std::cout<<"\nArea: "<<monitor.GetUncutPolygonArea()<<std::flush;

  // Subtract area
  std::vector<double> cut_corners{-0.2,0.2, 0.8,0.2, 0.8,0.8, -0.2,0.8};
  monitor.Subtract(cut_corners);
  std::cout<<"\nArea: "<<monitor.GetUncutPolygonArea()
	   <<"\nShould be: 0.52"<<std::flush;
}
