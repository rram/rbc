// Sriramajayam

#include <rbc_CutProfileMonitor.h>
#include <rbc_CubicBezierCurve.h>
#include <iostream>

using namespace rbc;

void PrintIntervals(const CutProfileMonitor& monitor);

int main()
{
  // Create a Bezier curve
  NLopt_Params opt_params;
  opt_params.ftol_rel = 1.e-8;
  opt_params.xtol_rel = 1.e-8;
  
  NLSolverParams nl_params;
  nl_params.digits=5;
  nl_params.max_iter=100;
  nl_params.normTol=1.0e-3;
  
  CubicBezierCurve spline({0.,1.}, 50, nl_params);
  
  // Control points along semi-circle
  const int nPoints = 4;
  std::vector<double> Pts{};
  for(int p=0; p<nPoints; ++p)
    {
      const double theta = M_PI*static_cast<double>(p)/static_cast<double>(nPoints-1);
      Pts.push_back( M_PI*cos(theta) );
      Pts.push_back( M_PI*sin(theta) );
    }
  spline.SetControlPoints(Pts);

  // Cut monitor
  const double ttol = 1.e-2;
  CutProfileMonitor monitor(spline);

  std::cout<<"\n\nInterval state at start "<<std::flush;
  PrintIntervals(monitor);

  std::cout<<"\n\nInterval state after marking [0.25,0.5] as cut "<<std::flush;
  monitor.InsertCut(0.25,0.5,ttol);
  PrintIntervals(monitor);

  std::cout<<"\n\nInterval state after marking [0.4,0.5002] as cut "<<std::flush;
  monitor.InsertCut(0.4,0.5002,ttol);
  PrintIntervals(monitor);

  std::cout<<"\n\nInterval state after marking [0.6,0.6005] as cut "<<std::flush;
  monitor.InsertCut(0.6,0.6005,ttol);
  PrintIntervals(monitor);

  std::cout<<"\n\nNumber of uncut intervals: "<<monitor.GetNumUncutIntervals()<<std::flush;
  std::cout<<"\n\nLength of uncut intervals: "<<monitor.GetUncutProfileLength()<<std::flush;
  
}


void PrintIntervals(const CutProfileMonitor& monitor)
{
  auto cut_intervals = monitor.GetCutIntervals();
  auto uncut_intervals = monitor.GetUncutIntervals();

  std::cout<<"\nCut intervals: ";
  for(auto& it:cut_intervals)
    std::cout<<"["<<it.first<<","<<it.second<<"]  "<<std::flush;

  std::cout<<"\nUncut intervals: ";
  for(auto& it:uncut_intervals)
    std::cout<<"("<<it.first<<","<<it.second<<") "<<std::flush;

  return;
}
