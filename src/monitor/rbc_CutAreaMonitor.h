// Sriramajayam

#ifndef RBC_CUT_AREA_MONITOR_H
#define RBC_CUT_AREA_MONITOR_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>

namespace rbc
{
  using boost_point2D = boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian>;
  using boost_polygon2D = boost::geometry::model::polygon<boost_point2D>;
  
  class CutAreaMonitor
  {
  public:
    //! Constructor
    CutAreaMonitor(const std::vector<double>& corners);

    //! Destructor
    inline virtual ~CutAreaMonitor() {}

    //! Disable copy and assignment
    CutAreaMonitor(const CutAreaMonitor&) = delete;
    CutAreaMonitor operator=(const CutAreaMonitor&) = delete;

    //! Main functionality
    void Subtract(const std::vector<double>& corners);

    //! Access the uncut polygon
    inline const boost_polygon2D& GetUncutPolygon() const
    { return uncut_polygon; }

    //! Access the area of the uncut polygon
    inline double GetUncutPolygonArea() const
    { return boost::geometry::area(uncut_polygon); }

    //! Access corners
    std::vector<double> GetCorners() const;

    //! plot uncut polygon
    void PlotTec(const std::string filename) const;
    
  private:
    boost_polygon2D uncut_polygon;
  };
}


#endif
