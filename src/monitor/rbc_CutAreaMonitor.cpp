// Sriramajayam

#include <rbc_CutAreaMonitor.h>
#include <PlottingUtils.h>
#include <cassert>

namespace rbc
{
  // Constructor
  CutAreaMonitor::CutAreaMonitor(const std::vector<double>& corners)
  {
    assert(corners.size()%2==0);
    const int nCorners = static_cast<int>(corners.size()/2);
    assert(nCorners>=4);
    for(int n=0; n<nCorners; ++n)
      boost::geometry::append(uncut_polygon.outer(), boost_point2D(corners[2*n],corners[2*n+1]));
    boost::geometry::correct(uncut_polygon);
    assert(boost::geometry::area(uncut_polygon)>1.e-4);
  }

  // Main functionality
  void CutAreaMonitor::Subtract(const std::vector<double>& corners)
  {
    // Given polygon
    assert(corners.size()%2==0);
    const int nCorners = static_cast<int>(corners.size()/2);
    boost_polygon2D cut_poly;
    for(int n=0; n<nCorners; ++n)
      boost::geometry::append(cut_poly.outer(), boost_point2D(corners[2*n],corners[2*n+1]));
    boost::geometry::correct(cut_poly);
    assert(boost::geometry::area(cut_poly)>1.e-4);

    // Difference
    std::vector<boost_polygon2D> diff_poly{};
    boost::geometry::difference(uncut_polygon, cut_poly, diff_poly);
    assert(static_cast<int>(diff_poly.size())==1);
    assert(diff_poly[0].inners().empty());

    // Update the uncut polygon
    uncut_polygon = diff_poly[0];

    return;
  }

  // Access corners
  std::vector<double> CutAreaMonitor::GetCorners() const
  {
    std::vector<double> corners{};
    const auto& poly_corners = uncut_polygon.outer();
    for(auto& x:poly_corners)
      {
	corners.push_back( x.get<0>() );
	corners.push_back( x.get<1>() );
      }
    return std::move(corners);
  }

  // plot uncut polygon
  void CutAreaMonitor::PlotTec(const std::string filename) const
  {
    CoordConn MD;
    MD.coordinates.clear();
    MD.connectivity.clear();
    MD.nodes = 0;
    MD.elements = 0;
    MD.spatial_dimension = 2;
    MD.nodes_element = 3;

    // coordinates
    const auto& corners = uncut_polygon.outer();
    for(auto& x:corners)
      {
	MD.coordinates.push_back(x.get<0>());
	MD.coordinates.push_back(x.get<1>());
	++MD.nodes;
      }
    MD.elements = MD.nodes;

    // connectivity
    for(int e=0; e<MD.elements-1; ++e)
      {
	MD.connectivity.push_back(e+1);
	MD.connectivity.push_back(e+1);
	MD.connectivity.push_back(e+2);
      }
    MD.connectivity.push_back(MD.nodes);
    MD.connectivity.push_back(MD.nodes);
    MD.connectivity.push_back(1);

    // print
    PlotTecCoordConn(filename.c_str(), MD);
    return;
  }
  
}
