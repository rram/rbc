// Sriramajayam

#include <rbc_CutProfileMonitor.h>
#include <PlottingUtils.h>

namespace rbc
{
  // Constructor
  CutProfileMonitor::CutProfileMonitor(const ParametricCurve& crv)
    :curve(crv)
  {
    double t0, t1;
    curve.GetTargetParameterBounds(t0, t1);
    tInterval = Interval::closed(t0,t1);
    cut_intervals.clear();
    uncut_intervals.clear();
    UpdateUncutIntervals();
  }

  // Constructor
  CutProfileMonitor::CutProfileMonitor(const ParametricCurve& crv,
				       const std::vector<std::pair<double,double>>& cut_interval_state)
    :curve(crv)
  {
    double tmin, tmax;
    curve.GetTargetParameterBounds(tmin, tmax);
    tInterval = Interval::closed(tmin, tmax);
    cut_intervals.clear();
    uncut_intervals.clear();

    // Insert cut intervals
    for(auto& it:cut_interval_state)
      {
	assert(it.first<=it.second);
	cut_intervals += Interval::closed(it.first, it.second);
      }

    // Update the set of uncut intervals
    UpdateUncutIntervals();
  }
  
  // Erase cut intervals smaller than a given size
  void CutProfileMonitor::CleanseCutIntervals(const double tsizeTOL)
  {
    // Erasure
    bool flag = false;
    while(flag==false)
      {
	flag = true;
	for(auto& it:cut_intervals) 
	  if(it.upper()-it.lower()<tsizeTOL)  // erase this interval. 
	    {
	      cut_intervals.erase(it); // Invalidates iterator. Redo the loop.
	      flag = false;
	      break;
	    }
      }

    return;
  }

  
  // Update the set of uncut intervals based on cut ones
  void CutProfileMonitor::UpdateUncutIntervals()
  {
    uncut_intervals.clear();
    uncut_intervals += tInterval;
    for(auto& it:cut_intervals)
      uncut_intervals -= it;
    return;
  }

  
  // Main functionality: update the set of cut and uncut intervals
  void CutProfileMonitor::Update(const Elastica& elastica, const double sdTOL, const double tsizeTOL)
  {
    // Loop over nodes of the elastica and determine distances to the elastica
    const int nNodes = elastica.GetNumNodes();
    std::vector<bool> sd_sign(nNodes, false); // false -> uncut, true -> cut
    std::vector<double> tvals(nNodes, 0.);
    const auto& xy = elastica.GetCartesianCoordinates();
    double sdval;
    for(int n=0; n<nNodes; ++n)
      {
	tvals[n] = curve.GetSignedDistance(&xy[2*n], sdval, nullptr);
	if(std::abs(sdval)<sdTOL)
	  sd_sign[n] = true;
      }

    // Update cut intervals using cut elements
    for(int e=0; e<nNodes-1; ++e)
      {
	if(sd_sign[e]==true && sd_sign[e+1]==true) // this element is deemed to be "cut"
	  cut_intervals += Interval::closed(tvals[e], tvals[e+1]);
      }

    // Clean up cut intervals
    CleanseCutIntervals(tsizeTOL);
    
    // Update the set of uncut intervals
    UpdateUncutIntervals();

    // done
    return;
  }


  // Main functionality: update the set of cut and uncut intervals
  void CutProfileMonitor::InsertCut(const double t0, const double t1, const double tsizeTOL)
  {
    assert(t0<=t1);
    cut_intervals += Interval::closed(t0, t1);

    // Clean up cut intervals
    CleanseCutIntervals(tsizeTOL);
    
    // Update the set of uncut intervals
    UpdateUncutIntervals();

    return;
  }

  
  // Access cut intervals
  std::vector<std::pair<double,double>> CutProfileMonitor::GetCutIntervals() const
  {
    std::vector<std::pair<double,double>> tcut{};
    for(auto& it:cut_intervals)
      tcut.push_back( {it.lower(), it.upper()} );
    return std::move(tcut);
  }

  // Access uncut intervals
  std::vector<std::pair<double,double>> CutProfileMonitor::GetUncutIntervals() const
  {
    std::vector<std::pair<double,double>> tuncut{};
    for(auto& it:uncut_intervals)
      tuncut.push_back( {it.lower(), it.upper()} );
    return std::move(tuncut);
  }

  // Plot the cut intervals in tec format
  void CutProfileMonitor::PlotTecCutProfile(const std::string filename, const int nsamples) const
  {
    CoordConn MD;
    MD.coordinates.clear();
    MD.connectivity.clear();
    MD.nodes = 0;
    MD.elements = 0;
    MD.spatial_dimension = 2;
    MD.nodes_element = 3;
    double pt[2];
    for(auto& it:cut_intervals)
      {
	// coordinates
	const double t0 = it.lower();
	const double t1 = it.upper();
	for(int i=0; i<=nsamples; ++i)
	  {
	    double lambda = static_cast<double>(i)/static_cast<double>(nsamples+1);
	    double t = (1.-lambda)*t0 + lambda*t1;
	    curve.Evaluate(t, pt, nullptr, nullptr);
	    MD.coordinates.push_back(pt[0]);
	    MD.coordinates.push_back(pt[1]);
	  }

	// connectivity
	for(int a=0; a<nsamples; ++a)
	  {
	    MD.connectivity.push_back( MD.nodes+a+1 );
	    MD.connectivity.push_back( MD.nodes+a+1 );
	    MD.connectivity.push_back( MD.nodes+a+2 );
	  }

	// update counts
	MD.nodes += (nsamples+1);
	MD.elements += nsamples;
      }

    // print
    PlotTecCoordConn(filename.c_str(), MD);
    return;
  }
  
  
  // Plot the uncut intervals in tec format
  void CutProfileMonitor::PlotTecUncutProfile(const std::string filename, const int nsamples) const
  {
    CoordConn MD;
    MD.coordinates.clear();
    MD.connectivity.clear();
    MD.nodes = 0;
    MD.elements = 0;
    MD.spatial_dimension = 2;
    MD.nodes_element = 3;
    double pt[2];
    for(auto& it:uncut_intervals)
      {
	// coordinates
	const double t0 = it.lower();
	const double t1 = it.upper();
	for(int i=0; i<=nsamples; ++i)
	  {
	    double lambda = static_cast<double>(i)/static_cast<double>(nsamples+1);
	    double t = (1.-lambda)*t0 + lambda*t1;
	    curve.Evaluate(t, pt, nullptr, nullptr);
	    MD.coordinates.push_back(pt[0]);
	    MD.coordinates.push_back(pt[1]);
	  }

	// connectivity
	for(int a=0; a<nsamples; ++a)
	  {
	    MD.connectivity.push_back( MD.nodes+a+1 );
	    MD.connectivity.push_back( MD.nodes+a+1 );
	    MD.connectivity.push_back( MD.nodes+a+2 );
	  }

	// update counts
	MD.nodes += (nsamples+1);
	MD.elements += nsamples;
      }

    // print
    PlotTecCoordConn(filename.c_str(), MD);
    return;
  }
  
}
