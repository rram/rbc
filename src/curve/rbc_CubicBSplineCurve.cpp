// Sriramajayam

#include <rbc_CubicBSplineCurve.h>
#include <iostream>

namespace rbc
{
  // Constructor
  CubicBSplineCurve::CubicBSplineCurve(const std::vector<double>& kv,
				       const std::pair<double,double> ttarget,
				       const int nsamples_per_interval,
				       const NLSolverParams& op)
    :ParametricCurve(nsamples_per_interval, op, static_cast<int>(kv.size())),
     t_target{ttarget.first, ttarget.second},
     is_control_points_set(false),
     nControlPoints(static_cast<int>(kv.size())-4), // nknots-order
     control_points(2*nControlPoints)
  {
    // Allocate workspace
    const int degree = 3;
    const int nknots = static_cast<int>(kv.size());
    const int order = degree+1; // Degree + 1
    const int nbreak = nControlPoints-order+2;
    ws = gsl_bspline_alloc(order, nbreak);
    nz_shp = gsl_matrix_calloc(order, 3); // 3 cols, evaluate upto 2nd deriv.
    
    // Set the knot vector
    for(int k=0; k<nknots; ++k)
      gsl_vector_set(ws->knots, k, kv[k]);

    // Bounds for first interval
    tleft[0] = gsl_vector_get(ws->knots, 0+degree);   
    tleft[1] = gsl_vector_get(ws->knots, 0+degree+1);

    // Bounds for last interval
    const int nIntervals = ws->l;
    tright[0] = gsl_vector_get(ws->knots, (nIntervals-1)+degree);   
    tright[1] = gsl_vector_get(ws->knots, (nIntervals-1)+degree+1);
  }
					      
  // Destructor
  CubicBSplineCurve::~CubicBSplineCurve()
  { gsl_bspline_free(ws);
    gsl_matrix_free(nz_shp); }
  
  // Returns the bounds of the parameterization
  void CubicBSplineCurve::GetParameterBounds(double& t0, double& t1) const
  { t0 = tleft[0];
    t1 = tright[1];
    return; }

   // Returns the parameter bounds to be cut
  void CubicBSplineCurve::GetTargetParameterBounds(double& t0, double& t1) const
  {
    t0 = t_target[0];
    t1 = t_target[1];
    return;
  }
  
  
  // Set  control points
  void CubicBSplineCurve::SetControlPoints(const std::vector<double>& cp)
  {
    assert(static_cast<int>(cp.size())==2*nControlPoints);
    for(int p=0; p<2*nControlPoints; ++p)
      control_points[p] = cp[p];
    is_control_points_set = true;

    // Populate the rtree
    rtree.clear();
    const int nIntervals = ws->l;
    double lambda, t, t0, t1, X[2];
    for(int ival=0; ival<nIntervals; ++ival)
      {
	t0 = gsl_vector_get(ws->knots, ival+3);   // This interval
	t1 = gsl_vector_get(ws->knots, ival+4);
	
	// Insert sample points. Skip the right end point of the interval
	for(int k=0; k<nSamples_Per_Interval; ++k)
	  {
	    lambda = 1.-static_cast<double>(k)/static_cast<double>(nSamples_Per_Interval);
	    t = lambda*t0 + (1.-lambda)*t1;
	    Evaluate(t, X);
	    rtree.insert(pointParam(boost_point2D(X[0],X[1]),t));
	  }
      }

    // done
    return;
  }
  
  
  // Evaluate coordinates
  void CubicBSplineCurve::Evaluate(const double& t, double* X, double* dX, double* d2X) const
  {
    assert(is_control_points_set==true);

    // Evaluate nonzero basis f
    size_t istart, iend;
    gsl_bspline_deriv_eval_nonzero(t, 2, nz_shp, &istart, &iend, ws);
    
    if(X!=nullptr)
      { X[0] = X[1] = 0.;
	double Ni;
	for(size_t i=istart; i<=iend; ++i)
	  {
	    Ni = gsl_matrix_get(nz_shp, i-istart, 0);
	    X[0] += control_points[2*i+0]*Ni;
	    X[1] += control_points[2*i+1]*Ni;
	  }
      }

    if(dX!=nullptr)
      {
	dX[0] = dX[1] = 0.;
	double dNi;
	for(size_t i=istart; i<=iend; ++i)
	  {
	    dNi = gsl_matrix_get(nz_shp, i-istart, 1);
	    dX[0] += control_points[2*i+0]*dNi;
	    dX[1] += control_points[2*i+1]*dNi;
	  }
      }

    if(d2X!=nullptr)
      {
	d2X[0] = d2X[1] = 0.;
	double d2Ni;
	for(size_t i=istart; i<=iend; ++i)
	  {
	    d2Ni = gsl_matrix_get(nz_shp, i-istart, 2);
	    d2X[0] += control_points[2*i+0]*d2Ni;
	    d2X[1] += control_points[2*i+1]*d2Ni;
	  }
      }

    // done
    return;
  }

}
