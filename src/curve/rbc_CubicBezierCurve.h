// Sriramajayam

#ifndef RBC_CUBIC_BEZIER_CURVE_H
#define RBC_CUBIC_BEZIER_CURVE_H

#include <rbc_ParametricCurve.h>

namespace rbc
{
  class CubicBezierCurve: public ParametricCurve
  {
  public:
    //! Constructor
    //! \param[in] nsamples Number of sampling points to use
    CubicBezierCurve(const std::pair<double,double> ttarget,
		     const int nsamples_per_interval,
		     const NLSolverParams& op);

    //! Disable copy and assignment
    CubicBezierCurve(const CubicBezierCurve&) = delete;
    CubicBezierCurve operator=(const CubicBezierCurve&) = delete;

    //! Destructor
    inline virtual ~CubicBezierCurve() {}

    //! Returns the parameter bounds to be cut
    void GetTargetParameterBounds(double& t0, double& t1) const override;

    //! Set control points
    void SetControlPoints(const std::vector<double>& pts);
   
    //! Evaluate coordinates
    virtual void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const override;

  protected:

    //! Returns the bounds of the parameterization
    void GetParameterBounds(double& t0, double& t1) const override;
    
  private:
    const double t_target[2];
    bool is_control_points_set;
    double P0[2], P1[2], P2[2], P3[2];
  };
}

#endif
