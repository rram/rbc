// Sriramajayam

#include <rbc_CubicSplineCurve.h>
#include <rbc_Structs.h>
#include <iostream>
#include <cmath>
#include <fstream>
#include <random>

using namespace rbc;

int main()
{
  // Knot vector 
  std::vector<double> knotvec{};
  const int nPoints = 10;
  for(int p=0; p<nPoints; ++p)
    knotvec.push_back( static_cast<double>(p)/static_cast<double>(nPoints-1) );

  // Create spline
  NLSolverParams opt_params;
  opt_params.digits = 5;
  opt_params.max_iter = 100;
  opt_params.normTol = 1e-3;

  std::pair<double,double> t_target{0.,1.};
  rbc::CubicSplineCurve spline(knotvec, t_target, nPoints, opt_params);
  
  // Control points along semi-circle
  std::vector<double> Pts{};
  for(int p=0; p<nPoints; ++p)
    {
      const double theta = M_PI*static_cast<double>(p)/static_cast<double>(nPoints-1);
      Pts.push_back( M_PI*cos(theta) );
      Pts.push_back( M_PI*sin(theta) );
    }
  spline.SetControlPoints(Pts);

  // Plot the spline
  std::fstream ss;
  ss.open((char*)"spline.dat", std::ios::out);
  ss << spline;
  ss.close();
  
  // Consistency tests
  std::random_device rd;  
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> xdis(-5.,5.);
  std::uniform_real_distribution<> ydis(1.,5.);
  const double pertEPS = 1.e-6;
  const double tolEPS = 1.e-4;
  for(int n=0; n<100; ++n)
    {
      // random point
      const double X[] = {xdis(gen), ydis(gen)};
      spline.ConsistencyTest(X, pertEPS, tolEPS);
    }
      
  // done
}
