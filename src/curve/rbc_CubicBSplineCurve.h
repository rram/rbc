// Sriramajayam

#ifndef RBC_CUBIC_BSPLINE_H
#define RBC_CUBIC_BSPLINE_H

#include <rbc_ParametricCurve.h>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_errno.h>

namespace rbc
{
  class CubicBSplineCurve: public ParametricCurve
  {
  public:
    //! Constructor
    CubicBSplineCurve(const std::vector<double>& kv,
		      const std::pair<double,double> ttarget,
		      const int nsamples_per_interval,
		      const NLSolverParams& op);

    //! Disable copy and assignment
    CubicBSplineCurve(const CubicBSplineCurve&) = delete;
    CubicBSplineCurve operator=(const CubicBSplineCurve&) = delete;

    //! Destructor
    virtual ~CubicBSplineCurve();
    
    //! Returns the parameter bounds to be cut
    void GetTargetParameterBounds(double& t0, double& t1) const override;
    
    //! Set  control points
    void SetControlPoints(const std::vector<double>& cp);

    //! Evaluate coordinates
    void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const override;

  protected:
    //! Returns the bounds of the parameterization
    void GetParameterBounds(double& t0, double& t1) const override;
    
  private:
    const double t_target[2];  // parametric interval of the curve to be cut
    gsl_bspline_workspace *ws; // workspace
    gsl_matrix* nz_shp;       // evaluate 0,1,2nd derivatives of non-zero basis functions
    bool is_control_points_set;
    const int nControlPoints;
    std::vector<double> control_points;
    double tleft[2], tright[2];
    double left_coefs[2][4], right_coefs[2][4]; // Coefficients of (x(t),y(t)) for left/right intervals
  };
}

#endif
