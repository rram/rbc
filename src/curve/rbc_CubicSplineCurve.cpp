// Sriramajayam

#include <rbc_CubicSplineCurve.h>

namespace rbc
{
  // Constructor
  CubicSplineCurve::CubicSplineCurve(const std::vector<double>& tvec,
				     const std::pair<double,double> ttarget,
				     const int nsamples_per_interval,
				     const NLSolverParams& op)
    :ParametricCurve(nsamples_per_interval, op, static_cast<int>(tvec.size())),
     knotVec(tvec),
     t_target{ttarget.first, ttarget.second},
     xspline(nullptr),
     yspline(nullptr),
     acc(nullptr),
     is_control_points_set(false),
     nControlPoints(static_cast<int>(tvec.size())),
     xvec(nControlPoints), yvec(nControlPoints),
     tleft{tvec[0],tvec[1]},
     tright{tvec[tvec.size()-2], tvec[tvec.size()-1]}
  {
    assert(nControlPoints>=3);
    xspline = gsl_spline_alloc(gsl_interp_cspline, nControlPoints);
    yspline = gsl_spline_alloc(gsl_interp_cspline, nControlPoints);
    acc = gsl_interp_accel_alloc();
  }

  
  // Destructor
  CubicSplineCurve::~CubicSplineCurve()
  {
    gsl_interp_accel_free(acc);
    gsl_spline_free(xspline);
    gsl_spline_free(yspline);
  }

  // Returns the bounds of the parameterization
  void CubicSplineCurve::GetParameterBounds(double& t0, double& t1) const
  { t0 = tleft[0];
    t1 = tright[1];
    return; }


  // Returns the parameter bounds to be cut
  void CubicSplineCurve::GetTargetParameterBounds(double& t0, double& t1) const
  {
    t0 = t_target[0];
    t1 = t_target[1];
    return;
  }

  
  // Set control points
  void CubicSplineCurve::SetControlPoints(const std::vector<double>& cp)
  {
    assert(static_cast<int>(cp.size())==2*nControlPoints);
    for(int p=0; p<nControlPoints; ++p)
      {
	xvec[p] = cp[2*p];
	yvec[p] = cp[2*p+1];
      }
    
    // (re)-initialize splines
    gsl_spline_init(xspline, knotVec.data(), xvec.data(), nControlPoints);
    gsl_spline_init(yspline, knotVec.data(), yvec.data(), nControlPoints);
    is_control_points_set = true;
    
    // Populate the r-tree
    rtree.clear();
    const int nIntervals = static_cast<int>(knotVec.size())-1;
    double lambda, t, X[2];
    for(int ival=0; ival<nIntervals; ++ival)
      {
	const double& t0 = knotVec[ival];
	const double& t1 = knotVec[ival+1];

	for(int k=0; k<nSamples_Per_Interval; ++k)
	  {
	    lambda = 1.-static_cast<double>(k)/static_cast<double>(nSamples_Per_Interval);
	    t = lambda*t0 + (1.-lambda)*t1;
	    Evaluate(t, X);
	    rtree.insert(pointParam(boost_point2D(X[0],X[1]),t));
	  }
      }
    
    // done
    return;
  }

  
  // Evaluate coordinates
  void CubicSplineCurve::Evaluate(const double& t, double* X, double* dX, double* d2X) const
  {
    assert(is_control_points_set==true);
    if(X!=nullptr)
      {
	X[0] = gsl_spline_eval(xspline, t, acc);
	X[1] = gsl_spline_eval(yspline, t, acc);
      }
    if(dX!=nullptr)
      {
	dX[0] = gsl_spline_eval_deriv(xspline, t, acc);
	dX[1] = gsl_spline_eval_deriv(yspline, t, acc);
      }
    if(d2X!=nullptr)
      {
	d2X[0] = gsl_spline_eval_deriv2(xspline, t, acc);
	d2X[1] = gsl_spline_eval_deriv2(yspline, t, acc);
      }

    // done
    return;
  }

}
