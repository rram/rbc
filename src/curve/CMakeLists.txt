# Sriramajayam

add_library(rbc_curve STATIC
  rbc_ParametricCurve.cpp
  rbc_ParametricCurve_roots.cpp
  rbc_CubicBSplineCurve.cpp
  rbc_CubicSplineCurve.cpp
  rbc_CubicBezierCurve.cpp)

# headers
target_include_directories(rbc_curve PUBLIC
  ${Boost_INCLUDE_DIR}
  ${GSL_INCLUDE_DIRS}
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../utils>)
  

# link
target_link_libraries(rbc_curve PUBLIC
  Boost::boost
  NLopt::nlopt
  GSL::gsl  GSL::gslcblas)


# Add required flags
target_compile_features(rbc_curve PUBLIC ${rbc_COMPILE_FEATURES})

install(FILES
  rbc_ParametricCurve.h
  rbc_CubicBSplineCurve.h
  rbc_CubicSplineCurve.h
  rbc_CubicBezierCurve.h
  DESTINATION ${PROJECT_NAME}/include)
