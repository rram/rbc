// Sriramajayam

#include <rbc_ParametricCurve.h>
#include <rbc_Structs.h>
#include <boost/math/quadrature/gauss_kronrod.hpp>
#include <iostream>

namespace rbc
{ 
  // Constructor
  ParametricCurve::ParametricCurve(const int nsamples_per_interval, const NLSolverParams& op,const int nsamples)
    :nSamples_Per_Interval(nsamples_per_interval),
     nlparams(op),
     nSamples(nsamples)
  {
    rtree.clear();
  }

  // Destructor
  ParametricCurve::~ParametricCurve()
  {}

  // Compute the signed distance function
  double ParametricCurve::GetSignedDistance(const double* X, double& sd, double* dsd) const
  {
    double t0,t1;
    this->GetParameterBounds(t0,t1);
    const double dt = (t1-t0)/static_cast<double>(nSamples);
    
    // Identify the closest sample point
    assert(rtree.empty()==false);
    boost_point2D pt(X[0], X[1]);
    std::vector<pointParam> result{};
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);

    // solve
    double tbounds[2] = {t0,t1};
    const double tcpt = Spline_FindRoot_Boost(X, result[0].second, dt,tbounds);
    
    // Evaluate the curve and its derivatives here
    double Y[2], dY[2];
    Evaluate(tcpt, Y, dY, nullptr);
    double norm = std::sqrt(dY[0]*dY[0]+dY[1]*dY[1]);
    assert(norm>1.e-4);                                // HACK HACK -> hard-coded tolerance
    double nvec[] = {-dY[1]/norm, dY[0]/norm};   
    
    // signed distance
    sd = 0.;
    for(int k=0; k<2; ++k)
      sd += (X[k]-Y[k])*nvec[k];

    // Gradient
    if(dsd!=nullptr)
      {
	dsd[0] = nvec[0];
	dsd[1] = nvec[1];
      }

    // done
    return tcpt;
  }

  // Compute the closest point projection
  double ParametricCurve::GetClosestPoint(const double* X, double* cpt) const
  {
    assert(cpt!=nullptr);

    double t0,t1;
    this->GetParameterBounds(t0,t1);
    const double dt = (t1-t0)/static_cast<double>(nSamples);
    
    // Identify the closest sample point
    assert(rtree.empty()==false);
    boost_point2D pt(X[0], X[1]);
    std::vector<pointParam> result{};
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);

    // solve
    double tbounds[2] = {t0,t1};
    const double tcpt = Spline_FindRoot_Boost(X, result[0].second, dt,tbounds);
    
    // Evaluate the curve and its derivatives here
    Evaluate(tcpt, cpt, nullptr, nullptr);

    return tcpt;
  }


  
  // compute arc-length usid boost qudrature
  struct arc_length_functor
  {
    inline arc_length_functor(const ParametricCurve* crv)
      :curve(crv){}
      
    inline double operator()(const double& tval) const
      {
	double rvec[2], drvec[2]; 
	curve->Evaluate(tval, rvec, drvec);
	return std::sqrt(drvec[0]*drvec[0] + drvec[1]*drvec[1]);
      }
    const ParametricCurve* curve;
  };
  
  double ParametricCurve::ComputeArcLength(const double t0, const double t1, const double stol) const
  {
    double error = 0.;
    double arc_len = boost::math::quadrature::gauss_kronrod<double, 15>::integrate(arc_length_functor(this), t0, t1, 10, stol, &error);
    assert(std::abs(error)/arc_len<0.01);
    return arc_len;
  }
  


  // Consistency test
  void ParametricCurve::ConsistencyTest(const double* X, const double pertEPS, const double tolEPS)
  {
    // Signed distance here
    double sd, dsd[2];
    const double topt = GetSignedDistance(X, sd, dsd);

    // Closest point projection 
    double cpt[2];
    const double topt2 = GetClosestPoint(X, cpt);

    // Identical parametric coordinates
    assert(std::abs(topt-topt2)<tolEPS);

    // Check that dsd has unit magnitude
    assert(std::abs(dsd[0]*dsd[0]+dsd[1]*dsd[1]-1.)<tolEPS);

    // Consistency of the gradient of sd
    for(int i=0; i<2; ++i)
      {
	double Y[] = {X[0], X[1]};
	Y[i] += pertEPS;

	double sdplus;
	GetSignedDistance(Y, sdplus, nullptr);

	double sdminus;
	Y[i] -= 2.*pertEPS;
	GetSignedDistance(Y, sdminus, nullptr);
	
	double dsdnum = (sdplus-sdminus)/(2.*pertEPS);
	assert(std::abs(dsd[i]-dsdnum)<tolEPS);
      }
    
    // Closest point should have zero distance
    {
      double zero;
      double topt3 = GetSignedDistance(cpt, zero);
      assert(std::abs(zero)<tolEPS);
      assert(std::abs(topt-topt3)<tolEPS);
    }

    // Relation between closest point projection and signed distances
    assert(std::abs(cpt[0]-(X[0]-sd*dsd[0])) + std::abs(cpt[1]-(X[1]-sd*dsd[1]))<tolEPS);

    // Check that X(topt) = cpt, X'(topt) // to dsd
    {
      double Z[2], dZ[2];
      Evaluate(topt, Z, dZ);
      assert(std::abs(Z[0]-cpt[0])+std::abs(Z[1]-cpt[1])<tolEPS);
						   
      // dsd should be perpendicular to the tangent at cpt
      assert(std::abs(dsd[0]*dZ[0]+dsd[1]*dZ[1])<tolEPS);
    }

    // Check consistency of parameterization at topt
    {
      double Z[2], dZ[2], d2Z[2];
      Evaluate(topt, Z, dZ, d2Z);
          
      double tplus = topt+pertEPS;
      double Zplus[2], dZplus[2];
      Evaluate(tplus, Zplus, dZplus);

      double tminus = topt-pertEPS;
      double Zminus[2], dZminus[2];
      Evaluate(tminus, Zminus, dZminus);

      for(int i=0; i<2; ++i)
	{
	  double dZnum = (Zplus[i]-Zminus[i])/(2.*pertEPS);
	  assert(std::abs(dZnum-dZ[i])<tolEPS);

	  double d2Znum = (dZplus[i]-dZminus[i])/(2.*pertEPS);
	  assert(std::abs(d2Znum-d2Z[i])<tolEPS);
	}
    }

    // Check that ds/dt = |r'| at this point
    {
      double tL, tR;
      GetParameterBounds(tL, tR);
      const double tplus  = topt+pertEPS;
      const double tminus = topt-pertEPS;
      const double stol   = pertEPS/1000.;
      double splus = ComputeArcLength(tL, tplus);
      double sminus = ComputeArcLength(tL, tminus);
      double dsdt = (splus-sminus)/(2.*pertEPS);
      double rvec[2], drvec[2];
      Evaluate(topt, rvec, drvec);
      const double dsnorm = std::sqrt(drvec[0]*drvec[0]+drvec[1]*drvec[1]);
      std::cout << dsnorm << " should be " << dsdt << std::endl;
      assert(dsdt>0. && std::abs(dsnorm-dsdt)<tolEPS);
    }

    // done
    return;
  }


  // overload extraction
  std::ostream& operator<<(std::ostream& output, const ParametricCurve& curve)
  {
    double t0, t1, t;
    curve.GetParameterBounds(t0, t1);
    const double dt = (t1-t0)/1000;
    double X[2];
    for(int n=0; n<1001; ++n)
      {
	t = t0 + static_cast<double>(n)*dt;
	curve.Evaluate(t, X);
	output << t << "\t" << X[0] << "\t" << X[1] <<"\n";
      }
    return output;
  }
}
