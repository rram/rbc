// Sriramajayam

#ifndef RBC_CUBIC_SPLINE_CURVE_H
#define RBC_CUBIC_SPLINE_CURVE_H

#include <rbc_ParametricCurve.h>
#include <gsl/gsl_spline.h>

namespace rbc
{
  class CubicSplineCurve: public ParametricCurve
  {
  public:
    //! Constructor
    CubicSplineCurve(const std::vector<double>& tvec,
		     const std::pair<double,double> ttarget,
		     const int nsamples_per_interval,
		     const NLSolverParams& op);

    // Disable copy and assignment
    CubicSplineCurve(const CubicSplineCurve&) = delete;
    CubicSplineCurve operator=(const CubicSplineCurve&) = delete;

    //! Destructor
    virtual ~CubicSplineCurve();

    //! Set control points
    void SetControlPoints(const std::vector<double>& cp);

    //! Evaluate coordinates
    void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const override;

    //! Returns the parameter bounds to be cut
    void GetTargetParameterBounds(double& t0, double& t1) const override;

  protected:
    //! Returns the bounds of the parameterization
    void GetParameterBounds(double& t0, double& t1) const override;
    
  private:
    const std::vector<double> knotVec;
    const double t_target[2]; //!< parametric interval to be cut
    gsl_spline *xspline, *yspline; //!< Spline function for the x/y coordinates
    gsl_interp_accel *acc; //!< Accelerator for x/y spline

    bool is_control_points_set;
    const int nControlPoints;
    std::vector<double> xvec, yvec;
    const double tleft[2], tright[2];
    double left_coefs[2][4], right_coefs[2][4];
  };
}

#endif
