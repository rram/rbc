// Sriramajayam

#include <rbc_ParametricCurve.h>
#include <rbc_Structs.h>
#include <boost/math/tools/roots.hpp>

namespace rbc
{
  // Root finding with boost
  struct Spline_RootFinding_BoostFunctor
  {
    inline Spline_RootFinding_BoostFunctor(const double* x, const ParametricCurve* geom)
      :X(x), spline(geom) {}

    inline std::pair<double, double> operator()(double const& tval)
    {
      const double t = tval-tshift;
      spline->Evaluate(t, rvec, drvec, d2rvec);

      // value: (r(t)-X).r'(t)
      val = (rvec[0]-X[0])*drvec[0] + (rvec[1]-X[1])*drvec[1];

      // derivative: r'(t).r'(t) + (r(t)-X).r''(t)
      dval = (drvec[0]*drvec[0]+drvec[1]*drvec[1]) + (rvec[0]-X[0])*d2rvec[0] +  (rvec[1]-X[1])*d2rvec[1];

      return std::make_pair(val, dval);
    }

    const double tshift = 1.;
    const double* X;
    const ParametricCurve* spline;
    mutable double rvec[2], drvec[2], d2rvec[2], val, dval;
  };

    
  // Finds the root 't' of the function (r(t)-X).r'(t) = 0
  double ParametricCurve::Spline_FindRoot_Boost(const double* X,
						const double t0,
						const double dt,
						const double* tbounds) const

  {
    // During root-finding calls, offset t->t+1 to avoid a root at t=0.
    // This enables boost N-R root-finder to terminate based on the number of signficicant digits
    // This shift t->t+1 is done in (i) root-finding calls, and in the boost-functor.
    const double tshift = 1.;

    // Try without careful bracketing
    try
      {
	boost::uintmax_t iters = static_cast<boost::uintmax_t>(nlparams.max_iter);
	const double tresult = boost::math::tools::newton_raphson_iterate(Spline_RootFinding_BoostFunctor(X, this), t0+tshift, tbounds[0]+tshift, tbounds[1]+tshift, nlparams.digits, iters)-tshift;
	    
	// expected one of three possibilities: converged, exceeded iterations or an exception.

	// converged?
	if(iters<nlparams.max_iter)
	  return tresult;
      }
    catch(std::exception& e) // exception is handled below
      {}

    // We are here because there was an exception, or the iterations did not converge
    // Proceed conservatively by bracketing the root
    std::cout<<std::endl <<" Handling an exception in N-R root-finding "<< std::endl;
	
    // f0 = f(t0)
    double rvec[2], drvec[2];
    Evaluate(t0, rvec, drvec, nullptr);
    const double f0 = (rvec[0]-X[0])*drvec[0] + (rvec[1]-X[1])*drvec[1];

    // d0 = |sd(t0)|^2
    const double d0 = (X[0]-rvec[0])*(X[0]-rvec[0]) + (X[1]-rvec[1])*(X[1]-rvec[1]);

    // bracket the root by advancing on either side of t0 in steps of dt
    double tplus = t0, tminus = t0;
    double t1, f1;
    int nplus = 0, nminus = 0;
    const int nmax_same_sign = 5; // limit search to 5 steps on either side of t0

    bool bracket_flag = false;
    while(bracket_flag==false)
      {
	// look ahead of t0
	tplus  += dt;
	if(tplus<=tbounds[1])
	  {
	    ++nplus;
	    Evaluate(tplus, rvec, drvec, nullptr);
	    f1 = (rvec[0]-X[0])*drvec[0] + (rvec[1]-X[1])*drvec[1];
	    if(f0*f1<0.)
	      {
		t1 = tplus;
		bracket_flag = true;
		break;
	      }
	  }

	// look behind t0
	tminus -= dt;
	if(tminus>=tbounds[0])
	  {
	    ++nminus;
	    Evaluate(tminus, rvec, drvec, nullptr);
	    f1 = (rvec[0]-X[0])*drvec[0] + (rvec[1]-X[1])*drvec[1];
	    if(f0*f1<0.)
	      {
		t1 = tminus;
		bracket_flag = true;
		break;
	      }
	  }

	// stopping criteria
	if(tminus<tbounds[0] && tplus>tbounds[1]) break;                 // Don't look outside the parametric interval
	else if(nminus>=nmax_same_sign || nplus>=nmax_same_sign) break;  // exceeded search iterations
      }
	
	
    // If a bracket could not be found, expect t0 to coincide with an endpoint of the bounding interval. This assumption can fail if 'dt' is too large
    if(bracket_flag==false)
      {
	assert(std::abs(t0-tbounds[0])<dt || std::abs(t0-tbounds[1])<dt);
	return t0;
      }
    else
      {
	if(bracket_flag==true)
	  {
	    double t_lo, t_hi;
	    if(t0<t1) { t_lo = t0; t_hi = t1;}
	    else      { t_lo = t1; t_hi = t0; }
	    boost::uintmax_t iters = static_cast<boost::uintmax_t>(nlparams.max_iter);
	    try
	      {
		const double tresult = boost::math::tools::newton_raphson_iterate(Spline_RootFinding_BoostFunctor(X, this), t0+tshift, t_lo+tshift, t_hi+tshift, nlparams.digits, iters)-tshift;
		    
		// Expect one of three possibilities: convergence, exceeded iterations or an exception.

		// In case of exceeding iteration count, choose the better among (t0, tresult)
		if(iters>=nlparams.max_iter)
		  std::cout<<"\nRoot finding for splines did not converge. Choosing the best available root. "<<std::endl;
		    
		// choose the better solution among t0 and tresult
		Evaluate(tresult, rvec, drvec, nullptr);
		double d1 = (X[0]-rvec[0])*(X[0]-rvec[0]) + (X[1]-rvec[1])*(X[1]-rvec[1]);
		    
		if(d0<d1)
		  return t0;
		else
		  return tresult;
	      }
	    catch(std::exception& ex)
	      {
		std::cout<< std::endl <<"Exception in Newton-Raphson root finding during closest point calculation: "<< ex.what();
	      }
	  }
      }

    // should not be here
    assert(false);
    return t0;
  }

}
