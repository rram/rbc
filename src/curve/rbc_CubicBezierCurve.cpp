// Sriramajayam

#include <rbc_CubicBezierCurve.h>

namespace rbc
{
  // Constructor
  CubicBezierCurve::CubicBezierCurve(const std::pair<double,double> ttarget, const int nsamples, const NLSolverParams& op)
    :ParametricCurve(nsamples, op, nsamples),
     t_target{ttarget.first, ttarget.second},
     is_control_points_set(false) {}

  // Returns the bounds of the parameterization
  void CubicBezierCurve::GetParameterBounds(double& t0, double& t1) const
  { t0 = 0.;
    t1 = 1.;
    return; }
  
  // Returns the parameter bounds to be cut
  void CubicBezierCurve::GetTargetParameterBounds(double& t0, double& t1) const
  {
    t0 = t_target[0];
    t1 = t_target[1];
    return;
  }


  // Set control points
  void CubicBezierCurve::SetControlPoints(const std::vector<double>& pts)
  {
    assert(static_cast<int>(pts.size())==8);
    for(int k=0; k<2; ++k)
      {
	P0[k] = pts[2*0+k];
	P1[k] = pts[2*1+k];
	P2[k] = pts[2*2+k];
	P3[k] = pts[2*3+k];
      }
    is_control_points_set = true;

    // populate the r-tree
    rtree.clear();
    double lambda, X[2];
    for(int p=0; p<nSamples_Per_Interval; ++p)
      {
	lambda = 1.-static_cast<double>(p)/static_cast<double>(nSamples_Per_Interval);
	Evaluate(lambda, X);
	rtree.insert( pointParam(boost_point2D(X[0],X[1]),lambda) );
      }

    // done
    return;
  }
    
  // Evaluate coordinates
  void CubicBezierCurve::Evaluate(const double& t, double* X, double* dX, double* d2X) const
  {
    assert(is_control_points_set==true);
    // Evaluate

    // X
    if(X!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  X[k] =
	    (1.-t)*(1.-t)*(1.-t)*P0[k] +
	    3.*(1.-t)*(1.-t)*t*  P1[k] +
	    3.*(1.-t)*t*t*       P2[k] +
	    t*t*t*               P3[k];
      }

    // dX
    if(dX!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  dX[k] =
	    3.*(1.-t)*(1.-t)*(P1[k]-P0[k]) +
	    6.*(1.-t)*t*     (P2[k]-P1[k]) +
	    3.*t*t*          (P3[k]-P2[k]);
      }

    // d2X
    if(d2X!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  d2X[k] =
	    6.*(1.-t)*(P2[k]-2.*P1[k]+P0[k]) +
	    6.*t*     (P3[k]-2.*P2[k]+P1[k]);
      }

    // done
    return;
  }

}
