// Sriramajayam

#ifndef RBC_PARAMETRIC_CURVE_H
#define RBC_PARAMETRIC_CURVE_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace rbc
{
  // Forward declare
  class NLSolverParams;
  
  //! Abstract class defining a plane parametric curve
  class ParametricCurve
  {
  public:
    //! Constructor
    ParametricCurve(const int nsamples_per_interval, const NLSolverParams& op, const int nsamples);

    //! Disable copy and assignment
    ParametricCurve(const ParametricCurve&) = delete;
    ParametricCurve& operator=(const ParametricCurve&) = delete;

    //! Destructor
    virtual ~ParametricCurve();

    //! Returns the parameter bounds to be cut
    virtual void GetTargetParameterBounds(double& t0, double& t1) const = 0;
    
    //! Evaluate coordinates
    virtual void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const = 0;

    //! Compute the signed distance function
    double GetSignedDistance(const double* X, double& sd, double* dsd=nullptr) const;

    //! Compute the closest point projection
    double GetClosestPoint(const double* X, double* cpt) const;

    //! compute arc-length usid boost qudrature
    double ComputeArcLength(const double t0, const double t1, const double stol=1.e-5) const;
    
    //! Consistency test
    void ConsistencyTest(const double* X, const double pertEPS, const double tolEPS);
    
    typedef boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian> boost_point2D;
    typedef std::pair<boost_point2D,double> pointParam;
    boost::geometry::index::rtree<pointParam, boost::geometry::index::quadratic<8>> rtree;
    
    //! overload extraction
    friend std::ostream& operator<<(std::ostream& output, const ParametricCurve& curve);
    
  protected:

    //! Returns the bounds of the parameterization
    virtual void GetParameterBounds(double& t0, double& t1) const = 0;
    
    //! Finds the root 't' of the function (r(t)-X).r'(t) = 0
    double Spline_FindRoot_Boost(const double* X, const double t0, const double dt, const double* tbounds) const;
      
    const int nSamples_Per_Interval, nSamples;
    const NLSolverParams& nlparams;
  };
  
}

#endif
