// Sriramajayam

#pragma once

#include <rbc_Functionals.h>
#include <rbc_ParametricCurve.h>
#include <rbc_WeightFunction.h>

namespace rbc
{
  class SectionFunctionals: public Functionals
  {
  public:
    //! Constructor
    inline SectionFunctionals(Elastica& str,
			      LoadParams& lp,
			      std::pair<int,double>& bcs,
			      OriginParams& op,
			      const std::array<bool,6>& opt_vars,
			      const SolveParams& sp,
			      const ParametricCurve& target_crv,
			      const ParametricCurve& constraint_crv,
			      const WeightFunction& obj_wt,
			      const WeightFunction& cst_wt)
      :Functionals(str, lp, bcs, op, opt_vars, sp),
      target_curve(target_crv),
      constraint_curve(constraint_crv),
      nConstraints(str.GetNumNodes()),
      obj_wtFunc(obj_wt),
      cst_wtFunc(cst_wt) {}

    //! Destructor
    inline virtual ~SectionFunctionals() {}

    //! Disable copy and assignment
    SectionFunctionals(const SectionFunctionals&) = delete;
    SectionFunctionals operator=(const SectionFunctionals&) = delete;

    //! Number of constraints
    inline int GetNumConstraints() const override
    { return nConstraints; }
    
    //! Evaluate the objective and its sensitivities
    virtual void EvaluateObjective(const std::vector<double>& x, double& Jval, double* dJvals) override;
    
    //! Evaluate constraints and their sensitivities
    void EvaluateConstraints(const std::vector<double>& x, double* gvals, double* dgvals) override;
    
  protected:

    //! Evaluate objective over a specified interval of nodes on the elastica
    void EvaluateObjective(const std::vector<double>& x, const int nstart, const int nstop, double& Jval, double* dJvals);
    
    const ParametricCurve& target_curve;
    const ParametricCurve& constraint_curve;
    const int nConstraints;
    const WeightFunction& obj_wtFunc; //! weight function for the objective
    const WeightFunction& cst_wtFunc; //! weight function for the constraint
  };
  
}
