// Sriramajayam

#pragma once

#include <rbc_Elastica.h>
#include <algorithm>

namespace rbc
{
  class Functionals
  {
  public:
    //! Constructor
    inline Functionals(Elastica& str,
		       LoadParams& lp,
		       std::pair<int,double>& bcs,
		       OriginParams& op,
		       const std::array<bool, 6>& opt_vars, // H, V, M, BC, x, y
		       const SolveParams& sp)
      :elastica(str),
      load_params(lp),
      dirichlet_bcs(bcs),
      origin_params(op),
      solve_params(sp),
      nParameters(std::count(opt_vars.begin(), opt_vars.end(), true))
      {
	assert(nParameters!=0);
	
	// Set indices of optimization variables
	std::fill(optVarIndex.begin(), optVarIndex.end(), -1); // switch off all variables
	int count = 0;
	for(int i=0; i<6; ++i)
	  if(opt_vars[i]==true)
	    optVarIndex[i] = count++;
	assert(count==nParameters);
      }

    //! Destructor
    inline virtual ~Functionals() {}

    // Disable copy and assignment
    Functionals(const Functionals&) = delete;
    Functionals& operator=(const Functionals&) = delete;

    //! Access the elastica
    inline Elastica& GetElastica() const
    { return elastica; }

    //! Access load parameters
    inline LoadParams& GetLoadParams() const
    { return load_params; }

    //! Access dirichlet bcs
    inline std::pair<int,double>& GetDirichletBCs() const
    { return dirichlet_bcs; }

    //! Access origin params
    inline const OriginParams& GetOriginParams() const
    { return origin_params; }
    
    //! Number of optimization parameters
    inline int GetNumParameters() const
    { return nParameters; }
    
    //! Number of constraints
    virtual int GetNumConstraints() const = 0;
    
    //! Evaluate the objective and its sensitivities
    virtual void EvaluateObjective(const std::vector<double>& x, double& Jval, double* Jvals) = 0;
    
    //! Evaluate constraints and their sensitivities
    virtual void EvaluateConstraints(const std::vector<double>& x, double* gvals, double* dgvals) = 0;

    //! Perform consistency test for the objective functional
    void ObjectiveConsistencyTest(const std::vector<double>& x, const double pertEPS, const double tolEPS);

    //! Perform consistency test for constraint functionals
    void ConstraintConsistencyTest(const std::vector<double>& x, const double pertEPS, const double tolEPS);
    
  protected:
    
    //! Interpret optimization variables: from x -> load_params, bc_params, origin_params
    void PushOptimizationVariableValues(const std::vector<double>& x);
    
    Elastica              &elastica;
    LoadParams            &load_params;
    std::pair<int,double> &dirichlet_bcs;
    OriginParams          &origin_params;
    const SolveParams     &solve_params;
    const int             nParameters;
    std::array<int,6>     optVarIndex;
  };
}
