// Sriramajayam

#include <rbc_ThetaFunctional.h>
#include <iostream>
#include <random>

// Integrand
void ThetaFunc(const double& s, const double& theta, double& integrand, double* dintegrand, void* usr)
{
  // Target theta
  double tgt = 0.2*s;
  integrand = 0.5*(theta-tgt)*(theta-tgt);
  (*dintegrand) = (theta-tgt);
  return;
}


int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create elastica
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const double EI = 1.;
  rbc::Elastica* str = new rbc::Elastica(coordinates, EI);

  // Load the elastica
  std::pair<int,double> bc_params(0,0.);
  rbc::LoadParams load_params({.HVal=std::sqrt(2.)*std::cos(M_PI/3.),
	.VVal=std::sqrt(2.)*std::sin(M_PI/3.),
	.MDof=nNodes-1, .MVal=0.});

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};
  
  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1.,	.dofScale = 1.,
	.nMaxIters = 25, .verbose = false});

  // Optimization variables (H,V, M, BC, x, y)
  const std::array<bool,6> optimization_variables{true, true, true, true, true, true};
  
  // Update state/sensitivities
  str->ComputeStateAndSensitivities(load_params, bc_params, origin_params, solve_params);
  
  // Functionals
  rbc::ThetaFunctional funcs(*str, load_params, bc_params, origin_params, optimization_variables, solve_params, ThetaFunc, nullptr);

  // random set of parameters
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1.,1.);
  std::vector<double> opt_vars(funcs.GetNumParameters());
  for(int i=0; i<funcs.GetNumParameters(); ++i)
    opt_vars[i] = dis(gen);

  // Tolerances for consistency tests
  const double pertEPS = 1.e-4;
  const double tolEPS = 1.e-3;

  // Objective consistency test
  funcs.ObjectiveConsistencyTest(opt_vars, pertEPS, tolEPS);
    
  // clean up
  delete str;
  PetscFinalize();
}
