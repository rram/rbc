# Sriramajayam

project(functional-unit-tests)

# Identical for all targets
foreach(TARGET
		testTheta
    		testSectional)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC rbc_functional)
  
  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${rbc_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
