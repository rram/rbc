// Sriramajayam

#include <rbc_CubicBezierCurve.h>
#include <rbc_SectionFunctionals.h>
#include <random>
#include <iostream>

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create spline over the interval [0,1]
  rbc::NLopt_Params opt_params;
  opt_params.ftol_rel = 1.e-8;
  opt_params.xtol_rel = 1.e-8;

  rbc::NLSolverParams nl_params;
  nl_params.digits = 5;
  nl_params.max_iter = 100;
  nl_params.normTol = 1e-4;
  
  rbc::CubicBezierCurve spline({0.,1.}, 50, nl_params);
  const int nPoints = 4;
  std::vector<double> Pts{};
  for(int p=0; p<nPoints; ++p)
    {
      const double tval = static_cast<double>(p)/static_cast<double>(nPoints-1);
      Pts.push_back( tval );
      Pts.push_back( tval*tval );
    }
  spline.SetControlPoints(Pts);

  // Create elastica
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const double EI = 1.;
  rbc::Elastica* str = new rbc::Elastica(coordinates, EI);

  // Load the elastica
  std::pair<int,double> bc_params(0,0.);
  rbc::LoadParams load_params({.HVal=std::sqrt(2.)*std::cos(M_PI/3.),
	.VVal=std::sqrt(2.)*std::sin(M_PI/3.),
	.MDof=nNodes-1, .MVal=0.});

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};
  
  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1.,	.dofScale = 1.,
	.nMaxIters = 25, .verbose = false});

  // Optimization variables (H,V, M, BC, x, y)
  const std::array<bool,6> optimization_variables{true, true, true, true, true, true};
  
   // Update state/sensitivities
  str->ComputeStateAndSensitivities(load_params, bc_params, origin_params, solve_params);

  // Weight function for the objective
  rbc::WeightFunction obj_wtFunc({0.1, 0.9}, 500.); 
  obj_wtFunc.Plot("obj_wtfunc.dat", 0., 1., 100);

  // Weight function for the constraint
  rbc::WeightFunction cst_wtFunc({0.25, 0.75}, 1000.);
  cst_wtFunc.Plot("cst_wttFunc.dat", 0.,1.,100);
  
  // Functionals
  rbc::SectionFunctionals funcs(*str, load_params, bc_params, origin_params, optimization_variables, solve_params, spline, spline, obj_wtFunc, cst_wtFunc);
  const int nParameters = funcs.GetNumParameters();
  const int nConstraints = funcs.GetNumConstraints();
  assert(nParameters==6 && nConstraints==nNodes);
  
  // random set of parameters
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1.,1.);
  std::vector<double> opt_vars(nParameters);
  for(int i=0; i<nParameters; ++i)
    opt_vars[i] = dis(gen);

  // Tolerances for consistency tests
  const double pertEPS = 1.e-4;
  const double tolEPS = 1.e-3;
  
  // Consistency test for objective
  funcs.ObjectiveConsistencyTest(opt_vars, pertEPS, tolEPS);

  // Consistency test for constraints
  funcs.ConstraintConsistencyTest(opt_vars, pertEPS, tolEPS);
    
  // clean up
  delete str;
  PetscFinalize();
}
