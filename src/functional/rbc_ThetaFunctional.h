// Sriramajayam

#pragma once

#include <rbc_Functionals.h>
#include <functional>

namespace rbc
{
  using ThetaFunction = std::function<void(const double& s, const double& theta,
					   double& integrand, double* dintegrand, void* params)>;
  
  class ThetaFunctional: public Functionals
  {
  public:
    //! Constructor
    inline ThetaFunctional(Elastica& str,
			   LoadParams& lp,
			   std::pair<int,double>& bcs,
			   OriginParams& op,
			   const std::array<bool,6>& opt_vars,
			   const SolveParams& sp,
			   ThetaFunction func, void* params)
      :Functionals(str, lp, bcs, op, opt_vars, sp),
      theta_func(func),
      theta_func_params(params),
      nConstraints(0) {}

    //! Destructor
    inline virtual ~ThetaFunctional() {}

    //! Disable copy and assignment
    ThetaFunctional(const ThetaFunctional&) = delete;
    ThetaFunctional operator=(const ThetaFunctional&) = delete;

    //! Number of constraints
    inline int GetNumConstraints() const override
    { return nConstraints; }

    // Evaluate the objective and its sensitivities
    void EvaluateObjective(const std::vector<double>& x, double& Jval, double* dJvals) override;
    
    // Evaluate constraints and their sensitivities
    void EvaluateConstraints(const std::vector<double>& x, double* gvals, double* dgvals) override
    { return; }
    
  private:
    const ThetaFunction theta_func;
    void                *theta_func_params;
    const int           nConstraints;
  };
}
