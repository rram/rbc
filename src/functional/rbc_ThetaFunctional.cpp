// Sriramajayam

#include <rbc_ThetaFunctional.h>

namespace rbc
{
  // Evaluate the objective and its sensitivities
  void ThetaFunctional::EvaluateObjective(const std::vector<double>& x, double& Jval, double* dJvals)
  {
    Jval = 0.;
    if(dJvals!=nullptr)
      for(int i=0; i<nParameters; ++i)
	dJvals[i] = 0.;

    // Push x -> load_params, bc_params, origin_params
    PushOptimizationVariableValues(x);
    
    // Update the state/sensitivities
    elastica.ComputeStateAndSensitivities(load_params, dirichlet_bcs, origin_params, solve_params);
    
    // Access
    const auto& state    = elastica.GetStateField();
    const auto& L2GMap   = elastica.GetLocalToGlobalMap();
    const auto& ElmArray = elastica.GetElementArray();
    const auto& alpha_H  = elastica.GetSensitivityField(SensitivityType::H);
    const auto& alpha_V  = elastica.GetSensitivityField(SensitivityType::V);
    const auto& alpha_M  = elastica.GetSensitivityField(SensitivityType::M);
    const auto& alpha_BC = elastica.GetSensitivityField(SensitivityType::BC);
    std::vector<const std::vector<double>*> alphaVec{&alpha_H, &alpha_V, &alpha_M, &alpha_BC};
    
    // Integrate along the length of the elastica
    const int nElements = elastica.GetNumElements();
    double fval, dfval, thetaval, alphaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto& Elm = ElmArray[e];
	const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
	const auto& Qwts = Elm->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = Elm->GetDof(0);
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta at this quadrature point
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += state[L2GMap.Map(0,a,e)]*Elm->GetShape(0,q,a);

	    // Evaluate integrand
	    theta_func(Qpts[q], thetaval, fval, &dfval, theta_func_params);
	    
	    // update the objective
	    Jval += Qwts[q]*fval;

	    // Update the sensitivities for the specified variables among (H, V, M, BC)
	    if(dJvals!=nullptr)
	      for(int v=0; v<4; ++v)
		if(optVarIndex[v]!=-1)
		  {
		    const int& p = optVarIndex[v];
		    alphaval = 0.;
		    for(int a=0; a<nDof; ++a)
		      alphaval += (*alphaVec[p])[L2GMap.Map(0,a,e)]*Elm->GetShape(0,q,a);
		  
		    // Update
		    dJvals[p] += Qwts[q]*dfval*alphaval;
		  }
	  }
      }

    // Sensitivities of (x, y) variables  = 0 since J is indep
    if(dJvals!=nullptr)
      {
	if(optVarIndex[4]!=-1) // x is required
	  dJvals[optVarIndex[4]] = 0.;

	if(optVarIndex[5]!=-1) // y is required
	  dJvals[optVarIndex[5]] = 0.;
      }
	  
    // -- done --
    return;
  }
  
}
