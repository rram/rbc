// Sriramajayam

#include <rbc_SectionFunctionals.h>

namespace rbc
{
  // Evaluate the objective and its sensitivities
  void SectionFunctionals::EvaluateObjective(const std::vector<double>& x, double& Jval, double* dJvals)
  {
    return EvaluateObjective(x, 0, elastica.GetNumNodes()-1, Jval, dJvals); 
  }
  
  // Evaluate objective over a specified interval of nodes on the elastica
  void SectionFunctionals::EvaluateObjective(const std::vector<double>& x, const int nstart, const int nstop, double& Jval, double* dJvals)
  {
    Jval = 0.;
    if(dJvals!=nullptr)
      for(int i=0; i<nParameters; ++i)
	dJvals[i] = 0.;

    // Push x -> load_params, bc_params, origin_params
    PushOptimizationVariableValues(x);

    // Update state/sensitivities
    elastica.ComputeStateAndSensitivities(load_params, dirichlet_bcs, origin_params, solve_params);

    // Access
    const auto& xy = elastica.GetCartesianCoordinates();
    const auto& sense_xy_H = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::H);
    const auto& sense_xy_V = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::V);
    const auto& sense_xy_M = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::M);
    const auto& sense_xy_BC = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::BC);

    const int nNodes = elastica.GetNumNodes();
    assert(nstart>=0 && nstart<nstop && nstop<nNodes);
    
    double sd, dsd[2], factor_gradT, gradT[2];
    double tval, wval, dwval;
    double gamma[2], dgamma[2], d2gamma[2];

    // Compute the functional & sensitivities
    for(int n=nstart; n<=nstop; ++n)
      {
	// phi, grad(phi), tcoord
	tval = target_curve.GetSignedDistance(&xy[2*n], sd, dsd);

	// Weight function: w(T(Xn))
	obj_wtFunc.Evaluate(tval, wval, &dwval);

	// Update functional
	Jval += 0.5*sd*sd*wval;
	
	if(dJvals!=nullptr)
	  {
	    // gamma' and gamma'' at closest point on the curve
	    target_curve.Evaluate(tval, gamma, dgamma, d2gamma);

	    // grad(T)
	    factor_gradT = 0.;
	    for(int k=0; k<2; ++k)
	      factor_gradT += dgamma[k]*dgamma[k] - sd*dsd[k]*d2gamma[k];

	    // HACK: hard-coded tolerance
	    assert(std::abs(factor_gradT)>1.e-4);

	    for(int k=0; k<2; ++k)
	      gradT[k] = dgamma[k]/factor_gradT;

	    // sensitivities of xy
	    const double* dxy[] = {&sense_xy_H[2*n], &sense_xy_V[2*n],
				   &sense_xy_M[2*n], &sense_xy_BC[2*n]};

	    // (H, V, M, BC)
	    for(int v=0; v<4; ++v)
	      if(optVarIndex[v]!=-1)
		{
		  const int& p =optVarIndex[v];
		  assert(p<nParameters);
		  dJvals[p] +=
		    sd*wval*        (dsd[0]*dxy[p][0]   + dsd[1]*dxy[p][1]) +
		    0.5*sd*sd*dwval*(gradT[0]*dxy[p][0] + gradT[1]*dxy[p][1]);
		}

	    // X
	    if(optVarIndex[4]!=-1)
	      dJvals[optVarIndex[4]] += sd*dsd[0]*wval + 0.5*sd*sd*dwval*gradT[0];

	    // Y
	    if(optVarIndex[5]!=-1)
	      dJvals[optVarIndex[5]] += sd*dsd[1]*wval + 0.5*sd*sd*dwval*gradT[1];
	  }
      }

    // Normalize (approximate)
    const double dNodes = static_cast<double>(nstop-nstart);
    Jval /= dNodes;
    if(dJvals!=nullptr)
      for(int p=0; p<nParameters; ++p)
	dJvals[p] /= dNodes;
    
    // done
    return;
  }
    
  // Evaluate constraints and their sensitivities
  void SectionFunctionals::EvaluateConstraints(const std::vector<double>& x,
					       double* gvals,
					       double* dgvals)
  {
    assert(gvals!=nullptr);
    for(int i=0; i<nConstraints; ++i)
      gvals[i] = 0.;

    if(dgvals!=nullptr)
      for(int i=0; i<nConstraints*nParameters; ++i)
	dgvals[i] = 0.;

    // Interpret parameters: x -> load_params, dirichlet_bcs, origin_params
    PushOptimizationVariableValues(x);

    // Compute the state/sensitivities
    elastica.ComputeStateAndSensitivities(load_params, dirichlet_bcs, origin_params, solve_params);
    
    // Access
    const auto& xy = elastica.GetCartesianCoordinates();
    const auto& sense_xy_H  = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::H);
    const auto& sense_xy_V  = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::V);
    const auto& sense_xy_M  = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::M);
    const auto& sense_xy_BC = elastica.GetCartesianCoordinatesSensitivity(SensitivityType::BC);
    
    // Constraints
    double sd, dsd[2], factor_gradT, gradT[2];
    double tval, wval, dwval;
    double gamma[2], dgamma[2], d2gamma[2];
    const int nNodes = elastica.GetNumNodes();
    assert(nNodes==nConstraints);
    for(int n=0; n<nNodes; ++n)
      {
	// phi, grad(phi) at Xn
	tval = constraint_curve.GetSignedDistance(&xy[2*n], sd, dsd);

	// Weight function: w(T(Xn))
	cst_wtFunc.Evaluate(tval, wval, &dwval);
	
	// Set constraint
	gvals[n] = wval*sd;
	
	// Sensitivities
	if(dgvals!=nullptr)
	  {
	    // gamma' and gamma'' at the closest point on the curve
	    constraint_curve.Evaluate(tval, gamma, dgamma, d2gamma);

	    // grad(T)
	    factor_gradT = 0.;
	    for(int k=0; k<2; ++k)
	      factor_gradT += dgamma[k]*dgamma[k] - sd*dsd[k]*d2gamma[k];

	    // HACK: hard-coded tolerance
	    assert(std::abs(factor_gradT)>1.e-4);

	    for(int k=0; k<2; ++k)
	      gradT[k] = dgamma[k]/factor_gradT;

	    // (H, V, M, BC)
	    const double* dxy[] = {&sense_xy_H[2*n], &sense_xy_V[2*n], &sense_xy_M[2*n], &sense_xy_BC[2*n]};
	    for(int v=0; v<4; ++v)
	      if(optVarIndex[v]!=-1)
		{
		  const int& p = optVarIndex[v];
		  dgvals[nParameters*n+p] +=
		    wval*    (dsd[0]*dxy[p][0]   + dsd[1]*dxy[p][1]) +
		    sd*dwval*(gradT[0]*dxy[p][0] + gradT[1]*dxy[p][1]);
		}

	    // X
	    if(optVarIndex[4]!=-1)
	      dgvals[nParameters*n+optVarIndex[4]] += wval*dsd[0] + sd*dwval*gradT[0];
	    
	    // Y
	    if(optVarIndex[5]!=-1)
	      dgvals[nParameters*n+optVarIndex[5]] += wval*dsd[1] + sd*dwval*gradT[1];
	  }
      }
    
    // done
    return;
  }

}
