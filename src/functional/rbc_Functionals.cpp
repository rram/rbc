// Sriramajayam

#include <rbc_Functionals.h>
#include <iostream>
#include <iomanip>

namespace rbc
{
  // Interpret optimization variables: from x -> load_params, bc_params, origin_params
  void Functionals::PushOptimizationVariableValues(const std::vector<double>& x)
  {
    assert(static_cast<int>(x.size())==nParameters);
    
    // Interpret parameters
    if(optVarIndex[0]!=-1) load_params.HVal        = x[optVarIndex[0]];
    if(optVarIndex[1]!=-1) load_params.VVal        = x[optVarIndex[1]];
    if(optVarIndex[2]!=-1) load_params.MVal        = x[optVarIndex[2]];
    if(optVarIndex[3]!=-1) dirichlet_bcs.second    = x[optVarIndex[3]];
    if(optVarIndex[4]!=-1) origin_params.coords[0] = x[optVarIndex[4]];
    if(optVarIndex[5]!=-1) origin_params.coords[1] = x[optVarIndex[5]];
  }
  
  // Perform consistency test for the objective functional
  void Functionals::ObjectiveConsistencyTest(const std::vector<double>& x,
					     const double pertEPS,
					     const double tolEPS)
  {
    // Compute objective and sensitivities at given set of parameters
    double Jval = 0.;
    std::vector<double> dJvals(nParameters);
    EvaluateObjective(x, Jval, dJvals.data());

    // Compute numerical sensitivities
    std::vector<double> opt_vars = x;
    double Jplus, Jminus, dJnum;
    for(int p=0; p<nParameters; ++p)
      {
	opt_vars[p] += pertEPS;
	EvaluateObjective(opt_vars, Jplus, nullptr); 
	opt_vars[p] -= 2.*pertEPS;
	EvaluateObjective(opt_vars, Jminus, nullptr);
	opt_vars[p] += pertEPS;
      
	dJnum = (Jplus-Jminus)/(2.*pertEPS);
	// std::cout << std::setprecision(10) << std::endl << dJvals[p] << " should be " << dJnum << std::endl;
	assert(std::abs(dJnum-dJvals[p])<tolEPS);
      }
  
    // done
    return;
  }
    

  // Perform consistency test for constraint functionals
  void Functionals::ConstraintConsistencyTest(const std::vector<double>& x,
					      const double pertEPS,
					      const double tolEPS)
  {
    const int nConstraints = GetNumConstraints();

    // Compute constraints and sensitivities at given parameters
    std::vector<double> gvals(nConstraints);
    std::vector<double> dgvals(nConstraints*nParameters);
    EvaluateConstraints(x, gvals.data(), dgvals.data());

    // Numerical sensitivities
    std::vector<double> gplus(nConstraints), gminus(nConstraints);
    std::vector<double> opt_vars = x;
    for(int p=0; p<nParameters; ++p)
      {
	opt_vars[p] += pertEPS;
	EvaluateConstraints(opt_vars, gplus.data(), nullptr); 
	opt_vars[p] -= 2.*pertEPS;
	EvaluateConstraints(opt_vars, gminus.data(), nullptr);
	opt_vars[p] += pertEPS;

	for(int n=0; n<nConstraints; ++n)
	  {
	    double dnum = (gplus[n]-gminus[n])/(2.*pertEPS);
	    const double& dval = dgvals[nParameters*n+p];
	    // std::cout << std::endl << dval << " should be " << dnum << std::endl;
	    assert(std::abs(dnum-dval)<tolEPS);
	  }
      }
      
    // done
    return;
  }
  
}
