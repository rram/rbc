// Sriramajayam

#pragma once

#include <rbc_SectionFunctionals.h>

namespace rbc
{
  class WeightedSectionFunctionals: public SectionFunctionals
  {
  public:
    //! Constructor
    inline WeightedSectionFunctionals(Elastica& str,
				      LoadParams& lp,
				      std::pair<int,double>& bcs,
				      OriginParams& op,
				      const std::array<bool,6>& opt_vars,
				      const SolveParams& sp,
				      const ParametricCurve& target_crv,
				      const ParametricCurve& constraint_crv,
				      const WeightFunction& obj_wt,
				      const WeightFunction& cst_wt)
      :SectionFunctionals(str, lp, bcs, op, opt_vars, sp,
			  target_crv, constraint_crv, obj_wt, cst_wt) {}

    //! Destructor
    inline virtual ~WeightedSectionFunctionals() {}

    //! Disable copy and assignment
    WeightedSectionFunctionals(const WeightedSectionFunctionals&) = delete;
    WeightedSectionFunctionals operator=(const WeightedSectionFunctionals&) = delete;

    //! Evaluate the objective and its sensitivities
    void EvaluateObjective(const std::vector<double>& x, double& Jval, double* dJvals) override;
  };
}
