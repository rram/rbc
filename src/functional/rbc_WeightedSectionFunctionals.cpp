// Sriramajayam

#include <rbc_WeightedSectionFunctionals.h>

namespace rbc
{
  // Evaluate the objective and its sensitivities
  void WeightedSectionFunctionals::
  EvaluateObjective(const std::vector<double>& x, double& Jval, double* dJvals)
  {
    // identify active nodes on the elastica
    
    // target interval to be cut
    auto tcut = obj_wtFunc.GetInterval();

    // arc length of the target over the interval 'tcut',
    // set a minimum threshold as a fraction of the blade length
    double arclen = target_curve.ComputeArcLength(tcut.first, tcut.second);
    const double blade_len = elastica.GetLength();
    if(arclen<0.25*blade_len)
      arclen = 0.25*blade_len;
    
    // number of nodes on either size of the origin
    const double h = elastica.GetMeshSize();
    const int nhalf_nodes = static_cast<int>(0.55*arclen/h);
    assert(nhalf_nodes>=1);
    
    // Set the range of nodes, with bound checks
    int nstart = origin_params.node-nhalf_nodes;
    int nstop  = origin_params.node+nhalf_nodes;
    const int nNodes = elastica.GetNumNodes();
    if(nstart<0)
      nstart = 0;
    if(nstop>=nNodes)
      nstop = nNodes-1;

    // Evaluate the objective
    return SectionFunctionals::EvaluateObjective(x, nstart, nstop, Jval, dJvals);
  }
     
}

