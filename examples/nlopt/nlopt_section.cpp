// Sriramajayam

#include <rbc_SectionFunctionals.h>
#include <rbc_NLopt_Optimizer.h>
#include <rbc_CubicBezierCurve.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create an elastica
  const double EI = 1.;
  rbc::Elastica* elastica = new rbc::Elastica(coordinates, EI);
  
  // Trivial loads
  rbc::LoadParams load_params({.HVal=0., .VVal=0., .MDof=nNodes-1, .MVal=0.});

  // Trivial Dirichlet bcs
  std::pair<int,double> bc_params(0, 0.);

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};
  
  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1., .dofScale = 1.,
	.nMaxIters = 25, .verbose = false});

  // Target shape
  rbc::NLopt_Params opt_params;
  opt_params.set_ftol_abs = true; opt_params.set_ftol_rel = true;
  opt_params.set_xtol_abs = true; opt_params.set_xtol_rel = true;
  const double tol = 1.e-8;
  opt_params.ftol_abs = tol; opt_params.ftol_rel = tol;
  opt_params.xtol_abs = tol; opt_params.xtol_rel = tol;
  rbc::CubicBezierCurve curve(40, opt_params); 
  std::vector<double> control_points(8);
  for(int i=0; i<4; ++i)
    {
      const double t = static_cast<double>(i)/3.;
      control_points[2*i] = t;
      control_points[2*i+1] = 0.2*std::sin(t);
    }
  curve.SetControlPoints(control_points);

  // Objective functional
  const std::array<bool, 6> optimization_variables{true, true, true, true, false, false}; // H, V, M, BC
  rbc::SectionFunctionals functional(*elastica, load_params, bc_params, origin_params, optimization_variables, solve_params, curve, curve);

  // Create optimizer
  rbc::NLopt_Optimizer opt(functional.GetNumParameters(), functional.GetNumConstraints());

  // Initial guess
  std::vector<double> opt_vars{0.,0.,0.,0.};
  std::vector<double> lbounds(4, -1.e10);
  std::vector<double> ubounds(4, 1.e10);
  
  // Initial value of functional
  double J0;
  functional.EvaluateObjective(opt_vars.data(), J0, nullptr);
    
  // Optimize
  int result = opt.Optimize(functional, opt_params, &opt_vars[0], &lbounds[0], &ubounds[0]);

  // Plot the final solution
  std::fstream pfile;
  pfile.open((char*)"opt-shape.dat", std::ios::out);
  pfile << *elastica;
  pfile.close();
  
  // Check optimized parameters
  const double* vals = opt_vars.data();
  assert(std::abs(load_params.HVal-vals[0])<tol);
  assert(std::abs(load_params.VVal-vals[1])<tol);
  assert(std::abs(load_params.MVal-vals[2])<tol);
  assert(std::abs(bc_params.second-vals[3])<tol);

  // Check functional
  double Jval, dJvals[4];
  functional.EvaluateObjective(vals, Jval, dJvals);
  assert(std::abs(Jval/J0)<1.e-4);

  // Check constraints
  std::vector<double> gvals(nNodes);
  functional.EvaluateConstraints(vals, gvals.data(), nullptr);
  for(int n=0; n<nNodes; ++n)
    assert(gvals[n]<1.e-4); // g(x) <= 0
    
  // Clean up
  delete elastica;
  PetscFinalize();
}
