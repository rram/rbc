# Sriramajayam

cmake_minimum_required(VERSION 3.12)

project(rbc-nlopt-examples
  VERSION 1.0
  DESCRIPTION "Examples using rbc with nlopt")

# find dependencies
find_package(rbc REQUIRED)

list(APPEND COMPILE_FEATURES
  "cxx_constexpr"
  "cxx_auto_type"
  "cxx_decltype"
  "cxx_decltype_auto"
  "cxx_enum_forward_declarations"
  "cxx_inline_namespaces"
  "cxx_lambdas"
  "cxx_nullptr"
  "cxx_override"
  "cxx_range_for")

set(TARGET "nlopt_section")

# add executable
add_executable(${TARGET} ${TARGET}.cpp)

# include headers
target_include_directories(${TARGET} PUBLIC ${RBC_INCLUDE_DIRS})

# link directories
target_link_directories(${TARGET} PUBLIC ${RBC_LIBRARY_DIRS})
target_link_libraries(${TARGET} PUBLIC ${RBC_LIBRARIES} ${CMAKE_DL_LIBS} NLopt::nlopt)

# choose appropriate flags
target_compile_features(${TARGET} PUBLIC ${COMPILE_FEATURES})






