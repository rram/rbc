// Sriramajayam

#ifndef RBC_SPLINE_WEIGHT_FUNCTION_H
#define RBC_SPLINE_WEIGHT_FUNCTION_H

#include <rbc_Structs.h>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_errno.h>
#include <nlopt.hpp>

namespace rbc
{
  class SplineWeightFunction
  {
  public:

    //! Constructor
    SplineWeightFunction(const std::vector<double>& kv);

    //! Destructor
    virtual ~SplineWeightFunction();

    //! Disable copy and assignment
    SplineWeightFunction(const SplineWeightFunction&) = delete;
    SplineWeightFunction operator=(const SplineWeightFunction&) = delete;

    //! Main functionality: evaluate weight function and derivatives
    void Evaluate(const double tval, double& val, double* dval=nullptr, double* d2val=nullptr);

    //! Main functionality: evaluate basis functions and derivatives
    void Basis(const double tval, int* indx, double* vals, double* dvals, double* d2vals);

    //! Main functionality: Set control points by computing a best fit to given data
    void Fit(const std::vector<double>& cut_tvals,
	     const std::vector<double>& uncut_tvals,
	     const NLopt_Params& opt_params);

    //! Main functionality: Unset control points
    inline void Unset()
    { is_control_points_set = false; }

    //! Consistency test
    bool ConsistencyTest(const double tval, const double pertEPS, const double tolEPS);

    //! Plot the profile of the weight function
    void Plot(const std::string filename, const int nPoints);
    
  private:

    // spline-related
    gsl_bspline_workspace *ws;
    gsl_matrix* nz_shp; 
    bool is_control_points_set;
    const int nControlPoints;
    std::vector<double> control_points;
    double tleft[2], tright[2];
    double left_coefs[4], right_coefs[4];
    mutable int indices[4];
    mutable double Na[4], dNa[4], d2Na[4];

    // Helper function to extend a cubic spline beyond bounds
    //void Extend_Spline(const double* tinterval, double coefs[4]);

    // Helper function to evaluate extended spline
    //static void Evaluate_Spline_Extension(const double& t, const double* tinterval,
    //const double coefs[4], double& val, double* dval);
    
    // fit-related
    nlopt::opt* optimizer;
    
    //! Helper struct for passing data to NLopt
    struct NLoptStruct
    {
      const std::vector<double>* cut_tvals;
      const std::vector<double>* uncut_tvals;
      SplineWeightFunction* wfunc;
    };

    //! Evaluate the objective function and its gradient for NLP
    friend double Weight_NLopt_Objective_Function(const std::vector<double>& x,
						  std::vector<double>& grad,
						  void* params);
  };
}

#endif
