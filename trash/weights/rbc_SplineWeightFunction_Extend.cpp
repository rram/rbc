// Sriramajayam

#include <rbc_SplineWeightFunction.h>

namespace rbc
{
  // Helper function to extend a cubic spline beyond bounds
  void SplineWeightFunction::Extend_Spline(const double* tinterval, double coefs[4])
  {
    // Sample the interval [t0,t1] at 4 points 
    const double lambda[] = {1.,2./3.,1./3.,0.};
    
    // Inverse of the matrix with rows
    // T^3 T^2 T 1, where T = 1-lambda.
    const double MatInv[4][4] = {{-4.5,   13.5,   -13.5,  4.5},
				 {9.,     -22.5,  18.,    -4.5},
				 {-5.5,   9.,     -4.5,   1.},
				 {1.,     0.,     0.,      0.}};

    double vals[4] = {0.,0.,0.,0.};
    double pt[2];
    for(int p=0; p<4; ++p)
      {
	double tval = lambda[p]*tinterval[0] + (1.-lambda[p])*tinterval[1];
	Evaluate(tval, vals[p], nullptr);
      }
    
    // Solve for coefficients in terms of T
    for(int i=0; i<4; ++i)
      {
	coefs[i] = 0.;
	for(int j=0; j<4; ++j)
	  coefs[i] += MatInv[i][j]*vals[j];
      }
      
    // done
    return;
  }

  
  // Helper function to evaluate extended spline
  void SplineWeightFunction::Evaluate_Spline_Extension(const double& t, const double* tinterval,
						       const double coefs[4],
						       double& val, double* dval)
  {
    // Scale coordinate
    const double dt = (tinterval[1]-tinterval[0]);
    const double T = (t-tinterval[0])/dt;
    
    val = coefs[0]*T*T*T + coefs[1]*T*T + coefs[2]*T + coefs[3];
    
    if(dval!=nullptr)
      (*dval) = (3.*coefs[0]*T*T + 2.*coefs[1]*T + coefs[2])/dt;

    return;
  }

}
