// Sriramajayam

#include <rbc_SplineWeightFunction.h>
#include <fstream>
#include <iostream>

namespace rbc
{
  // Evaluate the objective function and its gradient for NLP
  double Weight_NLopt_Objective_Function(const std::vector<double>& x,
					 std::vector<double>& grad,
					 void* params)
  {
    // Access
    assert(params!=nullptr);
    auto& ctx = *static_cast<SplineWeightFunction::NLoptStruct*>(params);
    auto& cut_tvals = *ctx.cut_tvals;
    auto& uncut_tvals = *ctx.uncut_tvals;
    auto& spline = *ctx.wfunc;

    // Weights for cut/uncut regions
    const double wt_uncut = 100.;    // an uncut region should be respected as uncut
    const double wt_cut = 1.;      // representing a cut region as uncut is OK

    // Desired function values for cut/uncut regions
    const double f_cut = 0.;
    const double f_uncut = 1.;

    // Basis functions and derivatives
    int index[4];
    double fval, dfval, d2fval;
    double shp[4], dshp[4], d2shp[4];
    
    // evaluate objective/sensitivities
    double Jval = 0.;
    const bool deriv = (grad.empty()) ? false : true;

    if(deriv==true)
      std::fill(grad.begin(), grad.end(), 0.);

    // Track that all dofs are constrained
    std::vector<bool> is_dof(x.size());
    std::fill(is_dof.begin(), is_dof.end(), false);
    
    // contributions from cut regions
    const double Ncut = static_cast<double>(cut_tvals.size());
    if(!cut_tvals.empty())
      for(auto& t:cut_tvals)
	{
	  // basis functions
	  spline.Basis(t, index, shp, dshp, d2shp);

	  // function value
	  fval = 0.;
	  for(int i=0; i<4; ++i)
	    if(index[i]!=-1)
	      fval += x[index[i]]*shp[i];
	      
	  // update objective
	  Jval += 0.5*wt_cut*(fval-f_cut)*(fval-f_cut)/Ncut;

	  // update sensitivities
	  if(deriv==true)
	    for(int i=0; i<4; ++i)
	      if(index[i]!=-1)
		grad[index[i]] += wt_cut*(fval-f_cut)*shp[i]/Ncut;
	}


    // contributions from uncut regions
    const double Nuncut = static_cast<double>(uncut_tvals.size());
    if(!uncut_tvals.empty())
      for(auto& t:uncut_tvals)
	{
	  // basis functions
	  spline.Basis(t, index, shp, dshp, d2shp);

	  // function value
	  fval = 0.;
	  for(int i=0; i<4; ++i)
	    if(index[i]!=-1)
	      fval += x[index[i]]*shp[i];
	
	  // update objective
	  Jval += 0.5*wt_uncut*(fval-f_uncut)*(fval-f_uncut)/Nuncut;

	  // update sensitivities
	  if(deriv==true)
	    for(int i=0; i<4; ++i)
	      if(index[i]!=-1)
		grad[index[i]] += wt_uncut*(fval-f_uncut)*shp[i]/Nuncut;
	}

    // regularization term: alpha/2 (f'^2) + beta/2 (f''^2)
    double t0, t1;
    const int nIntervals = spline.ws->l;
    const auto* knots = spline.ws->knots;
    const double tsize = gsl_vector_get(knots, (nIntervals-1)+4)-gsl_vector_get(knots, 0+3); // domain of integration
    const double dt = tsize/static_cast<double>(nIntervals);
    const double alpha = 0.1*std::pow(dt,2.);
    const double beta = 0.1*std::pow(dt,4.);
    for(int e=0; e<nIntervals; ++e)
      {
	t0 = gsl_vector_get(knots, e+3);
	t1 = gsl_vector_get(knots, e+4);
	spline.Basis(0.5*(t0+t1), index, shp, dshp, d2shp);
	
	dfval = 0.;
	d2fval = 0.;
	for(int i=0; i<4; ++i)
	  if(index[i]!=-1)
	    { dfval += x[index[i]]*dshp[i];
	      d2fval += x[index[i]]*d2shp[i]; }
	      

	// Update objective
	Jval += 0.5*(t1-t0)*(alpha*dfval*dfval + beta*d2fval*d2fval)/tsize;

	// update sensitivities
	if(deriv==true)
	  for(int i=0; i<4; ++i)
	    if(index[i]!=-1)
	      grad[index[i]] += (t1-t0)*(alpha*dfval*dshp[i] + beta*d2fval*d2shp[i])/tsize;
      }
    
    // done
    return Jval;
  }
  
  
  // Constructor
  SplineWeightFunction::SplineWeightFunction(const std::vector<double>& kv)
    :is_control_points_set(false),
     nControlPoints(static_cast<int>(kv.size())-4), // nknots-orddr
     control_points(nControlPoints, 0.)
  {
    // Allocate workspace
    const int degree = 3;
    const int nknots = static_cast<int>(kv.size());
    const int order = degree+1; // Degree + 1
    const int nbreak = nControlPoints-order+2;
    ws = gsl_bspline_alloc(order, nbreak);
    nz_shp = gsl_matrix_calloc(order, 3); // 3 cols, evaluate upto 2nd derivative
    
    // Set the knot vector
    for(int k=0; k<nknots; ++k)
      gsl_vector_set(ws->knots, k, kv[k]);

    // Bounds for first interval
    tleft[0] = gsl_vector_get(ws->knots, 0+degree);   
    tleft[1] = gsl_vector_get(ws->knots, 0+degree+1);

    // Bounds for last interval
    const int nIntervals = ws->l;
    tright[0] = gsl_vector_get(ws->knots, (nIntervals-1)+degree);   
    tright[1] = gsl_vector_get(ws->knots, (nIntervals-1)+degree+1);

    // Create optimizer & set objective, gradient
    optimizer = new nlopt::opt(nlopt::LD_SLSQP, nControlPoints);

    // Bound constraints [0,1] (copied by nlopt)
    std::vector<double> bounds(nControlPoints);
    std::fill(bounds.begin(), bounds.end(), 0.);
    optimizer->set_lower_bounds(bounds);
    std::fill(bounds.begin(), bounds.end(), 1.);
    optimizer->set_upper_bounds(bounds);
  }

  
  // Destructor
  SplineWeightFunction::~SplineWeightFunction()
  { gsl_bspline_free(ws);
    gsl_matrix_free(nz_shp);
    delete optimizer; }
  

  // Main functionality: evaluate basis functions and derivatives
  void SplineWeightFunction::Basis(const double tval, int* indx, double* vals, double* dvals, double* d2vals)
  {
    // TODO: Evaluate extensions
    assert(tval>=tleft[0] && tval<=tright[1]);
    assert(indx!=nullptr && vals!=nullptr && dvals!=nullptr && d2vals!=nullptr);

    // Initialize indices to -1
    std::fill(indx, indx+4, -1);

    // Evaluate
    size_t istart, iend;
    gsl_bspline_deriv_eval_nonzero(tval, 1, nz_shp, &istart, &iend, ws);
    for(size_t i=istart; i<=iend; ++i)
      {
	indx[i-istart]  = i;
	vals[i-istart]  = gsl_matrix_get(nz_shp, i-istart, 0);
	dvals[i-istart] = gsl_matrix_get(nz_shp, i-istart, 1);
	d2vals[i-istart] = gsl_matrix_get(nz_shp, i-istart, 2);
      }

    // done
    return;
  }
  
  // Main functionality: evaluate functions and derivatives
  void SplineWeightFunction::Evaluate(const double tval, double& val, double* dval, double* d2val)
  {
    assert(is_control_points_set==true);

    // Evaluate the basis functions
    Basis(tval, indices, Na, dNa, d2Na);

    // Evaluate function
    val = 0.;
    for(int i=0; i<4; ++i)
      if(indices[i]!=-1)
	val += control_points[indices[i]]*Na[i];

    // Evaluate derivative
    if(dval!=nullptr)
      {
	(*dval) = 0.;
	for(int i=0; i<4; ++i)
	  if(indices[i]!=-1)
	    (*dval) += control_points[indices[i]]*dNa[i];
      }

    // Evaluate 2nd derivative
    if(d2val!=nullptr)
      {
	(*d2val) = 0.;
	for(int i=0; i<4; ++i)
	  if(indices[i]!=-1)
	    (*d2val) += control_points[indices[i]]*d2Na[i];
      }
    
    // done
    return;
  }

  
  // Main functionality: Set control points by computing a best fit to given data
  void SplineWeightFunction::Fit(const std::vector<double>& cut_tvals,
				 const std::vector<double>& uncut_tvals,
				 const NLopt_Params& opt_params)
  {
    // Control points should not have been set already
    assert(is_control_points_set==false);
    
    // Set the objective function & parameters
    NLoptStruct ctx;
    ctx.cut_tvals = &cut_tvals;
    ctx.uncut_tvals = &uncut_tvals;
    ctx.wfunc = this;
    optimizer->set_min_objective(Weight_NLopt_Objective_Function, &ctx);

    // Set optimization parameters
    if(opt_params.set_ftol_abs) optimizer->set_ftol_abs(opt_params.ftol_abs);
    if(opt_params.set_ftol_rel) optimizer->set_ftol_rel(opt_params.ftol_rel);
    if(opt_params.set_xtol_abs) optimizer->set_xtol_abs(opt_params.xtol_abs);
    if(opt_params.set_xtol_rel) optimizer->set_xtol_rel(opt_params.xtol_rel);
    
    // Initial guess: control_points itself

    // Optimize
    double Jval = 0.;
    nlopt::result result = nlopt::FAILURE;
    try
      { result = optimizer->optimize(control_points, Jval);
	assert(result>0);
	assert(result==nlopt::FTOL_REACHED || result==nlopt::XTOL_REACHED);
      }
    catch(std::exception& e)
      { std::cout<<"\n nlopt failed: "<<e.what()<<"\n"<<std::flush; }

    // note that control points for the weight function have been set
    is_control_points_set = true;

    // done
    return;
  }


  // Consistency test
  bool SplineWeightFunction::ConsistencyTest(const double tval, const double pertEPS, const double tolEPS)
  {
    assert(is_control_points_set);
    assert(tval-pertEPS>=tleft[0] && tval+pertEPS<=tright[1]);

    // evaluate
    double fval, dfval;
    Evaluate(tval, fval, &dfval);

    // basis functions
    Basis(tval, indices, Na, dNa, d2Na);

    // Compute function and derivatives using basis functions
    double fcheck = 0.;
    double dfcheck = 0.;
    for(int i=0; i<4; ++i)
      if(indices[i]!=-1)
	{
	  fcheck  += control_points[indices[i]]*Na[i];
	  dfcheck += control_points[indices[i]]*dNa[i];
	}
    assert(std::abs(fval-fcheck)<tolEPS);
    assert(std::abs(dfval-dfcheck)<tolEPS);

    // numerical derivatives
    double fplus, fminus;
    Evaluate(tval+pertEPS, fplus, nullptr);
    Evaluate(tval-pertEPS, fminus, nullptr);
    double dfnum = (fplus-fminus)/(2.*pertEPS);
    assert(std::abs(dfval-dfnum)<tolEPS);

    // consistency of basis function derivatives
    int indices_plus[4], indices_minus[4];
    double shp_plus[4], shp_minus[4], dShp[4], d2Shp[4];
    Basis(tval+pertEPS, indices_plus, shp_plus, dShp, d2Shp);
    Basis(tval-pertEPS, indices_minus, shp_minus, dShp, d2Shp);

    // assume the same non-zero set of basis functions after perturbation
    for(int i=0; i<4; ++i)
      if(indices[i]!=-1 && indices[i]==indices_plus[i] && indices[i]==indices_minus[i])
	{
	  double dnum = (shp_plus[i]-shp_minus[i])/(2.*pertEPS);
	  assert(std::abs(dnum-dNa[i])<tolEPS);
	}
    
    return true;
  }

  // Plot the profile of the weight function
  void SplineWeightFunction::Plot(const std::string filename, const int nPoints)
  {
    assert(is_control_points_set==true);
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    assert(pfile.good());

    double lambda, tval, wval, dwval;
    for(int i=0; i<=nPoints; ++i)
      {
	lambda = static_cast<double>(i)/static_cast<double>(nPoints);
	tval = (1.-lambda)*tleft[0] + lambda*tright[1];
	Evaluate(tval, wval, &dwval);
	pfile<<tval<<" "<<wval<<" "<<dwval<<"\n";
      }
    pfile.flush();
    pfile.close();
  }
  
}
