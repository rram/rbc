// Sriramajayam

#ifndef RBC_OBJECTIVE_FUNCTIONAL_H
#define RBC_OBJECTIVE_FUNCTIONAL_H

#include <vector>
#include <cassert>
#include <rbc_Elastica.h>

// Forward declarations
class Element;
class LocalToGlobalMap;

namespace rbc
{
  
  //! Base class for defining objective functionals
  class ObjectiveFunctional
  {
  public:
    //! Constructor
    inline ObjectiveFunctional() {}

    //! Destructor
    inline virtual ~ObjectiveFunctional() {}

    //! Copy constructor
    inline ObjectiveFunctional(const ObjectiveFunctional& obj)
    {}

    //! Main functionality: evaluate the objective by integration
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! To be implemented by derived classes
    virtual void Evaluate(const Elastica& elastica, double& Jval) = 0;

    //! Main functionality:
    //! Evaluate the objective and its sensitivities wrt each variable by integration
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] alphaVec Sensitivities
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! \param[out] dJval Computed functional gradient
    //! To be implemented by derived classes
    virtual void Evaluate(const Elastica& elastica,
			  double& Jval, std::vector<double>& dJvals) = 0;
    
  };
    
}


#endif
