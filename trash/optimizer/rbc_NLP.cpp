// Sriramajayam

#include <rbc_NLP.h>
#include <rbc_Elastica.h>
#include <rbc_Structs.h>

using namespace Ipopt;

namespace rbc
{
  // Constructor
  NLP::NLP(NLPParams& p1,
	   LoadParams& p2,
	   std::pair<int,double>& p3,
	   const SolveParams& p4)
    :nlp_params(&p1),
     load_params(&p2),
     dirichlet_bcs(&p3),
     solver_params(&p4)
  {
    const int n = 4;
    const int m = static_cast<int>(nlp_params->constraint_nodes.size());
    
    // Resize states for warm starting
    solState.x.resize(n);
    std::fill(solState.x.begin(), solState.x.end(), 0.);
    solState.zL.resize(n);
    std::fill(solState.zL.begin(), solState.zL.end(), 0.);
    solState.zU.resize(n);
    std::fill(solState.zU.begin(), solState.zU.end(), 0.);
    solState.lambda.resize(m);
    std::fill(solState.lambda.begin(), solState.lambda.end(), 0.);
  }

  // Destructor, does nothing
  NLP::~NLP() {}

  // Info about the NLP
  bool NLP::get_nlp_info(Index& n,
			 Index& m,
			 Index& nnz_jac_g,
			 Index& nnz_h_lag,
			 IndexStyleEnum& index_style)
  {
    // Optimize 4 variables: H, V, M and BC
    n = 4;

    // Number of constraints
    m = static_cast<int>(nlp_params->constraint_nodes.size());
    
    // Number of nonzeros in the jacobian of constraints: dense
    nnz_jac_g = m*n;

    // Don't assemble the Hessian
    nnz_h_lag = 0;

    // C-style indexing
    index_style = TNLP::C_STYLE;

    // done
    return true;
  }

   // Get info about the bounds on variables and constraints
  bool NLP::get_bounds_info(Index n,
			    Number* xL,
			    Number* xU,
			    Index m,
			    Number* gL,
			    Number* gU)
  {
    assert(n==4 && m==static_cast<int>(nlp_params->constraint_nodes.size()));

    // no bounds on the optimization variables
    for(int i=0; i<n; ++i)
      { xL[i] = nlp_params->Lbounds[i];
	xU[i] = nlp_params->Ubounds[i]; }

    // for constraints, lower bound = 0, upper bound = infinity
    for(int j=0; j<m; ++j)
      { gL[j] = 0.;
	gU[j] = 2.e19; }

    // done
    return true;
  }


  // Get the initial guess
  bool NLP::get_starting_point(Index n,
			       bool init_x,
			       Number* x,
			       bool init_z,
			       Number* zL,
			       Number* zU,
			       Index m,
			       bool init_lambda,
			       Number* lambda)
  {
    assert(n==4 && m==static_cast<int>(nlp_params->constraint_nodes.size()));
    assert(init_x==true);

    // Copy the initial guess: H, V, M, theta0
    x[0] = load_params->HVal;
    x[1] = load_params->VVal;
    x[2] = load_params->MVal;
    x[3] = dirichlet_bcs->second;

    // warm start
    if(init_z==true)
      for(int i=0; i<n; ++i)
	{ zL[i] = solState.zL[i];
	  zU[i] = solState.zU[i]; }

    // warm start
    if(init_lambda==true)
      for(int j=0; j<m; ++j)
	lambda[j] = solState.lambda[j];

    // done
    return true;
  }


  // Evaluate the objective functional
  bool NLP::eval_f(Index n,
		   const Number* x,
		   bool new_x,
		   Number& obj_value)
  {
    assert(n==4);

    // Helpful aliases
    auto* str = nlp_params->str;
    const auto& obj_nodes = nlp_params->obj_nodes;
    auto sdfunc = nlp_params->sdfunc;
    void* sdparams = nlp_params->sdparams;
    
    // Set the load and dirichlet bcs based on the current iterate
    load_params->HVal = x[0];
    load_params->VVal = x[1];
    load_params->MVal = x[2];
    dirichlet_bcs->second = x[3];
    
    // Evaluate the state and sensitivities at this set of loads/bcs
    str->ComputeState(*load_params, *dirichlet_bcs, *solver_params);

    // Access xy coordinates
    const auto& xy = str->GetCartesianCoordinates();

    // Initialize
    obj_value = 0.;

    // Sum squared signed-distance functions
    double phi = 0.;
    for(auto& node:obj_nodes)
      {
	sdfunc(&xy[2*node], phi, nullptr, sdparams);
	obj_value += 0.5*phi*phi;
      }
	  
    // done
    return true;
  }

  
  // Evaluate the gradient of the objective
  bool NLP::eval_grad_f(Index n,
			const Number* x,
			bool new_x,
			Number* grad_f)
  {
    assert(n==4);
    
    // Helpful aliases
    auto* str = nlp_params->str;
    const auto& obj_nodes = nlp_params->obj_nodes;
    auto sdfunc = nlp_params->sdfunc;
    void* sdparams = nlp_params->sdparams;
    
    // Set the load and dirichlet bcs based on the current iterate
    load_params->HVal = x[0];
    load_params->VVal = x[1];
    load_params->MVal = x[2];
    dirichlet_bcs->second = x[3];
    
    // Evaluate the state and sensitivities at this set of loads/bcs
    str->ComputeStateAndSensitivities(*load_params, *dirichlet_bcs, *solver_params);

    // Access xy coordinates & their sensitivities
    const auto& xy = str->GetCartesianCoordinates();
    const std::vector<const std::vector<double>*> sense_xy{&str->GetCartesianCoordinatesSensitivity(SensitivityType::H),
	&str->GetCartesianCoordinatesSensitivity(SensitivityType::V),
	&str->GetCartesianCoordinatesSensitivity(SensitivityType::M),
	&str->GetCartesianCoordinatesSensitivity(SensitivityType::BC)};
    
    // Initialize
    for(int i=0; i<n; ++i)
      grad_f[i] = 0.;

    // Compute
    double phi = 0.;
    double dphi[2] = {0.,0.};
    for(auto& node:obj_nodes)
      {
	sdfunc(&xy[2*node], phi, dphi, sdparams);
	for(int k=0; k<4; ++k)
	  grad_f[k] += phi*(dphi[0]*(*sense_xy[k])[2*node] + dphi[1]*(*sense_xy[k])[2*node+1]);
      }

    // done
    return true;
  }

  // Evaluate constraints
  bool NLP::eval_g(Index n,
		   const Number* x,
		   bool new_x,
		   Index m,
		   Number* g)
  {
    if(m==0) return true;
    
    assert(n==4 && m==static_cast<int>(nlp_params->constraint_nodes.size()));

    // Helpful aliases
    auto* str = nlp_params->str;
    const auto& constraint_nodes = nlp_params->constraint_nodes;
    auto sdfunc = nlp_params->sdfunc;
    void* sdparams = nlp_params->sdparams;
    
    // Set the load and dirichlet bcs based on the current iterate
    load_params->HVal = x[0];
    load_params->VVal = x[1];
    load_params->MVal = x[2];
    dirichlet_bcs->second = x[3];
    
    // Evaluate the state and sensitivities at this set of loads/bcs
    str->ComputeState(*load_params, *dirichlet_bcs, *solver_params);

    // Access xy coordinates
    const auto& xy = str->GetCartesianCoordinates();

    // Initialize
    for(int j=0; j<m; ++j)
      g[j] = 0.;

    // Compute constraints
    double phi = 0.;
    int indx = 0;
    for(auto& node:constraint_nodes)
      {
	sdfunc(&xy[2*node], phi, nullptr, sdparams);
	g[indx] = phi;
	++indx;
      }

    // done
    return true;
  }

  
  // Evaluate derivatives of constraints
  bool NLP::eval_jac_g(Index n,
		       const Number* x,
		       bool new_x,
		       Index m,
		       Index nele_jac,
		       Index* iRow,
		       Index* jCol,
		       Number* values)
  {
    if(m==0) return true;
    
    assert(n==4 &&
	   m==static_cast<int>(nlp_params->constraint_nodes.size()) &&
	   nele_jac==n*m);

    // Helpful aliases
    auto* str = nlp_params->str;
    const auto& constraint_nodes = nlp_params->constraint_nodes;
    auto sdfunc = nlp_params->sdfunc;
    void* sdparams = nlp_params->sdparams;

    // First call? Set row & column indices
    if(x==nullptr)
      {
	assert(iRow!=nullptr && jCol!=nullptr && values==nullptr);
	int indx = 0;
	for(auto& node:constraint_nodes)
	  {
	    for(int k=0; k<4; ++k)
	      {
		iRow[4*indx+k] = indx;
		jCol[4*indx+k] = k;
	      }
	    ++indx;
	  }

	return true;
      }

    // ELSE: x != nullptr 
    // iRow and jCol should be null and values should be non-null
    assert(iRow==nullptr && jCol==nullptr && values!=nullptr);
    
    // Set the load and dirichlet bcs based on the current iterate
    load_params->HVal = x[0];
    load_params->VVal = x[1];
    load_params->MVal = x[2];
    dirichlet_bcs->second = x[3];

    // Evaluate the state and sensitivities at this set of loads/bcs
    str->ComputeStateAndSensitivities(*load_params, *dirichlet_bcs, *solver_params);
	
    // Access xy coordinates and their sensitivities
    const auto& xy = str->GetCartesianCoordinates();
    const std::vector<const std::vector<double>*> sense_xy
    {&str->GetCartesianCoordinatesSensitivity(SensitivityType::H),
	&str->GetCartesianCoordinatesSensitivity(SensitivityType::V),
	&str->GetCartesianCoordinatesSensitivity(SensitivityType::M),
	&str->GetCartesianCoordinatesSensitivity(SensitivityType::BC)};
    
    // Compute
    int indx = 0;
    double phi= 0.;
    double dphi[2] = {0.,0.};
    for(auto& node:constraint_nodes)
      {
	sdfunc(&xy[2*node], phi, dphi, sdparams);
	for(int k=0; k<4; ++k)
	  values[4*indx+k] = dphi[0]*(*sense_xy[k])[2*node] + dphi[1]*(*sense_xy[k])[2*node+1];
	++indx;
      }

    // done
    return true;
  }

  // Finalize the solution
  void NLP::finalize_solution(SolverReturn status,
			      Index n,
			      const Number* x,
			      const Number* zL,
			      const Number* zU,
			      Index m,
			      const Number* g,
			      const Number* lambda,
			      Number obj_value,
			      const IpoptData* ip_data,
			      IpoptCalculatedQuantities* ip_cq)
  {
    assert(status==SolverReturn::SUCCESS);
    assert(n==4);

    // Save state for warm starts
    for(int i=0; i<n; ++i)
      {
	solState.x[i] = x[i];
	solState.zL[i] = zL[i];
	solState.zU[i] = zU[i];
      }

    for(int j=0; j<m; ++j)
      { solState.lambda[j] = lambda[j]; }
	
    return;
  }
  
}
