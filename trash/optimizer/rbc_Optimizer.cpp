// Sriramajayam

#include <rbc_Optimizer.h>
#include <iostream>

namespace rbc
{
  // Anonymous namespace for function/gradient evaluations
  namespace
  {
    // Context for tao optimizer
    struct OptimizerContext
    {
      int nVars;
      bool* opt_vars;
      Elastica* str;
      ObjectiveFunctional* obj;
      LoadParams* load_params;
      std::pair<int,double>* bc_params;
      OptimizeResult* result;
      const SolveParams* solve_params;
      inline void Check()
      { assert(nVars>0 && nVars<=4 &&
	       opt_vars!=nullptr && str!=nullptr && obj!=nullptr &&
	       load_params!=nullptr && bc_params!=nullptr &&
	       solve_params!=nullptr && result!=nullptr); }
    };


    // Function and gradient evaluation
    PetscErrorCode FormFunctionGradient(Tao tao, Vec solVec,
					PetscReal* J, Vec dJ, void* usr)
    {
      // Get the application context
      OptimizerContext* params;
      PetscErrorCode ierr = TaoGetApplicationContext(tao, &params); CHKERRQ(ierr);
      assert(params!=nullptr && "rbc::Optimizer::FormFunctionGradient- user context is null");
      auto& ctx = *params;
      ctx.Check();

      // Unpackage
      const int nVars = ctx.nVars;
      const bool* opt_vars = ctx.opt_vars;
      auto& str = *ctx.str;
      auto& obj = *ctx.obj;
      auto& load_params = *ctx.load_params;
      auto& bc_params = *ctx.bc_params;
      auto& solve_params = *ctx.solve_params;
      auto& result = *ctx.result;

      // Get the loads and boundary conditions at which to solve
      int count = 0;
      if(opt_vars[0]) { ierr = VecGetValues(solVec, 1, &count, &load_params.HVal); CHKERRQ(ierr); ++count; }
      if(opt_vars[1]) { ierr = VecGetValues(solVec, 1, &count, &load_params.VVal); CHKERRQ(ierr); ++count; }
      if(opt_vars[2]) { ierr = VecGetValues(solVec, 1, &count, &load_params.MVal); CHKERRQ(ierr); ++count; }
      if(opt_vars[3]) { ierr = VecGetValues(solVec, 1, &count, &bc_params.second); CHKERRQ(ierr); }
      
      // Compute the state and sensitivities
      str.ComputeStateAndSensitivities(load_params, bc_params, solve_params);

      // Evaluate the objective and its gradient wrt all variables
      obj.Evaluate(str, result.Jval, result.dJvals);

      // J and dJ required by this routine
      (*J) = result.Jval;
      for(int i=0, count=0; i<4; ++i)
	if(opt_vars[i])
	  { ierr = VecSetValues(dJ, 1, &count, &result.dJvals[i], INSERT_VALUES); CHKERRQ(ierr);
	    ++count; }
      
      // Print iteration details if requested
      if(solve_params.verbose==true)
	{
	  int iter; ierr = TaoGetIterationNumber(tao, &iter); CHKERRQ(ierr);
	  std::cout<<"\n\nIteration "<<iter<<": \n";
	  if(opt_vars[0]) std::cout<<"HVal: "<<load_params.HVal<<", ";
	  if(opt_vars[1]) std::cout<<"VVal: "<<load_params.VVal<<", ";
	  if(opt_vars[2]) std::cout<<"MVal: "<<load_params.MVal<<", ";
	  if(opt_vars[3]) std::cout<<"BC: "<<bc_params.second;
	  std::cout<<"\nJ: "<<result.Jval<<", "<<"dJ: ";
	  for(int i=0; i<nVars; ++i)
	    std::cout<<result.dJvals[i]<<", "<<std::flush;
	}
      
      // done
      return 0;
    }
  }


  // Constructor
  Optimizer::Optimizer(Elastica& istr, const std::set<SensitivityType> vars)
    :nVars(static_cast<int>(vars.size())), str(&istr)
  {
    // Ensure that PETSc is initialized
    PetscBool flag;
    PetscErrorCode ierr;
    ierr = PetscInitialized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "rbc::Optimizer: Petsc not initialized");

    // Which variables to optimize
    assert(nVars>=1 && nVars<=4);
    std::fill(opt_vars, opt_vars+4, false);
    for(auto& it:vars)
      if(it==SensitivityType::H) opt_vars[0] = true;
      else if(it==SensitivityType::V) opt_vars[1] = true;
      else if(it==SensitivityType::M) opt_vars[2] = true;
      else if(it==SensitivityType::BC) opt_vars[3] = true;
    
    // Initialize Tao data structures
    // Allocate vectors for the solution and the gradient
    ierr = VecCreateSeq(PETSC_COMM_SELF, nVars, &x); CHKERRV(ierr);
    
    // Create the Tao solver
    ierr = TaoCreate(PETSC_COMM_SELF, &tao); CHKERRV(ierr);
    ierr = TaoSetType(tao, TAOLMVM); CHKERRV(ierr);

    // Set the solution vector and initial guess
    ierr = VecSet(x, 0.); CHKERRV(ierr);
    ierr = TaoSetInitialVector(tao, x); CHKERRV(ierr);

    // Set routines for function and gradient evaluations
    ierr = TaoSetObjectiveAndGradientRoutine(tao, FormFunctionGradient, PETSC_NULL);
    CHKERRV(ierr);
    
    // Check for command line options
    ierr = TaoSetFromOptions(tao); CHKERRV(ierr);
    
    // --- done setting up --- //
  }

  // Destructor
  Optimizer::~Optimizer()
  {
    PetscErrorCode ierr;
    ierr = TaoDestroy(&tao); CHKERRV(ierr);
    ierr = VecDestroy(&x); CHKERRV(ierr);
  }

  // Main functionality
  void Optimizer::Optimize(ObjectiveFunctional& obj,
			   LoadParams& load_params,
			   std::pair<int,double>& bc_params,
			   const SolveParams& solve_params,
			   OptimizeResult& result)
  {
    PetscErrorCode ierr;

    // Set the inital guess for the variables
    int count = 0;
    if(opt_vars[0]) { ierr = VecSetValues(x, 1, &count, &load_params.HVal, INSERT_VALUES); CHKERRV(ierr); ++count; }
    if(opt_vars[1]) { ierr = VecSetValues(x, 1, &count, &load_params.VVal, INSERT_VALUES); CHKERRV(ierr); ++count; }
    if(opt_vars[2]) { ierr = VecSetValues(x, 1, &count, &load_params.MVal, INSERT_VALUES); CHKERRV(ierr); ++count; }
    if(opt_vars[3]) { ierr = VecSetValues(x, 1, &count, &bc_params.second, INSERT_VALUES); CHKERRV(ierr); }
    ierr = TaoSetInitialVector(tao, x); CHKERRV(ierr);

    // Context
    OptimizerContext ctx;
    ctx.nVars = nVars;
    ctx.opt_vars = opt_vars;
    ctx.str = str;
    ctx.obj = &obj;
    ctx.load_params = &load_params;
    ctx.bc_params = &bc_params;
    ctx.solve_params = &solve_params;
    ctx.result = &result;
    
    // Set the context
    ierr = TaoSetApplicationContext(tao, &ctx); CHKERRV(ierr);

    // Optimize
    ierr = TaoSolve(tao); CHKERRV(ierr);

    // Check convergence
    TaoConvergedReason reason;
    ierr = TaoGetConvergedReason(tao, &reason); CHKERRV(ierr);
    if( !(reason==TAO_CONVERGED_GATOL || // Absolute function tolerance
	  reason==TAO_CONVERGED_GRTOL || // Relative function tolerance
	  reason==TAO_CONVERGED_GTTOL || // Relative function tolerance
	  reason==TAO_CONVERGED_STEPTOL || // Step size is small
	  reason==TAO_CONVERGED_MINF ) )    // Threshold value for function reached
      {
	// Solution has diverged
	assert(reason!=TAO_DIVERGED_MAXITS && "Load_Optimize::Optimize- Exceeded max iterations");
	assert(reason!=TAO_DIVERGED_NAN && "Load_Optimize::Optimize- Numerical problems");
	assert(reason!=TAO_DIVERGED_MAXFCN && "Load_Optimize::Optimize- Exceeded max function evaluations");
	assert(reason!=TAO_DIVERGED_LS_FAILURE && "Load_Optimize::Optimize- line search failed");
	assert(reason!=TAO_DIVERGED_TR_REDUCTION && "Load_Optimize::Optimize- trust region failed");
      }

    // Access the solution
    ierr = TaoGetSolutionVector(tao, &x); CHKERRV(ierr);
    count = 0;
    if(opt_vars[0]) { ierr = VecGetValues(x, 1, &count, &load_params.HVal); CHKERRV(ierr); ++count; }
    if(opt_vars[1]) { ierr = VecGetValues(x, 1, &count, &load_params.VVal); CHKERRV(ierr); ++count; }
    if(opt_vars[2]) { ierr = VecGetValues(x, 1, &count, &load_params.MVal); CHKERRV(ierr); ++count; }
    if(opt_vars[3]) { ierr = VecGetValues(x, 1, &count, &bc_params.second); CHKERRV(ierr); }
    
    // done
    return;
  }
  
}
