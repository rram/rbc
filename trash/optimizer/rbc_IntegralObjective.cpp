// Sriramajayam

#include <rbc_IntegralObjective.h>
#include <Element.h>
#include <LocalToGlobalMap.h>

namespace rbc
{
  // Main functionality: evaluate the objective functional
  void IntegralObjective::Evaluate(const Elastica& elastica, double& Jval)
  {
    // Initialize objective
    Jval = 0.;

    // Access
    const auto& thetadofs = elastica.GetStateField();
    const auto& ElmArray = elastica.GetElementArray();
    const auto& L2GMap = elastica.GetLocalToGlobalMap();
    const int nElements = static_cast<int>(ElmArray.size());
    
    // Integrate F(s,theta)
    double fval, thetaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	const auto& ShpVals = ElmArray[e]->GetShape(0);
	for(int q=0; q<nQuad; ++q)
	  {
	    // Value of the state at this quadrature point
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += thetadofs[L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];
	    
	    // Value of the integrand at this point
	    func_ptr(Qpts[q], thetaval, &fval, nullptr, func_params);
	      
	    // Update the objective
	    Jval += Qwts[q]*fval;
	  }
      }

    // done
    return;
  }



  // Main functionality: evaluate the objective functional and its sensitivities
  void IntegralObjective::Evaluate(const Elastica& elastica, 
				   double& Jval, std::vector<double>& dJvals)
  {
    // Initialize objective and sensitivities
    Jval = 0.;
    const int nVars = 4;
    if(static_cast<int>(dJvals.size())<nVars) dJvals.resize(nVars);
    std::fill(dJvals.begin(), dJvals.end(), 0.);

    // Access
    const auto& ElmArray = elastica.GetElementArray();
    const auto& L2GMap = elastica.GetLocalToGlobalMap();
    const int nElements = static_cast<int>(ElmArray.size());
    
    // Access theta and sensitivities
    const auto& thetadofs = elastica.GetStateField();
    std::vector<const std::vector<double>*> alphadofs
    {&elastica.GetSensitivityField(SensitivityType::H),
	&elastica.GetSensitivityField(SensitivityType::V),
	&elastica.GetSensitivityField(SensitivityType::M),
	&elastica.GetSensitivityField(SensitivityType::BC)};
    
    // Integrate F(s,theta) and d/dvar F(s,theta)
    double fval, dfval;
    double thetaval;
    double alphaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	const auto& ShpVals = ElmArray[e]->GetShape(0);
	
	for(int q=0; q<nQuad; ++q)
	  {
	    // Theta value here
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += thetadofs[L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];
	    
	    // Integrand and its derivative wrt theta
	    func_ptr(Qpts[q], thetaval, &fval, &dfval, func_params);

	    // Update the objective
	    Jval += Qwts[q]*fval;

	    // Update the sensitivities
	    for(int var=0; var<nVars; ++var)
	      {
		// Value of the sensitivity wrt variable #var at this point
		alphaval = 0.;
		for(int a=0; a<nDof; ++a)
		  alphaval += (*alphadofs[var])[L2GMap.Map(0,a,e)]*ShpVals[q*nDof+a];

		// Update
		dJvals[var] += Qwts[q]*dfval*alphaval;
	      }
	  }
      }

    // -- done --
    return;
  }
  
}
