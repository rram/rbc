// Sriramajayam

#ifndef RBC_OPTIMIZER_H
#define RBC_OPTIMIZER_H

#include <set>
#include <rbc_Elastica.h>
#include <rbc_ObjectiveFunctional.h>
#include <rbc_Structs.h>
#include <petsctao.h>

namespace rbc
{
  //! Result of optimization
  struct OptimizeResult
  {
    double Jval;
    std::vector<double> dJvals;
  OptimizeResult():
    dJvals(4) {}
  };
  
  //! Optimize variables for a given combination of
  // an elastica and an objective
  class Optimizer
  {
  public:
    //! Constructor
    //! \param[in] istr Reference to the elastica object
    //! \param[in] ivars Optimization variables
    Optimizer(Elastica& istr, const std::set<SensitivityType> ivars);

    //! Destructor
    virtual ~Optimizer();

    //! Disable copy and assignment
    Optimizer(const Optimizer&) = delete;
    Optimizer& operator=(const Optimizer&) = delete;

    //! Access the elastica
    Elastica& GetElastica() const;

    //! Access the objective functional
    ObjectiveFunctional& GetObjective() const;

    //! Optimize
    //! \param[in] obj Objective functional
    //! \param[in] load_params Loads
    //! \param[in] bc_params Dirichlet bcs
    //! \param[in] solve_params options for ode solution
    //! \param[out] result Information about optimized solution
    void Optimize(ObjectiveFunctional& obj,
		  LoadParams& load_params,
		  std::pair<int,double>& bc_params,
		  const SolveParams& solve_params,
		  OptimizeResult& result);


  private:
    const int nVars;
    Elastica* str; //!< Pointer to the elastica
    bool opt_vars[4]; //!< Which variables to optimize among H, V, M and BC
    Tao tao; //!< Tao context
    Vec x; //!< Solution vector 
  };
}

#endif
