// Sriramajayam

//#include <rbc_P11DElement.h>
#include <P12DElement.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>

//#include <rbc_Elastica.h>
//#include <rbc_NLP.h>
//#include <rbc_Spline_RootFinding.h>
//#include <rbc_CubicBezier.h>

//#include <iostream>
//#include <fstream>

//#include <rbc_SignedDistanceFunction.h>
//#include <IpIpoptApplication.hpp>
//#include <rbc_GeomStructs.h>

int main()
{
  /*
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform mesh over the interval [0,1]
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create an elastica
  const double EI = 1.;
  rbc::Elastica str(coordinates, connectivity, EI);
  
  // Create the geometry
  std::vector<double> spline_pts{0.,0., 0.25,0., 0.75,0., 1.,0.};
  rbc::CubicBezier geom(0, spline_pts, 50);

  // Parameters for computing signed distance functions
  rbc::NLSolverParams sdsolver{.ftol=1.e-4, .ttol=1.e-4, .max_iter=25, .normTol=1.e-4};

  // Parameters to compute the signed distance function
  rbc::P3Bezier_SDParams sdparams({&geom, &sdsolver});
  
  // Trivial loads
  rbc::LoadParams load_params{.HVal=0.,.VVal=0.,.MDof=nNodes-1, .MVal=0.};

  // Trivial bcs
  rbc::DirichletBCs bc_params{.dof=0,.value=0,.xy={0.,0.},
      .h_alpha=0.,.v_alpha=0.,.m_alpha=0.,.bc_alpha=1.};

  // Solve parameters
  rbc::SolveParams solve_params{.EPS=1.e-10,.resScale=1.,.dofScale=1.,.nMaxIters=25,.verbose=false};
  */

}
