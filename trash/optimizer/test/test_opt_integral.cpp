// Sriramajayam
 
#include <rbc_Elastica.h>
#include <P11DElement.h>
#include <rbc_IntegralObjective.h>
#include <rbc_Optimizer.h>
#include <iostream>
#include <fstream>
#include <functional>

// Integrand
void TargetIntegrand(const double& s, const double& theta,
		     double* integrand, double* dintegrand, void* usr);

		     
int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create an elastica
  const double origin[] = {0.,0.};
  const double EI = 1.;
  rbc::Elastica* str = new rbc::Elastica(coordinates, EI, origin);

  // Function defining the target
  rbc::IntegrandFunction tgtfunc(TargetIntegrand);

  // Objective functional
  rbc::IntegralObjective objective(tgtfunc);
  
  // Trivial loads
  rbc::LoadParams load_params({.HVal=0., .VVal=0., .MDof=nNodes-1, .MVal=0.});

  // Trivial Dirichlet bcs
  std::pair<int,double> bc_params(0, 0.);

  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1., .dofScale = 1., .nMaxIters = 25, .verbose = true});

  // Variables to optimize
  std::set<rbc::SensitivityType> OptVars({
      rbc::SensitivityType::H, rbc::SensitivityType::V, rbc::SensitivityType::M, rbc::SensitivityType::BC});
	
  // Create optimizer
  rbc::Optimizer* Opt = new rbc::Optimizer(*str, OptVars);

  // Optimize
  rbc::OptimizeResult opt_result;
  Opt->Optimize(objective, load_params, bc_params, solve_params, opt_result);

  // Plot the solution
  const auto& state = str->GetStateField();
    std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<0.2*coordinates[n]<<"\n";
  stream.close();
  
  // Clean up
  delete str;
  delete Opt;
  PetscFinalize();
}


// Integrand
void TargetIntegrand(const double& s, const double& theta,
		     double* integrand, double* dintegrand, void* usr)
{
  // Target theta
  double tgt = 0.2*s;
  (*integrand) = 0.5*(theta-tgt)*(theta-tgt);
  (*dintegrand) = (theta-tgt);
  return;
}
