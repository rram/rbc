// Sriramajayam

#include <rbc_Elastica.h>
#include <rbc_NLP.h>
#include <P11DElement.h>
#include <iostream>
#include <fstream>
#include <IpIpoptApplication.hpp>

// Signed distance function for a circle
const double rad = 1.;
const double center[] = {0.,-rad};
void circle_sdfunc(const double* X, double& phi,
		   double* dphi, void* sdparams)
{
  double x[] = {X[0]-center[0], X[1]-center[1]};
  phi = x[0]*x[0]+x[1]*x[1]-rad*rad;
  if(dphi!=nullptr)
    {
      dphi[0] = 2.*x[0];
      dphi[1] = 2.*x[1];
    }
  return;
}


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform mesh over the interval [0,1]
  const int nNodes = 40;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = h*static_cast<double>(n);

  // Create an elastica
  const double origin[] = {0.,0.};
  const double EI = 1.;
  rbc::Elastica str(coordinates, EI, origin);

  // Trivial loads
  rbc::LoadParams load_params{.HVal=0.,.VVal=0.,.MDof=nNodes-1, .MVal=0.};

  // Trivial bcs
  std::pair<int,double> bc_params(0, 0.);

  // Solve parameters
  rbc::SolveParams solve_params{.EPS=1.e-10,.resScale=1.,.dofScale=1.,.nMaxIters=25,.verbose=false};

  // NLP parameters
  rbc::NLPParams nlp_params;
  nlp_params.str = &str;
  nlp_params.obj_nodes.clear();
  nlp_params.constraint_nodes.clear();
  for(int i=0; i<nNodes; ++i)
    { nlp_params.obj_nodes.insert(i); 
      nlp_params.constraint_nodes.insert(i);  }
  assert(nlp_params.obj_nodes==nlp_params.constraint_nodes);
  nlp_params.sdfunc = circle_sdfunc;
  nlp_params.sdparams = nullptr;
  for(int i=0; i<4; ++i)
    { nlp_params.Lbounds[i] = -0.5;
      nlp_params.Ubounds[i] = 0.5; }

  // Create NLP
  Ipopt::SmartPtr<Ipopt::TNLP> nlp = new rbc::NLP(nlp_params, load_params,
						  bc_params, solve_params);
  
  // Create optimization application
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
  //app->Options()->SetStringValue("derivative_test", "first-order");
  app->Options()->SetNumericValue("obj_scaling_factor", 1);
  app->Options()->SetStringValue("hessian_approximation", "limited-memory");
  app->Options()->SetStringValue("warm_start_init_point", "yes");
  app->Options()->SetNumericValue("warm_start_bound_push", 1.e-6);
  app->Options()->SetNumericValue("warm_start_mult_bound_push", 1.e-6);
  //app->Options()->SetStringValue("derivative_test_print_all", "yes");
  auto status = app->Initialize();
  assert(status==Ipopt::Solve_Succeeded);

  // Solve: iteratively increase the bounds
  for(int iter=0; iter<10; ++iter)
    {
      status = app->OptimizeTNLP(nlp);
      for(int i=0; i<4; ++i)
	{ nlp_params.Lbounds[i] *= 1.25;
	  nlp_params.Ubounds[i] *= 1.25; }
      
      std::cout<<"\nOptimized parameters: "
	       <<"\nH: "<<load_params.HVal
	       <<"\nV: "<<load_params.VVal
	       <<"\nM: "<<load_params.MVal
	       <<"\nBC: "<<bc_params.second
	       <<"\n"<<std::flush;
    }
  
  // Plot the solution
  const auto& xy = str.GetCartesianCoordinates();
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    { double ang = 2.*M_PI*static_cast<double>(n)/static_cast<double>(nNodes-1);
      stream << center[0]+rad*std::cos(ang)<<" "<<center[1]+rad*std::sin(ang)<<" "
	     <<xy[2*n]<<" "<<xy[2*n+1]<<"\n"; }
  stream.close();
  
}
