// Sriramajayam

#ifndef RBC_OBJECTIVE_ANGLE_PROFILE_H
#define RBC_OBJECTIVE_ANGLE_PROFILE_H

#include <rbc_ObjectiveFunctional.h>
#include <functional>

namespace rbc
{
  // Evaluate the integrand target profile at a given coordinate 's'
  // integrand -> value of the integrand
  // d_integrand -> derivative of the integrand with respect to theta
  using IntegrandFunction = std::function<void(const double& s, const double& theta,
					       double* integrand, double* dintegrand, void* params)>;

  //! Objective functional that seeks to match a
  //! given profile as a function of arc length
  class IntegralObjective: public ObjectiveFunctional
  {
  public:
    //! Constructor
    //! \param[in] obj_func Function pointer to evaluate
    //! the integrand and its sensitivities
    //! \param[in] fparams Any function parameters to be passed
    inline IntegralObjective(IntegrandFunction obj_func, void* fparams=nullptr)
      :ObjectiveFunctional(),
      func_ptr(obj_func),
      func_params(fparams) {}

    //! Destructor
    inline virtual ~IntegralObjective() {}

    //! Copy constructor
    inline IntegralObjective(const IntegralObjective & obj)
      :ObjectiveFunctional(),
      func_ptr(obj.func_ptr),
      func_params(obj.func_params) {}
    
    //! Main functionality: implement objective evaluation by integration
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    virtual void Evaluate(const Elastica& elastica, double& Jval) override;

    //! Main functionality:
    //! Evaluate the objective and its sensitivities wrt each variable by integration
    //! \param[in] theta State at which to evaluate functional
    //! \param[in] alphaVec Sensitivities
    //! \param[in] params Parameters to aid in evaluation
    //! \param[out] Jval Computed functional value
    //! \param[out] dJval Computed functional gradient
    virtual void Evaluate(const Elastica& elastica,
			  double& Jval, std::vector<double>& dJvals) override;
    
  private:
    IntegrandFunction func_ptr; //!< Function pointer to evaluate integrand and its sensitivity
    void* func_params; //!< Parameters to be passed during function evaluation
  };
  
}

#endif
