// Sriramajayam

#ifndef RBC_IPOPT_OPTIMIZER_H
#define RBC_IPOPT_OPTIMIZER_H

#include <rbc_Ipopt_NonlinearProblem.h>

namespace rbc
{

  //! Solver options for Ipopt
  struct Ipopt_Params
  {
    std::string linear_solver = "pardiso";
    double obj_scaling_factor = 1.;
    std::string hessian_approximation = "limited-memory";
    std::string warm_start_init_point = "yes";
    double warm_start_bound_push = 1.e-6;
    double warm_start_mult_bound_push = 1.e-6;
    int print_level = 0;
  };
    
  //! Wrapper class for optimization with Ipopt
  class Ipopt_Optimizer
  {
  public:
    Ipopt_Optimizer(const int nparams, const int nconstraints);

    //! Disable copy and assignment
    Ipopt_Optimizer(const Ipopt_Optimizer&) = delete;
    Ipopt_Optimizer operator=(const Ipopt_Optimizer&) = delete;

    //! Destructor
    inline virtual ~Ipopt_Optimizer()
    {};

    //! Access the solver
    inline Ipopt::IpoptApplication& GetSolver()
    { return *optimizer; }

    //! Main functionality: optimize
    int Optimize(Functionals& functionals, const Ipopt_Params& options,
		 double* x, const double* lbounds, const double* ubounds);

  private:
    Ipopt::SmartPtr<rbc::Ipopt_NonlinearProblem> nlp;
    Ipopt::SmartPtr<Ipopt::IpoptApplication> optimizer;
  };
}

#endif
