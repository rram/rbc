// Sriramajayam

#ifndef RBC_IPOPT_NONLINEAR_PROBLEM_H
#define RBC_IPOPT_NONLINEAR_PROBLEM_H

#include <IpTNLP.hpp>
#include <IpIpoptApplication.hpp>
#include <rbc_Functionals.h>

namespace rbc
{
  using namespace Ipopt;
  
  class Ipopt_NonlinearProblem: public TNLP
  {
  public:
    //! Constructor
    Ipopt_NonlinearProblem(const int nparams, const int nconstraints);

    //! Disable copy and assignment
    Ipopt_NonlinearProblem(const Ipopt_NonlinearProblem&) = delete;
    Ipopt_NonlinearProblem operator=(const Ipopt_NonlinearProblem&) = delete;
    
    //! Destructor
    inline virtual ~Ipopt_NonlinearProblem() {}

    //! Set the functional
    inline void SetFunctionals(Functionals& func)
    { functionals = &func; }

    //! Unset functional
    inline void UnsetFunctionals()
    { functionals = nullptr; }
    
    //! Set the initial guess
    inline void SetInitGuessAndBounds(const double* x, const double* lbounds, const double* ubounds)
    {
      assert(is_init_guess_set==false);
      for(int i=0; i<nParameters; ++i)
	{
	  assert(lbounds[i]<ubounds[i] && x[i]<ubounds[i] && x[i]>lbounds[i]);
	  opt_vars[i] = x[i];
	  var_lbounds[i] = lbounds[i];
	  var_ubounds[i] = ubounds[i];
	}
      is_init_guess_set = true;
    }

    //! Unset the initial guess
    inline void UnsetInitGuessAndBounds()
    { is_init_guess_set = false; }

    //! Return the solution
    inline const double* GetSolution() const
    { assert(is_init_guess_set==false);
      return solState.x.data(); }
    
    //! Info about the nlp
    //! \param[out] n Number of optimization variables
    //! \param[out] m Number of constraints
    //! \param[out] nnz_jac_g Number of nonzeros in the Jacobian of constraints
    //! \param[out] nnz_h_lag Number of nonzeros in the Hessian
    //! \param[out] index_style Indexing style
    bool get_nlp_info(Index& n,
		      Index& m,
		      Index& nnz_jac_g,
		      Index& nnz_h_lag,
		      IndexStyleEnum& index_style) override;

    //! Get info about the bounds
    //! \param[in] n Number of optimization variables
    //! \param[out] xL Lower bound on optimization variables
    //! \param[out] xU Upper bound on optimization variables
    //! \param[in] m Number of constraints
    //! \param[out] gL Lower bound on constraints
    //! \param[out] gU Upper bound on constraints
    bool get_bounds_info(Index n,
			 Number* xL,
			 Number* xU,
			 Index m,
			 Number* gL,
			 Number* gU) override;

    //! Get the initial guess
    //! \param[in] n Number of optimization variables
    //! \param[in] init_x If true, the method provides an initial guess for optimization variables
    //! \param[in] x Starting values for the optimization varaibles
    //! \param[in] init_z If true, the method provides an initial guess for bound multipliers
    //! \param[out] z_L Initial values for the bound multipliers z_L
    //! \param[out] z_U Initial values for the bound multipliers z_U
    //! \param[in] m Number of constraints
    //! \param[in] init_lambda If true, the method provides initial values for the constraint multipliers
    //! \param[out] lambda Initial guess for multipliers
    bool get_starting_point(Index n,
			    bool init_x,
			    Number* x,
			    bool init_z,
			    Number* zL,
			    Number* zU,
			    Index m,
			    bool init_lambda,
			    Number* lambda) override;


    //! Evaluate the objective functional
    //! \param[in] n Number of optimization variables
    //! \param[in] x Values of primal variables
    //! \param[in] new_x Not used, helps with efficiency. TODO
    //! \param[out] obj_value Value of the computed objective
    bool eval_f(Index n,
		const Number* x,
		bool new_x,
		Number& obj_value) override;


    //! Evaluate the gradient of the objective
    //! \param[in] n Number of optimization variables
    //! \param[in] x Values of primal variables
    //! \param[in] new_x Not used, helps with efficiency. TODO
    //! \param[out] grad_f Evaluated gradient of the objective
    bool eval_grad_f(Index n,
		     const Number* x,
		     bool new_x,
		     Number* grad_f) override;


    //! Evaluate constraints
    //! \param[in] n Number of optimization variables
    //! \param[in] x Values of primal variables
    //! \param[in] new_x Not used, helps with efficiency. TODO
    //! \param[in] m Number of constraints
    //! \param[out] g Evaluated constraints
    bool eval_g(Index n,
		const Number* x,
		bool new_x,
		Index m,
		Number* g) override;

    //! Evaluate derivatives of constraints
    //! \param[in] n Number of optimization variables
    //! \param[in] x Values of optimization variables
    //! \param[in] new_x Not used, helps with efficiency. TODO
    //! \param[in] m Number of constraints
    //! \param[in] nele_jac Number of non zero elements in the Jacobian
    //! \param[out] Array of row indices. Null after the first call
    //! \param[out] Array of col indices. Null after the first call
    //! \param[out] Values of jacobian entries. Null during the first call
    bool eval_jac_g(Index n,
		    const Number* x,
		    bool new_x,
		    Index m,
		    Index nele_jac,
		    Index* iRow,
		    Index* jCol,
		    Number* values) override;

    //! Solution return
    //! \param[in] status Whether the solve was successful
    //! \param[in] n Number of optimization variables
    //! \param[in] x Values of the optimization variables
    //! \param[in] zL values of lower bound multipliers
    //! \param[in] zU values of upper bound multipliers
    //! \param[in] m Number of constraints
    //! \param[in] Final values of constraints
    //! \param[in] lambda Final values of constraint multipliers
    //! \param[in] obj_value Final value of the objective function
    //! \param[in] ip_data Not used
    //! \param[in] ip_cq Not used
    void finalize_solution(SolverReturn status,
			   Index n,
			   const Number* x,
			   const Number* zL,
			   const Number* zU,
			   Index m,
			   const Number* g,
			   const Number* lambda,
			   Number obj_value,
			   const IpoptData* ip_data,
			   IpoptCalculatedQuantities* ip_cq) override;

  private:
    const int nParameters;
    const int nConstraints;
    std::vector<double> opt_vars;
    std::vector<double> var_lbounds, var_ubounds;
    
    // State of the solution
    struct IpOptSolutionState
    {
      std::vector<double> x;
      std::vector<double> zL, zU;
      std::vector<double> lambda;
    };
    IpOptSolutionState solState;

    mutable std::vector<double> constraint_vals;
    mutable Functionals* functionals;
    mutable bool is_init_guess_set;
  };
}

#endif
