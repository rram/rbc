// Sriramajayam

#include <rbc_Ipopt_Optimizer.h>

namespace rbc
{
  // Constructor
  Ipopt_Optimizer::Ipopt_Optimizer(const int nparams, const int nconstraints)
  {
    nlp = new rbc::Ipopt_NonlinearProblem(nparams, nconstraints);
    optimizer = IpoptApplicationFactory();
  }

  // Main functionality: optimize
  int Ipopt_Optimizer::Optimize(Functionals& functionals, const Ipopt_Params& options,
				double* x, const double* lbounds, const double* ubounds)
  {
    // Initialize
    optimizer->Options()->SetStringValue("linear_solver",               options.linear_solver);
    optimizer->Options()->SetNumericValue("obj_scaling_factor",         options.obj_scaling_factor);
    optimizer->Options()->SetStringValue("hessian_approximation",       options.hessian_approximation);
    optimizer->Options()->SetStringValue("warm_start_init_point",       options.warm_start_init_point);
    optimizer->Options()->SetNumericValue("warm_start_bound_push",      options.warm_start_bound_push);
    optimizer->Options()->SetNumericValue("warm_start_mult_bound_push", options.warm_start_mult_bound_push);
    optimizer->Options()->SetIntegerValue("print_level",                5); //options.print_level);
    auto status = optimizer->Initialize();
    assert(status==Ipopt::Solve_Succeeded);

    // Set the functional, initial guess and bounds
    nlp->SetFunctionals(functionals);
    nlp->SetInitGuessAndBounds(x, lbounds, ubounds);

    // Optimize
    status = optimizer->OptimizeTNLP(nlp);
    assert(status==Ipopt::Solve_Succeeded);
    
    // Save the solution
    auto* sol = nlp->GetSolution();
    const int nParameters = functionals.GetNumParameters();
    for(int i=0; i<nParameters; ++i)
      x[i] = sol[i];
    
    // Unset functional and initial guess in nlp
    nlp->UnsetFunctionals();
    nlp->UnsetInitGuessAndBounds();

    // done
    return status;
  }

}
