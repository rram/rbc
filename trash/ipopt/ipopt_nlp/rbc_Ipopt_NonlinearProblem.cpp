// Sriramajayam

#include <rbc_Ipopt_NonlinearProblem.h>

namespace rbc
{
  using namespace Ipopt;
  
  // Constructor
  Ipopt_NonlinearProblem::Ipopt_NonlinearProblem(const int nparams, const int nconstraints)
    :nParameters(nparams), 
     nConstraints(nconstraints),
     opt_vars(nParameters),
     var_lbounds(nParameters),
     var_ubounds(nParameters),
     constraint_vals(nConstraints),
     functionals(nullptr),
     is_init_guess_set(false)
  {
    // Data structures for a warm start
    solState.x.resize(nParameters);
    solState.zL.resize(nParameters);
    solState.zU.resize(nParameters);
    solState.lambda.resize(nConstraints);
    std::fill(solState.x.begin(),  solState.x.end(), 0.);
    std::fill(solState.zL.begin(), solState.zL.end(), 0.);
    std::fill(solState.zU.begin(), solState.zU.end(), 0.);
    std::fill(solState.lambda.begin(), solState.lambda.end(), 0.);
  }

  // Info about the NLP
  bool Ipopt_NonlinearProblem::get_nlp_info(Index& n,
					    Index& m,
					    Index& nnz_jac_g,
					    Index& nnz_h_lag,
					    IndexStyleEnum& index_style)
  {
    n = nParameters;   // # parameters
    m = nConstraints;  // # constraints
    nnz_jac_g = m*n;  // # non-zeros in the jacobian (dense matrix)
    nnz_h_lag = 0;    // Don't assemble the hessian
    index_style = TNLP::C_STYLE; // C-style indexing

    return true;
  }


  // Info about the bounds on variables and constraints
  bool Ipopt_NonlinearProblem::get_bounds_info(Index n,
					       Number* xL,
					       Number* xU,
					       Index m,
					       Number* gL,
					       Number* gU)
  {
    assert(is_init_guess_set==true);
    assert(n==nParameters && m==nConstraints);

    // Set bounds on variables
    for(int i=0; i<nParameters; ++i)
      { xL[i] = var_lbounds[i];
	xU[i] = var_ubounds[i]; }

    // Constraint bounds: upper-bound = 0, lower bound = -inf
    for(int j=0; j<nConstraints; ++j)
      { gL[j] = -1.e19;
	gU[j] = 0.; }

    // done
    return true;
  }


  // Get the initial guess
  bool Ipopt_NonlinearProblem::get_starting_point(Index n,
						  bool init_x,
						  Number* x,
						  bool init_z,
						  Number* zL,
						  Number* zU,
						  Index m,
						  bool init_lambda,
						  Number* lambda)
  {
    assert(n==nParameters && m==nConstraints);
    assert(is_init_guess_set==true);
    assert(init_x==true);

    // Copy initial guess
    for(int i=0; i<nParameters; ++i)
      x[i] = opt_vars[i];
    
    // warm start?
    if(init_z==true)
      for(int i=0; i<nParameters; ++i)
	{ zL[i] = solState.zL[i];
	  zU[i] = solState.zU[i]; }
    
    if(init_lambda==true)
      for(int j=0; j<nConstraints; ++j)
	lambda[j] = solState.lambda[j];

    // done
    return true;
  }

  // Evaluate the objective functional
  bool Ipopt_NonlinearProblem::eval_f(Index n,
				      const Number* x,
				      bool new_x,
				      Number& obj_value)
  {
    assert(functionals!=nullptr);
    assert(n==nParameters);
    functionals->EvaluateObjective(x, obj_value, nullptr);
    return true;
  }

  // Evaluate the sensitivities of the objective functional
  bool Ipopt_NonlinearProblem::eval_grad_f(Index n,
					   const Number* x,
					   bool new_x,
					   Number* grad_f)
  {
    assert(functionals!=nullptr);
    assert(n==nParameters);
    double Jval;
    functionals->EvaluateObjective(x, Jval, grad_f);
    return true;
  }


  // Evaluate constraints
  bool Ipopt_NonlinearProblem::eval_g(Index n,
				      const Number* x,
				      bool new_x,
				      Index m,
				      Number* g)
  {
    assert(functionals!=nullptr);
    if(m==0) return true;
    assert(n==nParameters && m==nConstraints);
    functionals->EvaluateConstraints(x, g, nullptr);
    return true;
  }


  // Evaluate derivatives of constraints
  bool Ipopt_NonlinearProblem::eval_jac_g(Index n,
					  const Number* x,
					  bool new_x,
					  Index m,
					  Index nele_jac,
					  Index* iRow,
					  Index* jCol,
					  Number* values)
  {
    assert(functionals!=nullptr);
    if(m==0) return true;
    assert(n==nParameters && m==nConstraints && nele_jac==n*m);

    // First call? Set row & column indices
    if(x==nullptr)
      {
	assert(iRow!=nullptr && jCol!=nullptr && values==nullptr);
	for(int i=0; i<nConstraints; ++i)
	  for(int j=0; j<nParameters; ++j)
	    {
	      iRow[nParameters*i+j] = i;
	      jCol[nParameters*i+j] = j;
	    }
	 
	return true;
      }

    // Else: x, values != null
    // iRow, jCol = null
    assert(values!=nullptr && iRow==nullptr && jCol==nullptr);

    // evaluate
    functionals->EvaluateConstraints(x, constraint_vals.data(), values);

    return true;
  }

  // Finalize the solution
  void Ipopt_NonlinearProblem::finalize_solution(SolverReturn status,
						 Index n,
						 const Number* x,
						 const Number* zL,
						 const Number* zU,
						 Index m,
						 const Number* g,
						 const Number* lambda,
						 Number obj_value,
						 const IpoptData* ip_data,
						 IpoptCalculatedQuantities* ip_cq)
  {
    assert(status==SolverReturn::SUCCESS);
    assert(n==nParameters);

    // Save the state for warm starts
    for(int i=0; i<nParameters; ++i)
      {
	opt_vars[i] = x[i];
	solState.x[i] = x[i];
	solState.zL[i] = zL[i];
	solState.zU[i] = zU[i];
      }

    for(int j=0; j<nConstraints; ++j)
      solState.lambda[j] = lambda[j];

    // Insist on explicitly setting the initial guess before the next solve
    is_init_guess_set = false;
    
    return;
  }

}
