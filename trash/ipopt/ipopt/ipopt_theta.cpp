// Sriramajayam

#include <rbc_ThetaFunctional.h>
#include <rbc_Ipopt_Optimizer.h>
#include <P11DElement.h>
#include <iostream>

// Integrand
void TargetTheta(const double& s, const double& theta,
		 double& integrand, double* dintegrand, void* usr);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);

  // Create an elastica
  const double EI = 1.;
  rbc::Elastica* elastica = new rbc::Elastica(coordinates, EI);
  
  // Trivial loads
  rbc::LoadParams load_params({.HVal=0., .VVal=0., .MDof=nNodes-1, .MVal=0.});

  // Trivial Dirichlet bcs
  std::pair<int,double> bc_params(0, 0.);

  // Origin
  rbc::OriginParams origin_params{.node=0, .coords={0.,0.}};
  
  // Solve parameters
  rbc::SolveParams solve_params({.EPS = 1.e-10, .resScale = 1., .dofScale = 1.,
	.nMaxIters = 25, .verbose = false});

  // Function defining the target
  rbc::ThetaFunction tgtfunc(TargetTheta);

  // Optimization variables
  std::array<bool, 6> optimization_variables{true, true, true, true, false, false}; // H, V, M, BC
  // Objective functional
  rbc::ThetaFunctional functional(*elastica, load_params, bc_params, origin_params, optimization_variables,
				  solve_params, tgtfunc, nullptr);

  // Initial guess
  std::vector<double> opt_vars{0.,0.,0.,0.};

  // Variable bounds
  std::vector<double> lbounds(4, -1.);
  std::vector<double> ubounds(4, 1.);

  // Initial value of functional
  double J0;
  functional.EvaluateObjective(opt_vars.data(), J0, nullptr);
  
  // Create optimizer
  auto optimizer = new rbc::Ipopt_Optimizer(functional.GetNumParameters(), functional.GetNumConstraints());

  // Ipopt options with defaults
  rbc::Ipopt_Params ipopt_params;

  // Optimize
  auto status = optimizer->Optimize(functional, ipopt_params, opt_vars.data(), lbounds.data(), ubounds.data());
  assert(status==Ipopt::Solve_Succeeded);
    
  // Check optimized parameters
  const double tol = 1.e-4;
  const double* vals = opt_vars.data();
  assert(std::abs(load_params.HVal-vals[0])<tol);
  assert(std::abs(load_params.VVal-vals[1])<tol);
  assert(std::abs(load_params.MVal-vals[2])<tol);
  assert(std::abs(bc_params.second-vals[3])<tol);
  std::cout<<"\nSolution: "<<vals[0]<<", "<<vals[1]<<", "<<vals[2]<<", "<<vals[3]<<std::flush;

  // Check sensitivities of functional
  double Jval, dJvals[4];
  functional.EvaluateObjective(vals, Jval, dJvals);
  assert(std::abs(Jval/J0)<1.e-5);
  for(int i=0; i<4; ++i)
    assert(std::abs(dJvals[i])<1.e-4);
    
  // Clean up
  delete elastica;
}
  
  
// Integrand
void TargetTheta(const double& s, const double& theta,
		 double& integrand, double* dintegrand, void* usr)
{
  // Target theta
  double tgt = 0.5*s;
  integrand = 0.5*(theta-tgt)*(theta-tgt);
  (*dintegrand) = (theta-tgt);
  return;
}
