// Sriramajayam

#ifndef RBC_PARAMETRIC_CURVE_COMBINATION_H
#define RBC_PARAMETRIC_CURVE_COMBINATION_H

#include <rbc_ParametricCurve.h>

namespace rbc
{
  //! Class defining the convex linear combination of a pair of parametric curves
  class ParametricCurveCombination: public ParametricCurve
  {
  public:
    //! Constructor
     ParametricCurveCombination(const ParametricCurve& c1, const ParametricCurve& c2,
				const int nsamples, const NLopt_Params& op);
     
    //! Disable copy and assignment
    ParametricCurveCombination(const ParametricCurveCombination&) = delete;
    ParametricCurveCombination& operator=(const ParametricCurveCombination&) = delete;

    //! Destructor
    inline virtual ~ParametricCurveCombination() {}

    //! Return the bounds of the parametrization
    void GetParameterBounds(double& t0, double& t1) const;

    //! Set the combination factor
    void SetCombinationFactor(const double val);
    
    //! Evaluate coordinates
    void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr) const;

  private:
    const ParametricCurve& curveA;
    const ParametricCurve& curveB;
    double tleft, tright;
    double alpha;
    mutable bool is_factor_set;
  };
}

#endif

