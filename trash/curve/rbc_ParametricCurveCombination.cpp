// Sriramajayam

#include <rbc_ParametricCurveCombination.h>

namespace rbc
{
  // Constructor
  ParametricCurveCombination::
  ParametricCurveCombination(const ParametricCurve& c1, const ParametricCurve& c2,
			     const int nsamples, const NLopt_Params& op)
    :ParametricCurve(nsamples, op),
     curveA(c1), curveB(c2),
     is_factor_set(false)
  {
    // Parameter bounds should be identical
    double left_A, right_A;
    curveA.GetParameterBounds(left_A, right_A);
    double left_B, right_B;
    curveB.GetParameterBounds(left_B, right_B);
    assert(std::abs(left_A-left_B) + std::abs(right_A-right_B)<1.e-4);
    tleft = left_A;
    tright = right_A;
  }

  // Return the bounds of the parametrization
  void ParametricCurveCombination::GetParameterBounds(double& t0, double& t1) const
  {
    t0 = tleft;
    t1 = tright;
    return;
  }

  // Set the combination factor
  void ParametricCurveCombination::SetCombinationFactor(const double val)
  {
    alpha = val;
    is_factor_set = true;

    // populate the r-tree
    double lambda, tval, A[2], B[2], X[2];
    for(int p=0; p<nSamples_Per_Interval; ++p)
      {
	lambda = 1.-static_cast<double>(p)/static_cast<double>(nSamples_Per_Interval);
	tval = lambda*tleft + (1.-lambda)*tright;
	curveA.Evaluate(tval, A);
	curveB.Evaluate(tval, B);
	X[0] = alpha*A[0] + (1.-alpha)*B[0];
	X[1] = alpha*A[1] + (1.-alpha)*B[1];
	rtree.insert( pointParam(boost_point2D(X[0],X[1]),lambda) );
      }

    // done
    return;
  }
							
    
  // Evaluate coordinates
  void ParametricCurveCombination::Evaluate(const double& t, double* X, double* dX, double* d2X) const
  {
    assert(is_factor_set==true);

    // Evaluate
    double A[2], dA[2], d2A[2];
    curveA.Evaluate(t, A, dA, d2A);
    double B[2], dB[2], d2B[2];
    curveB.Evaluate(t, B, dB, d2B);

    if(X!=nullptr)
      for(int k=0; k<2; ++k)
	X[k] = alpha*A[k] + (1.-alpha)*B[k];

    if(dX!=nullptr)
      for(int k=0; k<2; ++k)
	dX[k] = alpha*dA[k] + (1.-alpha)*dB[k];

    if(d2X!=nullptr)
      for(int k=0; k<2; ++k)
	d2X[k] = alpha*d2A[k] + (1.-alpha)*d2B[k];

    return;
  }

}
