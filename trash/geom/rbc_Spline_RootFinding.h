// Sriramajayam

#ifndef RBC_SPLINE_ROOT_FINDING_H
#define RBC_SPLINE_ROOT_FINDING_H

#include <rbc_GeomStructs.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>

namespace rbc
{
  namespace detail
  {
    //! Root finding for closest point detection
    template<typename GeomType>
      struct Spline_RootFinding_Context
      {
	const double* X; //!< Point for which to find closest
	GeomType* spline; //!< Invoking object
	gsl_root_fdfsolver* solver; //!< Solver for 1D root finding
	gsl_function_fdf fdf; //!< Function for root finding
	double rvec[2], drvec[2], d2rvec[2]; //! Mutables
      };

    //! Function value: (r(t)-X).r'(t)
    template<typename GeomType>
      double Spline_RootFinding_f(double t, void* params);

    //! Derivative of (r(t)-X).r'(t) = r'(t).r'(t) + (r(t)-X).r''(t)
    template<typename GeomType>
      double Spline_RootFinding_df(double t, void* params);

    //! Function & derivative of (r(t)-X).r'(t)
    template<typename GeomType>
      void Spline_RootFinding_fdf(double t, void* params, double* val, double* dval);

    //! Finds the root 't' of the function (r(t)-X).r'(t) = 0
    template<typename GeomType>
      double Spline_FindRoot(const double* X, 
			     const double tinit,
			     const double tbounds[2],
			     const NLSolverParams& nlparams,
			     Spline_RootFinding_Context<GeomType>& rootfinding_ctx);
    
  } // rbc::detail::
} // rbc::

#endif

// Implementation of templated functions
#include <rbc_Spline_RootFinding_Impl.h>
