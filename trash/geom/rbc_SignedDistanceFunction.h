// Sriramajayam

#ifndef RBC_SIGNED_DISTANCE_H
#define RBC_SIGNED_DISTANCE_H

#include <rbc_CubicBezier.h>
#include <rbc_CubicSpline.h>

namespace rbc
{
  //! Signed distance function for a geometry
  //! params is assumed to be of a pointer to std::pair<GType*, NLSolverParams*> object
  template<class GeomType>
    inline void Geometry_SignedDistanceFunction(const double* X, double& sd, double* dsd, void* params)
    {
      assert(params!=nullptr);
      auto ctx = static_cast<std::pair<GeomType*, NLSolverParams*>*>(params);
      auto geom = ctx->first;
      auto nlparams = ctx->second;
      geom->GetSignedDistance(X, *nlparams, sd, dsd);
    }

  //! Useful aliases
  using P3Bezier_SDParams = std::pair<CubicBezier*, NLSolverParams*>;
  using P3Spline_SDParams = std::pair<CubicSpline*, NLSolverParams*>;
  
  //! Explicit instantiations of templated signed distance function
  auto p3bezier_sdfunc = Geometry_SignedDistanceFunction<CubicBezier>;
  auto p3spline_sdfunc = Geometry_SignedDistanceFunction<CubicSpline>;
  
}

#endif
