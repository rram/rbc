// Sriramajayam

#ifndef RBC_GEOM_STRUCTS_H
#define RBC_GEOM_STRUCTS_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace rbc
{
  //! \brief Struct encapsulating parameters to be used when computing the signed distance function
  struct NLSolverParams
  {
    double ftol; //!< Tolerance to use for checking function convergence
    double ttol; //!< Tolerance to use for checking convergence of the root
    int max_iter; //!< Maximum number of permitted iterations
    double normTol; //!< Tolerance for the norms of vectors. Vector norms will be checked to be larger than this.
  };

  // helper aliases for boost utilities
  using boost_point2D = boost::geometry::model::point<double, 2, boost::geometry::cs::cartesian>;
  using boost_box2D = boost::geometry::model::box<boost_point2D>;
  using boost_point_rtree2D = boost::geometry::index::rtree<boost_point2D, boost::geometry::index::quadratic<8>>;
  using boost_pointID_rtree2D = boost::geometry::index::rtree<std::pair<boost_point2D,int>, boost::geometry::index::quadratic<8>>;
}

#endif
