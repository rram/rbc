// Sriramajayam

#include <cmath>
#include <iostream>
#include <fstream>
#include <rbc_CubicSpline.h>
#include <random>

int main()
{
  // Semi-circle
  const int nPoints = 10;
  std::vector<double> tvec{};
  for(int i=0; i<nPoints; ++i)
    tvec.push_back( M_PI*static_cast<double>(i)/static_cast<double>(nPoints-1) );
  std::vector<double> xy{};
  for(auto& t:tvec)
    { xy.push_back( std::cos(t) );
      xy.push_back( std::sin(t) ); }
  
  // Create spline with 10 sample points per interval
  const int id = 1;
  rbc::CubicSpline spline(id, tvec, xy, 10);

  // Solver params
  rbc::NLSolverParams nlparams{.ftol=1.e-4, .ttol=1.e-5, .max_iter=20, .normTol=1.e-3};
  
  // Plot the spline
  std::fstream pfile;
  pfile.open((char*)"spline.dat", std::ios::out);
  const double t0 = tvec[0];
  const double t1 = tvec[tvec.size()-1];
  for(int n=0; n<73; ++n)
    {
      double lambda = static_cast<double>(n)/72;
      double t = (1.-lambda)*t0 + lambda*t1;
      double Y[2], dY[2], d2Y[2];
      spline.Evaluate(t, Y, dY, d2Y);
      pfile << t<<" "<<Y[0]<<" "<<Y[1]<<" "<<dY[0]<<" "<<dY[1]<<" "<<d2Y[0]<<" "<<d2Y[1]<<"\n";
    }
  pfile.close();

  // Closest point searches for a random set of 100 points
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> xdis(-0.95,0.95);
  std::uniform_real_distribution<> ydis(0.05, 1.5);
  for(int n=0; n<100; ++n)
    {
      double X[] = {xdis(gen), ydis(gen)};
      double sd = 0.;
      double dsd[2];
      spline.GetSignedDistance(X, nlparams, sd, dsd);
    }
}
