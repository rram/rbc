// Sriramajayam

#include <rbc_CubicSpline.h>
#include <iostream>
#include <cassert>

namespace rbc
{
  // Constructor
  CubicSpline::CubicSpline(int id,
			   const std::vector<double>& tvec,
			   const std::vector<double>& pts,
			   const int nsamples)
    :curve_id(id),
     tmin(tvec[0]),
     tmax(tvec[tvec.size()-1])
  {
    const int nPoints = static_cast<int>(tvec.size());

    // Sanity checks on the data
    assert(nPoints>=2);
    assert(nsamples>=1);
    assert(static_cast<int>(pts.size())==2*nPoints);
    for(int i=0; i<nPoints-1; ++i)
      { assert(tvec[i]<tvec[i+1]); }

    // X & Y data
    std::vector<double> xvec(nPoints), yvec(nPoints);
    for(int p=0; p<nPoints; ++p)
      { xvec[p] = pts[2*p];
	yvec[p] = pts[2*p+1]; }

    // Accelerators
    xacc = gsl_interp_accel_alloc();
    yacc = gsl_interp_accel_alloc();

    // X spline
    xspline = gsl_spline_alloc(gsl_interp_cspline, nPoints);
    gsl_spline_init(xspline, &tvec[0], &xvec[0], nPoints);

    // Y spline
    yspline = gsl_spline_alloc(gsl_interp_cspline, nPoints);
    gsl_spline_init(yspline, &tvec[0], &yvec[0], nPoints);

    // Extend the spline at the left end
    tfirst[0] = tvec[0];
    tfirst[1] = tvec[1];
    ExtendSpline(tfirst[0], tfirst[1], xspline, xacc, yspline, yacc, first_coefs);

    // Extend the spline at the right end
    tlast[0] = tvec[nPoints-2];
    tlast[1] = tvec[nPoints-1];
    ExtendSpline(tlast[0], tlast[1], xspline, xacc, yspline, yacc, last_coefs);
    
    // Populate the r-tree with sample points
    const int nIntervals = nPoints-1;
    double lambda, t, x, y;
    pointParam pp;
    int indx = 0;
    for(int interval=0; interval<nIntervals; ++interval)
      {
	const double& t0 = tvec[interval];
	const double& t1 = tvec[interval+1];

	for(int s=0; s<nsamples; ++s)
	  {
	    lambda = static_cast<double>(s)/static_cast<double>(nsamples);
	    t = (1.-lambda)*t0 + lambda*t1;
	    x = gsl_spline_eval(xspline, t, xacc);
	    y = gsl_spline_eval(yspline, t, yacc);
	    pp.first = boost_point2D(x, y);
	    pp.second = std::pair<int,double>(indx++,t);
	    rtree.insert(pp);
	  }
      }
    // Add the end point
    pp.first = boost_point2D(xvec[nPoints-1], yvec[nPoints-1]);
    pp.second = std::pair<int,double>(indx++,tvec[nPoints-1]);
    rtree.insert(pp);


    // Setup root-finding
    rootfinding_ctx.spline = this;
    rootfinding_ctx.solver = gsl_root_fdfsolver_alloc(gsl_root_fdfsolver_steffenson);
    rootfinding_ctx.fdf.f = &detail::Spline_RootFinding_f<CubicSpline>;
    rootfinding_ctx.fdf.df = &detail::Spline_RootFinding_df<CubicSpline>;
    rootfinding_ctx.fdf.fdf = &detail::Spline_RootFinding_fdf<CubicSpline>;
    rootfinding_ctx.fdf.params = &rootfinding_ctx;
    
    // done
  }


  // Helper function to extend a spline
  void CubicSpline::ExtendSpline(const double& t0, const double& t1,
				 gsl_spline* xspline, gsl_interp_accel* xacc,
				 gsl_spline* yspline, gsl_interp_accel* yacc,
				 double coefs[2][4])
  {
    // Sample the interval [t0,t1] at 4 points 
    const double lambda[] = {1.,2./3.,1./3.,0.};

    // Inverse of the matrix with rows
    // T^3 T^2 T 1, where T = 1-lambda.
    const double MatInv[4][4] = {{-4.5,   13.5,   -13.5,  4.5},
				 {9.,     -22.5,  18.,    -4.5},
				 {-5.5,   9.,     -4.5,   1.},
				 {1.,     0.,     0.,      0.}};

    double xy[2][4];
    for(int i=0; i<4; i++)
      {
	double tval = lambda[i]*t0 + (1.-lambda[i])*t1;
	xy[0][i] = gsl_spline_eval(xspline, tval, xacc);
	xy[1][i] = gsl_spline_eval(yspline, tval, yacc);
      }

    // Solve for coefficients for x and y parameterizations in terms of T
    for(int k=0; k<2; ++k)
      for(int i=0; i<4; i++)
	{ coefs[k][i] = 0.;
	  for(int j=0; j<4; ++j)
	    coefs[k][i] += MatInv[i][j]*xy[k][j];  }
      
    // done
    return;
  }
  
  
  // Destructor: clean up
  CubicSpline::~CubicSpline()
  {
    // delete splines
    gsl_spline_free(xspline);
    gsl_spline_free(yspline);

    // delete accelerators
    gsl_interp_accel_free(xacc);
    gsl_interp_accel_free(yacc);

    // delete root-finder
    gsl_root_fdfsolver_free(rootfinding_ctx.solver);
  }

  // Curve ID
  int CubicSpline::GetCurveID() const
  { return curve_id; }

  // Returns the end points of the curve
  void CubicSpline::GetTerminalPoints(double* tpoints)
  {
    Evaluate(tmin, tpoints);
    Evaluate(tmax, tpoints+2);
    return;
  }

  
  // Evaluate the coordinates of the spline at the requested point
  void CubicSpline::Evaluate(const double& t, double* X, double* dX, double* d2X)
  {
    // Need to evaluate the extension?
    if(t<tmin)
      Evaluate_Extension(t, tfirst[0], tfirst[1], first_coefs, X, dX, d2X);
    else if(t>tmax)
      Evaluate_Extension(t, tlast[0], tlast[1], last_coefs, X, dX, d2X);
    else // extension not required
      {
	// Coordinates
	assert(X!=nullptr);
	X[0] = gsl_spline_eval(xspline, t, xacc);
	X[1] = gsl_spline_eval(yspline, t, yacc);

	// Derivatives
	if(dX!=nullptr)
	  { dX[0] = gsl_spline_eval_deriv(xspline, t, xacc);
	    dX[1] = gsl_spline_eval_deriv(yspline, t, yacc); }

	if(d2X!=nullptr)
	  { d2X[0] = gsl_spline_eval_deriv2(xspline, t, xacc);
	    d2X[1] = gsl_spline_eval_deriv2(yspline, t, yacc); }
      }
    // done
    return;
  }


  // Evaluate spline extension for t<tmin or t>tmax
  void CubicSpline::Evaluate_Extension(const double& t, const double& t0, const double& t1,
				       const double coefs[2][4],
				       double* X, double* dX, double* d2X)
  {
    // Scale coordinate
    const double dt = (t1-t0);
    const double T = (t-t0)/dt;
    
    if(X!=nullptr)
      for(int k=0; k<2; ++k)
	X[k] = coefs[k][0]*T*T*T + coefs[k][1]*T*T + coefs[k][2]*T + coefs[k][3];
    
    if(dX!=nullptr)
      for(int k=0; k<2; ++k)
	dX[k] = (3.*coefs[k][0]*T*T + 2.*coefs[k][1]*T + coefs[k][2])/dt;

    if(d2X!=nullptr)
      for(int k=0; k<2; ++k)
	d2X[k] = (6.*coefs[k][0]*T + 2.*coefs[k][1])/(dt*dt);

    // done
    return;
  }
  
  
  // Returns the signed distance function to a point
  void CubicSpline::GetSignedDistance(const double* X,
				      const NLSolverParams& nlparams,
				      double& sd, double* dsd)
  {
    // Initial guess for the closest point
    boost_point2D pt(X[0], X[1]);
    std::vector<pointParam> result{};
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);
    double tinit = (result[0].second).second;
    
    // Extremizer of the distance function
    const double tbounds[] = {tmin, tmax};
    const double t = detail::Spline_FindRoot(X, tinit, tbounds,
					     nlparams, rootfinding_ctx);
    
    // Evaluate the spline and its derivatives here.
    // Y is the closest point projection
    double Y[2], dY[2];
    Evaluate(t, Y, dY, nullptr);
    double norm = std::sqrt(dY[0]*dY[0]+dY[1]*dY[1]);
    assert(norm>nlparams.normTol); 
    double nvec[] = {-dY[1]/norm, dY[0]/norm};

    // signed distance
    sd = 0.;
    for(int k=0; k<2; ++k)
      sd += (X[k]-Y[k])*nvec[k];

    // Gradient
    if(dsd!=nullptr)
      { dsd[0] = nvec[0];
	dsd[1] = nvec[1]; }

    // done
    return;
  }
  
} // rbc::

