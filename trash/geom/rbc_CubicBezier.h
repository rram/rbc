// Sriramajayam

#ifndef RBC_CUBIC_BEZIER_H
#define RBC_CUBIC_BEZIER_H

#include <rbc_Spline_RootFinding.h>

namespace rbc
{
  //! Class defining a Cubic Bezier curve
  //! Not thread safe.
  class CubicBezier
  {
  public:
    //! Constructor
    //! \param[in] id Curve id
    //! \param[in] pts Set of 4 control points in order.
    //! \param[in] nsamples Number of sampling points to use
    CubicBezier(int id, std::vector<double>& pts,
		const int nsamples);

    //! Disable copy and assignment
    CubicBezier(const CubicBezier&) = delete;
    CubicBezier& operator=(const CubicBezier) = delete;

    //! Destructor
    //! Clean up
    virtual ~CubicBezier();

    //! Returns the curve id
    int GetCurveID() const;

    //! Returns the end points of the curve
    void GetTerminalPoints(double* tpoints) const;

    //! Returns the signed distance function to a point
    //! \param[in] X Point at which to evaluate
    //! \param[in] nlparams Solver parameters
    //! \param[out] sd Computed signed distance
    //! \param[out] dsd Computed derivative of the signed distance. Defaulted to nullptr.
    void GetSignedDistance(const double* X, const NLSolverParams& nlparams,
			   double& sd, double* dsd=nullptr);

    //! Evaluate the coordinates of the spline at the requested point
    //! \param[in] t Parameter value
    //! \param[out] X evaluate coordinate
    //! \param[out] dX evaluated derivative
    //! \param[out] d2X evaluated 2nd derivative
    void Evaluate(const double& t, double* X, double* dX=nullptr, double* d2X=nullptr);

  private:
    const int curve_id; //!< Curve id.
    const double P0[2], P1[2], P2[2], P3[2]; //!< Coordinats of control points
    typedef std::pair<int,double> tID; //!< Pairing of point index and its parametric coordinate
    typedef std::pair<boost_point2D,tID> pointParam;
    boost::geometry::index::rtree<pointParam, boost::geometry::index::quadratic<8>> rtree; //!< boost r-tree for closest point searches
    const double tbounds[2] = {0.,1.};
    detail::Spline_RootFinding_Context<CubicBezier> rootfinding_ctx; //!< Context to pass for root finding
  };

  
}

#endif
