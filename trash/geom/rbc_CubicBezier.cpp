// Sriramajayam

#include <rbc_CubicBezier.h>

namespace rbc
{
  // Implementation of the class CubicBezier

  // Constructor
  CubicBezier::CubicBezier(int id,
			   std::vector<double>& pts, const int nsamples)
    :curve_id(id),
     P0{pts[0],pts[1]}, P1{pts[2],pts[3]}, P2{pts[4],pts[5]}, P3{pts[6],pts[7]}
  {
    // Sanity checks
    assert(static_cast<int>(pts.size())==8);
    assert(nsamples>0);

    // Populate the r-tree with sample points
    double t, X[2];
    pointParam pp;
    int indx = 0;
    const double ds = 1./static_cast<double>(nsamples-1);
    for(int s=0; s<nsamples; ++s)
      {
	t = static_cast<double>(s)*ds;
	for(int k=0; k<2; ++k)
	  X[k] =
	    (1.-t)*(1.-t)*(1.-t)*P0[k] +
	    3.*(1.-t)*(1.-t)*t*P1[k] +
	    3.*(1.-t)*t*t*P2[k] +
	    t*t*t*P3[k];
	pp.first = boost_point2D(X[0], X[1]);
	pp.second = std::pair<int,double>(indx++,t);
	rtree.insert(pp);
      }

    // Setup root-finding
    rootfinding_ctx.spline = this;
    rootfinding_ctx.solver = gsl_root_fdfsolver_alloc(gsl_root_fdfsolver_steffenson);
    rootfinding_ctx.fdf.f = &detail::Spline_RootFinding_f<CubicBezier>;
    rootfinding_ctx.fdf.df = &detail::Spline_RootFinding_df<CubicBezier>;
    rootfinding_ctx.fdf.fdf = &detail::Spline_RootFinding_fdf<CubicBezier>;
    rootfinding_ctx.fdf.params = &rootfinding_ctx;
    
    // done
  }

  // Destructor: Clean up
  CubicBezier::~CubicBezier()
  { gsl_root_fdfsolver_free(rootfinding_ctx.solver); }

  // Returns the curve id
  int CubicBezier::GetCurveID() const
  { return curve_id; }
  
  // Returns the end points of the curve
  void CubicBezier::GetTerminalPoints(double* tpoints) const
  {
    for(int k=0; k<2; ++k)
      { tpoints[k] = P0[k];
	tpoints[2+k] = P3[k]; }
    return;
  }

  // Evaluate the coordinates of the spline at the requested point
  void CubicBezier::Evaluate(const double& t, double* X, double* dX, double* d2X)
  {
    // Coordinates
    assert(X!=nullptr);

    // Evaluate

    // X
    for(int k=0; k<2; ++k)
      X[k] =
	(1.-t)*(1.-t)*(1.-t)*P0[k] +
	3.*(1.-t)*(1.-t)*t*  P1[k] +
	3.*(1.-t)*t*t*       P2[k] +
	t*t*t*               P3[k];

    // dX
    if(dX!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  dX[k] =
	    3.*(1.-t)*(1.-t)*(P1[k]-P0[k]) +
	    6.*(1.-t)*t*     (P2[k]-P1[k]) +
	    3.*t*t*          (P3[k]-P2[k]);
      }

    // d2X
    if(d2X!=nullptr)
      {
	for(int k=0; k<2; ++k)
	  d2X[k] =
	    6.*(1.-t)*(P2[k]-2.*P1[k]+P0[k]) +
	    6.*t*     (P3[k]-2.*P2[k]+P1[k]);
      }

    // done
    return;
  }
       
  
  // Returns the signed distance function to a point
  void CubicBezier::GetSignedDistance(const double* X, const NLSolverParams& nlparams,
				      double& sd, double* dsd)
  {
    // Initial guess for the closest point
    boost_point2D pt(X[0], X[1]);
    std::vector<pointParam> result{};
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);
    double tinit = (result[0].second).second;
    
    // Extremizer of the distance function
    const double t = detail::Spline_FindRoot(X, tinit, tbounds,
					     nlparams, rootfinding_ctx);
    
    // Evaluate the spline and its derivatives here.
    // Y is the closest point projection
    double Y[2], dY[2];
    Evaluate(t, Y, dY, nullptr);
    double norm = std::sqrt(dY[0]*dY[0]+dY[1]*dY[1]);
    assert(norm>nlparams.normTol); 
    double nvec[] = {-dY[1]/norm, dY[0]/norm};
    
    // signed distance
    sd = 0.;
    for(int k=0; k<2; ++k)
      sd += (X[k]-Y[k])*nvec[k];

    // Gradient
    if(dsd!=nullptr)
      { dsd[0] = nvec[0];
	dsd[1] = nvec[1]; }
    
    // done
    return;
  }

} // rbc::  
