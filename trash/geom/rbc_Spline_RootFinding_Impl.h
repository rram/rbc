// Sriramajayam

#ifndef RBC_SPLINE_ROOT_FINDING_IMPL_H
#define RBC_SPLINE_ROOT_FINDING_IMPL_H

namespace rbc
{
  namespace detail
  {
    // Function value: (r(t)-X).r'(t)
    template<typename GeomType>
      double Spline_RootFinding_f(double t, void* params)
      {
	assert(params!=nullptr);
	auto* ctx = static_cast<Spline_RootFinding_Context<GeomType>*>(params);
	assert(ctx!=nullptr);
	
	const double* X = ctx->X;
	double* rvec = ctx->rvec;
	double* drvec = ctx->drvec;
	ctx->spline->Evaluate(t, rvec, drvec);
	
	return (rvec[0]-X[0])*drvec[0] + (rvec[1]-X[1])*drvec[1];
      }

     // Derivative of (r(t)-X).r'(t) = r'(t).r'(t) + (r(t)-X).r''(t)
    template<typename GeomType>
      double Spline_RootFinding_df(double t, void* params)
    {
      assert(params!=nullptr);
      auto* ctx = static_cast<detail::Spline_RootFinding_Context<GeomType>*>(params);
      assert(ctx!=nullptr);
      
      const double* X = ctx->X;
      double* rvec = ctx->rvec;
      double* drvec = ctx->drvec;
      double* d2rvec = ctx->d2rvec;
      ctx->spline->Evaluate(t, rvec, drvec, d2rvec);
      
      return (drvec[0]*drvec[0]+drvec[1]*drvec[1]) + (rvec[0]-X[0])*d2rvec[0] +  (rvec[1]-X[1])*d2rvec[1];
    }

     // Function & derivative of (r(t)-X).r'(t)
    template<typename GeomType>
    void Spline_RootFinding_fdf(double t, void* params, double* val, double* dval)
    {
      assert(params!=nullptr);
      auto* ctx = static_cast<detail::Spline_RootFinding_Context<GeomType>*>(params);
      assert(ctx!=nullptr);

      const double* X = ctx->X;
      double* rvec = ctx->rvec;
      double* drvec = ctx->drvec;
      double* d2rvec = ctx->d2rvec;
      ctx->spline->Evaluate(t, rvec, drvec, d2rvec);

      // value: (r(t)-X).r'(t)
      (*val) = (rvec[0]-X[0])*drvec[0] + (rvec[1]-X[1])*drvec[1];

      // derivative: r'(t).r'(t) + (r(t)-X).r''(t)
      (*dval) = (drvec[0]*drvec[0]+drvec[1]*drvec[1]) + (rvec[0]-X[0])*d2rvec[0] +  (rvec[1]-X[1])*d2rvec[1];

      return;
    }


    // Finds the root 't' of the function (r(t)-X).r'(t) = 0
    template<typename GeomType>
      double Spline_FindRoot(const double* X,
			     const double tinit,
			     const double tbounds[2],
			     const NLSolverParams& nlparams,
			     Spline_RootFinding_Context<GeomType>& rootfinding_ctx)
      {
	// Aliases
	const double& ttol = nlparams.ttol;
	const double& ftol = nlparams.ftol;
	auto* solver = rootfinding_ctx.solver;
	auto& fdf = rootfinding_ctx.fdf;
		
	// Setup the root-finding context
	rootfinding_ctx.X = X;

	// Initial guess for the closest point
	double t = tinit;

	// If the closest point is one of the end points, return it
	if(t<=tbounds[0]+ttol) return tbounds[0];
	else if(t>tbounds[1]-ttol) return tbounds[1];

	// Initialize the solver
	gsl_root_fdfsolver_set(solver, &fdf, t);

	// Iterate
	int iteration = 0;
	int status = 0;
	double tprev = t;
	double fval = 0.;
	while(iteration<nlparams.max_iter)
	  {
	    status = gsl_root_fdfsolver_iterate(solver);
	    t = gsl_root_fdfsolver_root(solver);

	    // Is this solution good enough

	    // Based on tolerance for the root
	    status = gsl_root_test_delta(tprev, t, ttol, 0.);
	    if(status==GSL_SUCCESS)
	      break;

	    // Based on tolerance for the function value
	    fval = fdf.f(t, &rootfinding_ctx);
	    status = gsl_root_test_residual(fval, ftol);
	    if(status==GSL_SUCCESS)
	      break;
	    
	    // Otherwise: continue
	    tprev = t;
	    ++iteration;
	  }

	assert(iteration<nlparams.max_iter &&
	       "rbc::detail::Spline_FindRoot- exceeded max iterations");
    
	return t;
      }

  } // rbc::detail
} // rbc::


#endif
